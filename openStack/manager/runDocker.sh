#!/bin/bash -v
sed -i "s/localhost/$(cat ~/lb-IP)/" ~/integrasjonsprosjekt/client/index.html
sed -i "s/localhost/$(cat ~/lb-IP)/" ~/integrasjonsprosjekt/api/api.js

cd ~/integrasjonsprosjekt/api
npm i -y
cd ~/integrasjonsprosjekt/client
npm i -y
cd ~/integrasjonsprosjekt
sudo docker-compose up -d
