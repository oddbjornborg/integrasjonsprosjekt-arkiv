#!/bin/bash -v
sudo apt install ca-certificates
#sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates
curl -fsSL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt -y install nodejs
sudo apt install docker -y
#sudo apt install npm -y
sudo apt install docker-compose -y
DIR=/home/ubuntu/integrasjonsprosjekt/
BOOL=false
while [ "$BOOL" != "true" ]
do
sleep 20s
if [ -d "$DIR" ];
then
        echo "$DIR exists"
        cd /home/ubuntu/integrasjonsprosjekt/client/
        npm i
        cd /home/ubuntu/integrasjonsprosjekt/api/
        npm i
        cd /home/ubuntu/integrasjonsprosjekt/
        sudo docker-compose up -d
        BOOL=true
        #All went well
else
        echo "$DIR does NOT exist"
        #TODO error handling: Keypair does not exist in .ssh folder of openstack client. cannot send the keypair to other instanses and enable ssh connection between them
fi
done
node -v
nodejs -v
docker -v
docker-compose -v