#!/bin/bash -v
servere=~/integrasjonsprosjekt/openStack/manager/serversIP
peers=~/integrasjonsprosjekt/openStack/manager/sendRepoTil

#sjekk om fil eksisterer
if [ -f "$peers" ]; then
        echo "$peers exists."
else
        touch ~/integrasjonsprosjekt/openStack/manager/sendRepoTil
fi

#sjekk om fil eksisterer
if [ -f "$servere" ]; then
        echo "$servere exists."
else
        #Hent openstack sin IP addresse
        openstackIP=$(cat ~/openstackIP)

        #hent stack sitt navn
        stackName=$(cat ~/integrasjonsprosjekt/openStack/manager/stack_name)

        #hent ID til pool
        poolID=$(ssh -i ~/.ssh/slettMeg dittUserName@$openstackIP openstack loadbalancer pool list | grep "$stackName-pool_web" | awk '{printf $2}')

        #OPENSTACK hent IP av alle servere tilknyttet LB
        ssh -i ~/.ssh/slettMeg dittUserName@$openstackIP openstack loadbalancer member list $poolID --format json | grep address | tr -d '" :address,' > ~/integrasjonsprosjekt/openStack/manager/serversIP
fi



#Så lenge det finnes flere linjer i servere filen
while IFS= read -r IPaddresse
do
sleep 3s
#Sjekk om IPaddressen fra servere ikke finner i peers filen
if ! grep -q $IPaddresse "$peers"; then
    #Dersom den ikke fantes er det en ny server
    echo "Server: $IPaddresse har IKKE fått tilsendt repo, sender nå"
    scp -r ~/integrasjonsprosjekt ubuntu@$IPaddresse:/home/ubuntu/
    sleep 5s
    ssh ubuntu@$IPaddresse 'bash -s' < ~/integrasjonsprosjekt/openStack/manager/runDocker.sh 
    echo "$IPaddresse" >> $peers
else
    #Dersom den fantes er ikke dette en ny server
    echo "Server: $IPaddresse har fått tilsendt repo fra før"
fi

done < "$servere"
