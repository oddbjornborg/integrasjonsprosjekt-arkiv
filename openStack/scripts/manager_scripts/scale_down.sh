#!/bin/bash -v

#TODO sjekk om fil med scale up/down URL eksisterer
FILE=~/integrasjonsprosjekt/openStack/manager/scale_down_url
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
    echo "$FILE does NOT exist"
    exit 1
fi


#Hent scale up/down URL
scale_up_url=$(cat ~/integrasjonsprosjekt/openStack/manager/scale_down_url)

#Send POST forespørsel om å minske antall servere til openstack API-et 
curl -X POST $scale_up_url

#Vent inntil en server er disconnecta fra glusterfs
loopFerdig="false"
#hent ip for disconnecta
disconnecta=$(sudo gluster peer status | grep -B 3 'Disconnected' | grep Hostname | awk '{printf $2}')

#Sjekk om det finnes en disconnected server
while [ "$loopFerdig" != "true" ]
do
sleep 5s
if [ -z "$disconnecta" ]
then
    echo "disconnecta er tom: $disconnecta"
	disconnecta=$(sudo gluster peer status | grep -B 3 'Disconnected' | grep Hostname | awk '{printf $2}')
else
    echo "disconnecta er ikke tom: $disconnecta"
	loopFerdig="true"
fi
done


#hent antall Bricks, altså hvor mange som replikkerer
antallReplikerendeNoder=$(sudo gluster volume info | grep 'Number of Bricks' | awk '{printf $8}')

#Sjekk om antallReplikerendeNoder er noe
while [ "$loopFerdig" != "true" ]
do
sleep 20s
if [ -z "$antallReplikerendeNoder" ]
then
    echo "antallReplikerendeNoder er tom: $antallReplikerendeNoder"
	antallReplikerendeNoder=$(sudo gluster volume info | grep 'Number of Bricks' | awk '{printf $8}')
else
    echo "antallReplikerendeNoder er ikke tom: $antallReplikerendeNoder"
	loopFerdig="true"
fi
done

if [ "$antallReplikerendeNoder" == 1 ]; then
    #hvis du skal fjerne peer og det er den siste
	echo "Det skal alltid være en server oppe"
    #Kan vurdere i fremtiden å stoppe replikkeringen
    #sudo gluster volume stop gv0
elif [ "$antallReplikerendeNoder" == 2 ]; then
	#Fjern replikkerte volum på server og endre antall replikkerte i volumet
    sudo gluster volume remove-brick gv0 replica 1 $disconnecta:/data/glusterfs/repo force
    #Kan være man må svare på yes/no prompts
    #Fjern server fra kjente glusterfs servere
    sudo gluster peer detach $disconnecta
elif [ "$antallReplikerendeNoder" == 3 ]; then
	#Fjern replikkerte volum på server og endre antall replikkerte i volumet
    sudo gluster volume remove-brick gv0 replica 2 $disconnecta:/data/glusterfs/repo force
    #Kan være man må svare på yes/no prompts
    #Fjern server fra kjente glusterfs servere
elif [ "$antallReplikerendeNoder" == 4 ]; then
	#Fjern replikkerte volum på server og endre antall replikkerte i volumet
    sudo gluster volume remove-brick gv0 replica 3 $disconnecta:/data/glusterfs/repo force
    #Kan være man må svare på yes/no prompts
    #Fjern server fra kjente glusterfs servere
	sudo gluster peer detach $disconnecta
else
    echo "antall av replikerende noder er ikke som forventet: $antallReplikerendeNoder"
    exit 1
fi