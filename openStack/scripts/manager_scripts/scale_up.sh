#!/bin/bash -v


#TODO sjekk om fil med scale up/down URL eksisterer
FILE=~/integrasjonsprosjekt/openStack/manager/scale_up_url
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
    echo "$FILE does NOT exist"
    exit 1
fi


#Hent antall peers
sudo gluster peer status | grep 'Number of Peers' | awk '{printf $4}'

#Hent IP addresser av kjente peers
sudo gluster peer status | grep Hostname | awk '{printf $2}' > ~/integrasjonsprosjekt/openStack/manager/peers


#Hent openstack sin IP addresse
openstackIP=$(cat ~/openstackIP)

#hent stack sitt navn
stackName=$(cat ~/integrasjonsprosjekt/openStack/manager/stack_name)

#kjør kommando på openstack
####ssh -i ~/.ssh/slettMeg nicholbs@$openstackIP openstack stack show $stackName --format json | grep -A 3 'pool_ip_address' | grep output_value | awk '{printf $2}' | tr -d '" :' > ~/integrasjonsprosjekt/openStack/manager/poolID

#hent ID til pool
poolID=$(ssh -i ~/.ssh/slettMeg dittUserName@$openstackIP openstack loadbalancer pool list | grep "$stackName-pool_web" | awk '{printf $2}')

#Hent scale up/down URL
scale_up_url=$(cat ~/integrasjonsprosjekt/openStack/manager/scale_up_url)

#Send POST forespørsel om å øke antall servere til openstack API-et 
curl -X POST $scale_up_url

#Vent til ny server er koblet til lastbalanserer
sleep 1m

#OPENSTACK hent IP av alle servere tilknyttet LB
ssh -i ~/.ssh/slettMeg dittUserName@$openstackIP openstack loadbalancer member list $poolID --format json | grep address | tr -d '" :address,' > ~/integrasjonsprosjekt/openStack/manager/serversIP


#Sammenlign alle IP addresser koblet til glusterfs med alle IP addresser tilkoblet lastbalanserer, den som ikke finnes i glusterfs fra før er den nye serveren
servere=~/integrasjonsprosjekt/openStack/manager/serversIP
peers=~/integrasjonsprosjekt/openStack/manager/peers

#Så lenge det finnes flere linjer i servere filen
while IFS= read -r IPaddresse
do
sleep 3s
#Sjekk om IPaddressen fra servere ikke finner i peers filen
if ! grep -q $IPaddresse "$peers"; then
    #Dersom den ikke fantes er det en ny server
    echo "$IPaddresse er ny server"
    #koble ny server til glusterfs klyngen
    sudo gluster peer probe $IPaddresse
    #TODO legg til loop inntil probe er successfull
    sleep 20s
    #finn ut hvor mange noder som replikerer
    antallReplikerendeNoder=$(sudo gluster volume info | grep 'Number of Bricks' | awk '{printf $8}')
    
    #Sjekk om antallReplikerendeNoder er noe
    while [ "$loopFerdig" != "true" ]
    do
    sleep 5s
    if [ -z "$antallReplikerendeNoder" ]; then
        echo "antallReplikerendeNoder er tom: $antallReplikerendeNoder"
        antallReplikerendeNoder=$(sudo gluster volume info | grep 'Number of Bricks' | awk '{printf $8}')
    else
        echo "antallReplikerendeNoder er ikke tom: $antallReplikerendeNoder"
        loopFerdig="true"
    fi
    done

    if [ "$antallReplikerendeNoder" == 1 ]; then
        sudo gluster volume add-brick gv0 replica 2 $IPaddresse:/data/glusterfs/repo force
    elif [ "$antallReplikerendeNoder" == 2 ]; then
        sudo gluster volume add-brick gv0 replica 3 $IPaddresse:/data/glusterfs/repo force
    elif [ "$antallReplikerendeNoder" == 3 ]; then
        sudo gluster volume add-brick gv0 replica 4 $IPaddresse:/data/glusterfs/repo force
    else
        echo "antall av replikerende noder er ikke som forventet: $antallReplikerendeNoder"
        exit 1
    fi
else
    #Dersom den fantes er ikke dette en ny server
    echo "$IPaddresse finnes i fil"
fi

done < "$servere"




