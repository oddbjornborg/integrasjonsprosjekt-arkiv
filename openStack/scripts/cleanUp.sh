#!/bin/bash -v
#Delete all openstack stacks
openstack stack list --format json | grep "Stack Name" | awk '{print $3}' | tr -d '",' | while read stack ; do openstack stack delete $stack -y; done

#Delete all keypairs
openstack keypair list --format json | grep "Name" -m 1 | awk '{print $2}' | tr -d '",' | while read keyPair ; do openstack keypair delete $keyPair; done
FILE=~/.ssh/slettMeg
if [ -f "$FILE" ]; 
then
	echo "$FILE exists"
	rm ~/.ssh/slettMeg
else
	echo "$FILE does NOT exist"
fi

