#!/bin/bash

for TABLE in "moves" "effects" "move_has_effects" "pokemon" "users"
do
        cockroach sql --insecure -e "TABLE defaultdb.$TABLE" --format=csv > ~/integrasjonsprosjekt/data/$TABLE.csv
        echo "#$(cat ~/data/$TABLE.csv)" > ~/integrasjonsprosjekt/data/$TABLE.csv
        echo "Exported $TABLE"
done