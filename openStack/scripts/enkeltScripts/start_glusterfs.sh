#!/bin/bash -v


managerIP=$(hostname -I | awk '{print $1}')

if [ "$#" == 1 ]; then
        gluster volume create gv0 replica 2 $managerIP:/data/glusterfs/repo $1:/data/glusterfs/repo force
elif [ "$#" == 2 ]; then
        gluster volume create gv0 replica 3 $managerIP:/data/glusterfs/repo $1:/data/glusterfs/repo $2:/data/glusterfs/repo force
elif [ "$#" == 3 ]; then
        gluster volume create gv0 replica 4 $managerIP:/data/glusterfs/repo $1:/data/glusterfs/repo $2:/data/glusterfs/repo $3:data/glusterfs/repo force
else
        echo "vet ikke hvor mange parametere"
        exit 1
fi

#Start glusterfs tjenesten på volumet
gluster volume start gv0
