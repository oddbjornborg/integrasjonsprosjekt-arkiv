#!/bin/bash -v
finnesInterntNettverk=$(openstack network list --format value | awk '{print $2}' | grep -v '^ntnu')

if [ -z "$finnesInterntNettverk" ]
then
 echo "\$finnesInterntNettverk is empty"
else
 echo "\$finnesInterntNettverk is NOT empty"
fi

