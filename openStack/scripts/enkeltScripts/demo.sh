#!/bin/bash -v

#Defualt values
stackName="testStack"
keyPairName="slettMeg"
stackNameClient="clientInstance"
stackNameAuto="autoScale"


#TODO Check if keypair exists
finnesKeyPair=$(openstack keypair list --format json | grep "Name" -m 1 | awk '{print $2}' | tr -d '",')
if [ -z "$finnesKeyPair" ]
then
	echo "\$finnesKeyPair is empty"
	#Create a keypair for SSH between instances in project and put the private key into .ssh folder
	openstack keypair create $keyPairName > ~/.ssh/slettMeg
	chmod 700 ~/.ssh/slettMeg
else
	#Keypair existed, do not need to create another
	echo "\$finnesKeyPair is NOT empty"
	keyPairName=$(openstack keypair list --format json | grep "Name" -m 1 | awk '{print $2}' | tr -d '",')
	FILE=~/.ssh/$keyPairName
	if [ -f "$FILE" ]; 
	then
		echo "$FILE exists"
		#All went well
	else
		echo "$FILE does NOT exist"
		#TODO error handling: Keypair does not exist in .ssh folder of openstack client. cannot send the keypair to other instanses and enable ssh connection between them
		exit 1
	fi
fi	



#Create a stack from the 'create-manager.yaml' template and send keypair as parameter
openstack stack create --template  ~/integrasjonsprosjekt/openStack/create-manager.yaml --parameter "keyPair_name=$keyPairName" $stackName
sleep 1m

#TODO test på om basic infrastruktur stack har completed successfully, dersom ikke får ikke manager floating IP addresse
ref="CREATE_COMPLETE"
stackStatus=$(openstack stack show $stackName --format json | jq '.stack_status' | tr -d '"')
if [ "$stackStatus" == "$ref" ]; then
	echo "Strings are equal"
else
	exit 1
fi

#loop until the network from stack has been created, we cannot create further instances before the network exists
loopFerdig="false"
finnesInterntNettverk=$(openstack network list --format value | awk '{print $2}' | grep -v '^ntnu') #finnesInterntNettverk = navnPåNettverkOpprettetFraCreateManager.yaml
while [ "$loopFerdig" != "true" ]
do
sleep 5s
 if [ -z "$finnesInterntNettverk" ]
 then
	 #echo "\$finnesInterntNettverk is empty"
	 finnesInterntNettverk=$(openstack network list --format value | awk '{print $2}' | grep -v '^ntnu')
 else
	 #The network exists and we can now continue the deployment
	 #echo "\$finnesInterntNettverk is NOT empty"
	 loopFerdig="true"
 fi
done

# openstack stack create --template ~/integrasjonsprosjekt/openStack/create-machine.yaml --parameter "internal_net=$finnesInterntNettverk;keyPair_name=$keyPairName" $stackNameClient
# sleep 2m

# #TODO test på om client stack har completed successfully, dersom ikke får ikke clienten floating IP addresse
# ref="CREATE_COMPLETE"
# stackStatus=$(openstack stack show $stackNameClient --format json | jq '.stack_status' | tr -d '"')
# if [ "$stackStatus" == "$ref" ]; then
# 	echo "Strings are equal"
# else
# 	exit 1
# fi

#TODO hent floating IP addresse fra instanser
managerIP=$(openstack stack show $stackName -f json | jq '.outputs[0].output_value,.outputs' | awk '/Floating/{getline; print}' | awk '{print $2}' | tr -d '"')
# clientIP=$(openstack stack show $stackNameClient -f json | jq '.outputs[0].output_value,.outputs' | awk '/Floating/{getline; print}' | awk '{print $2}' | tr -d '"')

#TODO send keyPair filer til alle instanser slik at de kan ta i mot og opprette SSH tilkoblinger
sleep 3m


#Loops nedenfor ser ut til å ikke funke. Forrige kommando $? ser ikke ut til å gi riktig return verdier, kanskje de blir 0 (succsessfull) pga annen kommando
loopFerdig="false"
#Wait until instance is ready for ssh connection, $result depicts result of scp command with 0 on success, >0 on error
scp -i ~/.ssh/$keyPairName ~/.ssh/$keyPairName ubuntu@$managerIP:/home/ubuntu/.ssh/
while [ "$loopFerdig" != "true" ] && [ $? -ne "0" ]
do
sleep 30s
 if [ -z "$managerIP" ]
 then
	#echo "\$managerIP is empty"
	#echo "\$result is $result"
	managerIP=$(openstack stack show $stackName -f json | jq '.outputs[0].output_value,.outputs' | awk '/Floating/{getline; print}' | awk '{print $2}' | tr -d '"')
 else
	#echo "\$managerIP is NOT empty"
	#echo "\$result is $result"
	loopFerdig="true"
	scp -i ~/.ssh/$keyPairName ~/.ssh/$keyPairName ubuntu@$managerIP:/home/ubuntu/.ssh/
 fi
done


# loopFerdig="false"
# #Wait until instance is ready for ssh connection, $result depicts result of scp command with 0 on success, >0 on error
# scp -i ~/.ssh/$keyPairName ~/.ssh/$keyPairName ubuntu@$clientIP:/home/ubuntu/.ssh/
# while [ "$loopFerdig" != "true" ] && [ "$?" -ne "0" ]
# do
# sleep 30s
# if [ -z "$clientIP" ]
# then
# 	#echo "\$clientIP is empty"
# 	#echo "\$result is $result"
# 	clientIP=$(openstack stack show $stackNameClient -f json | jq '.outputs[0].output_value,.outputs' | awk '/Floating/{getline; print}' | awk '{print $2}' | tr -d '"')
# else
# 	#echo "\$clientIP is NOT empty"
# 	#echo "\$result is $result"
# 	loopFerdig="true"
# 	scp -i ~/.ssh/$keyPairName ~/.ssh/$keyPairName ubuntu@$clientIP:/home/ubuntu/.ssh/
# fi
# done

#Send repository to manager
scp -r -i ~/.ssh/$keyPairName ~/integrasjonsprosjekt/ ubuntu@$managerIP:/home/ubuntu/
SUBNET=$(openstack subnet list --format json | grep "Name" | awk '{printf $2}' | tr -d '",')
openstack stack create --template ./create-autoScale.yaml -e ~/integrasjonsprosjekt/openStack/env-variables.yaml --parameter "network=$finnesInterntNettverk;subnet_id=$SUBNET" $stackNameAuto
# scp -r -i ~/.ssh/$keyPairName ~/integrasjonsprosjekt/ ubuntu@$clientIP:/home/ubuntu/
sleep 2m

LOADB_name=$(openstack loadbalancer pool list --format json | grep "name" | awk '{printf $2}' | tr -d '",')
MEMBERS=$(openstack loadbalancer member list $LOADB_name --format json | grep address | tr -d '",address: ')

echo $MEMBERS > ~/integrasjonsprosjekt/openstack/members
scp -i ~/.ssh/slettMeg -r ~/integrasjonsprosjekt/openStack/members ubuntu@$managerIP:/home/ubuntu/integrasjonsprosjekt/openStack/
