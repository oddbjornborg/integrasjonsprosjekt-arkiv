#!/bin/bash

for TABLE in "moves" "effects" "move_has_effects" "pokemon" "users"
do
        cockroach userfile delete $TABLE.csv --insecure
        cockroach userfile upload ~/integrasjonsprosjekt/data/$TABLE.csv /$TABLE.csv --insecure > /dev/null
done

cockroach sql --insecure < ~/integrasjonsprosjekt/openStack/scripts/enkeltScripts/sql_import