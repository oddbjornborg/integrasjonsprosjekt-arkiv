#!/bin/bash -v
managerIP=$(hostname -I | awk '{print $1}')


if [ "$#" == 1 ]; then
        echo "-----------En server-----------"
        #Start manager
        sudo cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$managerIP:26257 --advertise-addr=$managerIP:26257 --max-offset=1500ms
        #/home/ubuntu/manager-IP
        
        #Start Server1
        ssh ubuntu@$1 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$managerIP:26257 --advertise-addr=$1:26257 --max-offset=1500ms

elif [ "$#" == 2 ]; then
        echo "-----------to servere---------"
        #Start manager
        sudo cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$managerIP:26257 --advertise-addr=$managerIP:26257 --max-offset=1500ms

        #Start Server1
        ssh ubuntu@$1 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$managerIP:26257 --advertise-addr=$1:26257 --max-offset=1500ms
        
        #Start Server2
        ssh ubuntu@$2 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$managerIP:26257 --advertise-addr=$2:26257 --max-offset=1500ms

elif [ "$#" == 3 ]; then
        echo "-----------tre servere---------"
        #Start manager
        sudo cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$3:26257,$managerIP:26257 --advertise-addr=$managerIP:26257 --max-offset=1500ms
        
        #Start Server1
        ssh ubuntu@$1 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$3:26257,$managerIP:26257 --advertise-addr=$1:26257 --max-offset=1500ms
        
        #Start Server2
        ssh ubuntu@$2 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$3:26257,$managerIP:26257 --advertise-addr=$2:26257 --max-offset=1500ms
        
        #Start Server3
        ssh ubuntu@$3 cockroach start --insecure --store=/home/ubuntu/cockroachdb --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8083 --background --join=$1:26257,$2:26257,$3:26257,$managerIP:26257 --advertise-addr=$3:26257 --max-offset=1500ms
else
        echo "vet ikke hvor mange parametere"
        exit 1
fi
sleep 15s

cockroach init --insecure --host=$managerIP:26257

cockroach import db MYSQLDUMP ~/integrasjonsprosjekt/integrasjon_pokemon.sql --insecure

sh ~/integrasjonsprosjekt/openStack/scripts/enkeltScripts/import_from_csv.sh