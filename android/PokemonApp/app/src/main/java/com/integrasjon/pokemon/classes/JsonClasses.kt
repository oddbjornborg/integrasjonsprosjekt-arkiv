package com.integrasjon.pokemon.classes

import com.google.gson.annotations.SerializedName

// Provides body for login request
data class JSONCredentials(
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String
)

// Receives data from login response
data class JSONUserInfo(
    @SerializedName("username") val username: String,
    @SerializedName("id") val id: Int
)

data class JSONUserRegister(
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String,
    @SerializedName("repeatPassword") val repeatPassword: String
)

// Provides body for create game request
data class JSONCreateGame(
    @SerializedName("lobby_name") val lobbyName: String,
    @SerializedName("is_private") val isPrivate: Boolean,
    @SerializedName("host_socket") val hostSocket: String
)

data class JSONUserRequest(
    @SerializedName("user") val user: Int
)

data class JSONUserResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("username") val username: String,
    @SerializedName("pokemon_first")val poke1: Int,
    @SerializedName("pokemon_second")val poke2: Int,
    @SerializedName("pokemon_third")val poke3: Int,
    @SerializedName("pokemon_fourth")val poke4: Int,
    @SerializedName("pokemon_fifth")val poke5: Int,
    @SerializedName("pokemon_sixth")val poke6: Int,
)

data class JSONPokemonRequest(
    val team: Array<Int>
)

data class JSONPokemonResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("level") val level: Int,
    @SerializedName("hp") val hp: Int,
    @SerializedName("attack") val attack: Int,
    @SerializedName("defense") val defense: Int,
    @SerializedName("special_attack") val special_attack: Int,
    @SerializedName("special_defense") val special_defense: Int,
    @SerializedName("speed") val speed: Int,
    @SerializedName("type_primary") val type_primary: String,
    @SerializedName("type_secondary") val type_secondary: String,
    @SerializedName("sprite") val sprite: String,
    @SerializedName("sex") val sex: String,
    @SerializedName("move_first") val move_first: Int,
    @SerializedName("move_second") val move_second: Int,
    @SerializedName("move_third") val move_third: Int,
    @SerializedName("move_fourth") val move_fourth: Int
)

data class JSONMoveRequest (
    val movelist: Array<Int>
)

data class JSONMoveResponse (
    // Move data
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("type") val type: String,
    @SerializedName("power") val power: Int,
    @SerializedName("is_physical") val isPhysical: Int,
    @SerializedName("power_points") val pp: Int,
    @SerializedName("accuracy") val accuracy:  Int,
    @SerializedName("priority") val priority: Int,
    @SerializedName("crit_ratio") val critRatio: Int,
    @SerializedName("description") val description: String,
    @SerializedName("move_anim") val anim: String,
    @SerializedName("move_anim_target") val animTarget: String,

    // Effect data
    @SerializedName("effect_target") val effectTarget: String?,
    @SerializedName("apply_chance") val applyChance: Int?,
    @SerializedName("status_applied") val statusApplied: String?,
    @SerializedName("stats_modified") val statsModified: String?,
    @SerializedName("modified_value") val modifiedValue: Int?,
    @SerializedName("effect_anim") val effectAnim: String?,
    @SerializedName("effect_anim_target") val effectAnimTarget: String?
)

data class JSONPlayerJoined (
    @SerializedName("user_id") val user_id: Int,
    @SerializedName("socket_id") val socket_id: String
)

data class JSONJoinGameResponse (
    @SerializedName("socket_id") val lobby: String,
    @SerializedName("user") val user: String,
    @SerializedName("host_id") val host_id: Int,
    @SerializedName("host_socket") val host_socket: String
)