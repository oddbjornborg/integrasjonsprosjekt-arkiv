package com.integrasjon.pokemon.composables

import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import com.integrasjon.pokemon.Animation
import com.integrasjon.pokemon.Constants
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.animIsBuff
import com.integrasjon.pokemon.animIsStatus
import kotlinx.coroutines.runBlocking
import kotlin.concurrent.thread

/**
 * Displays the Pokémon's sprites and performs animations.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
@Composable
fun PokemonIcon(
    painter: Painter,
    faceRight: Boolean,
    modifier: Modifier = Modifier,
    playAnim: Animation = Animation.NONE
) {
    // *-- ATTACK ANIMATIONS --*

    // Target dp values
    var targetOffsetX by remember { mutableStateOf(0.dp)}
    var targetOffsetY by remember { mutableStateOf(0.dp)}

    // Animations specs
    val attackTweenSpec: AnimationSpec<Dp> = tween(
        durationMillis = 250,
        easing = if(targetOffsetX == 0.dp) LinearOutSlowInEasing else LinearEasing
    )

    // Animated dp values
    val offsetAnimX = animateDpAsState(
        targetValue = targetOffsetX,
        animationSpec = attackTweenSpec
    )
    val offsetAnimY = animateDpAsState(
        targetValue = targetOffsetY,
        animationSpec = attackTweenSpec
    )

    // *-- BUFF AND DEBUFF ANIMATIONS --*

    // Normal tween for main movement, reset tween for resetting to original position
    val normalTween: TweenSpec<Float> = tween(500, easing = FastOutSlowInEasing)

    val resetTween: TweenSpec<Float> = tween(50)
    // Activates / deactivates buff icon
    var showBuffIcons by remember { mutableStateOf(false) }
    var showDebuffIcons by remember { mutableStateOf(false)}

    // Target for alpha and position animations
    var alphaTarget by remember { mutableStateOf(0.0f)}
    var animTarget by remember { mutableStateOf(0.0f)}

    // Animated float value for buff graphic movement
    val buffAnim = animateFloatAsState(
        targetValue = animTarget,
        animationSpec = if(animTarget == 1f) normalTween else resetTween
    )

    // Animated float value for buff graphic alpha
    val buffAnimAlpha = animateFloatAsState(
        targetValue = alphaTarget,
        animationSpec = tween(250, easing = LinearEasing)
    )

    val spriteAlpha by remember { mutableStateOf(1f)}

    // Lock to prevent stacking calls of animation cycle
    var lock by remember { mutableStateOf(false)}

    // Animation cycle
    when {
        // Attack animation
        playAnim == Animation.ATTACK -> {
            if (!lock) {
                lock = true
                thread {
                    targetOffsetX = -(20).dp
                    Thread.sleep(250)
                    targetOffsetX = 0.dp
                    Thread.sleep(250)
                }
                lock = false
            }
        }
        // Buff and debuff animations
        animIsBuff(playAnim) -> {
            if (!lock) {
                runBlocking {
                    thread {
                        lock = true
                        for (i in 0..1) {
                            showBuffIcons = playAnim == Animation.BUFF
                            showDebuffIcons = playAnim == Animation.DEBUFF
                            animTarget = 1f
                            alphaTarget = 1f
                            Thread.sleep(250)
                            alphaTarget = 0f
                            Thread.sleep(250)
                            showBuffIcons = false
                            showDebuffIcons = false
                            animTarget = 0f
                            Thread.sleep(50)
                        }
                        lock = false
                    }
                }
            }
        }
        // Affected by status animations
        animIsStatus(playAnim) -> {
            if(!lock) {
                runBlocking {
                    thread {
                        lock = true
                        for (i in 0..1) {
                            alphaTarget = 1f
                            Thread.sleep(250)
                            alphaTarget = 0f
                            Thread.sleep(300)
                        }
                        lock = false
                    }
                }
            }
        }
        // Probably unnecessary?
        else -> {
            showBuffIcons = false
            showDebuffIcons = false
        }
    }

    // Sprite and animation
    Box {
        // Pokémon sprite
        Image(
            painter = painter,
            contentDescription = "",
            contentScale = ContentScale.Fit,
            modifier = modifier
                .scale(if (faceRight) -1f else 1f, 1f)
                .offset(offsetAnimX.value, offsetAnimY.value)
                .background(Color.Transparent)
                .alpha(spriteAlpha)
        )

        // Color filter overlay for flashing effects
        if(animIsBuff(playAnim) || animIsStatus(playAnim)) {
            Image(
                painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit,
                modifier = modifier
                    .scale(if (faceRight) -1f else 1f, 1f)
                    .background(Color.Transparent),
                colorFilter = ColorFilter.tint(
                    when (playAnim) {
                        Animation.BUFF -> Color.Blue
                        Animation.DEBUFF -> Color.Red
                        Animation.PARALYSIS -> Color.Yellow
                        Animation.BURN -> Color.Red
                        Animation.POISON -> Color.Magenta
                        else -> Color.Transparent
                    }
                ),
                alpha = buffAnimAlpha.value
            )
        }

        // Buff and debuff particles
        if(animIsBuff(playAnim)) {
            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .size(170.dp)
                    .padding(top = 20.dp, bottom = 30.dp)
            ) {
                if (showBuffIcons) {
                    for (i in 0..2) {
                        Box(
                            modifier = Modifier
                                .height(30.dp)
                                .width(170.dp)
                                .padding(start = 10.dp, end = 10.dp)
                                .alpha(buffAnimAlpha.value)
                                .background(
                                    Brush.verticalGradient(
                                        0.0f to Color.Blue,
                                        0.6f to Color.Transparent
                                    ),
                                    CutCornerShape(50),
                                    0.50f
                                )
                        )
                    }
                    Box(modifier = Modifier.fillMaxHeight(buffAnim.value))
                }
                else if (showDebuffIcons) {
                    for (i in 0..2) {
                        Box(
                            modifier = Modifier
                                .height(30.dp)
                                .width(170.dp)
                                .padding(start = 10.dp, end = 10.dp)
                                .offset(y = -((140 - 90) * (1 - buffAnim.value)).dp)
                                .alpha(buffAnimAlpha.value)
                                .background(
                                    Brush.verticalGradient(
                                        0.4f to Color.Transparent,
                                        1.0f to Color.Red
                                    ),
                                    CutCornerShape(50),
                                    0.60f
                                )
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun IconPreview() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .background(Color.Blue)
    ) {
        PokemonIcon(
            painterResource(id = R.drawable.dragonsect),
            false,
            modifier = Modifier.size(170.dp)
        )
    }

}