package com.integrasjon.pokemon.classes

import com.integrasjon.pokemon.Animation
import com.integrasjon.pokemon.AnimationTarget

/**
 * Used for all visual feedback to the player. Contains an obligatory message, and optional
 * animation data.
 *
 * @author Oddbjørn S. Borge-jensen
 */
data class MessageFlag (
    val message: String,
    val animFlag: Animation = Animation.NONE,
    val animTarget: AnimationTarget = AnimationTarget.SELF
)
