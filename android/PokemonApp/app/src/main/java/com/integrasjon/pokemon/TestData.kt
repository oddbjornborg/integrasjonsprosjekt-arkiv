package com.integrasjon.pokemon

import com.integrasjon.pokemon.classes.*

// ! - - DEPRECATED - - !

// TEST MOVES
val MOVE_TACKLE = Move(
    name = "TACKLE",
    type = Type.NORMAL,
    power = 40,
    isPhysical = true,
    moveEffects = mutableListOf(),
    ppMax = 35,
    accuracy = 100,
    priority = 0,
    description = "Inflicts damage on the target.",
    animFlag = Animation.ATTACK
)
val MOVE_THUNDER_WAVE = Move(
    name = "THUNDER WAVE",
    type = Type.ELECTRIC,
    power = 0,
    isPhysical = false,
    moveEffects = mutableListOf(
        MoveEffect(
            target = AnimationTarget.FOE,
            applyChance = 100,
            statusApplied = StatusCondition.PARALYSIS
        )
    ),
    ppMax = 20,
    accuracy = 90,
    priority = 0,
    description = "Causes paralysis in the target.",
    animFlag = Animation.ATTACK
)
val MOVE_DRAGON_DANCE = Move(
    name = "DRAGON DANCE",
    type = Type.DRAGON,
    power = 0,
    isPhysical = false,
    moveEffects = mutableListOf(
        MoveEffect(
            target = AnimationTarget.SELF,
            applyChance = 100,
            statsModified = listOf(
                Constants.STAT_ATTACK,
                Constants.STAT_SPEED
            ),
            modifiedValue = 1
        )
    ),
    ppMax = 20,
    accuracy = 0,
    priority = 0,
    description = "Boosts the user's Attack and Speed by one level.",
)
val MOVE_GROWL = Move(
    name = "GROWL",
    type = Type.NORMAL,
    power = 0,
    isPhysical = false,
    moveEffects = mutableListOf(
        MoveEffect(
            target = AnimationTarget.FOE,
            applyChance = 100,
            statsModified = listOf(Constants.STAT_ATTACK),
            modifiedValue = -1
        )
    ),
    ppMax = 30,
    accuracy = 0,
    priority = 0,
    description = "Reduces enemy's Attack by one level.",
    animFlag = Animation.ATTACK
)
val MOVE_RAZOR_LEAF = Move(
    name = "RAZOR LEAF",
    type = Type.GRASS,
    power = 55,
    isPhysical = true,
    ppMax = 25,
    accuracy = 95,
    priority = 0,
    critRatio = 1,
    description = "High critical hit ratio.",
    animFlag = Animation.ATTACK
)
val MOVE_FORCE_PULL = Move(
    name = "FORCE PULL",
    type = Type.PSYCHIC,
    power = 20,
    isPhysical = false,
    moveEffects = mutableListOf(
        MoveEffect(
            target = AnimationTarget.FOE,
            applyChance = 100,
            statsModified = listOf(Constants.STAT_EVASION),
            modifiedValue = -2
        )
    ),
    ppMax = 20,
    accuracy = 0,
    description = "Get over here!",
    animFlag = Animation.ATTACK,
    animTarget = AnimationTarget.FOE
)

// TEST POKEMON
val POKEMON_MAROTOISE = Pokemon(
    name = "MAROTOISE",
    hpMax = 154,
    hpCurrent = 127,
    attack = 113,
    defense = 120,
    spAttack = 94,
    spDefense = 125,
    speed = 98,
    types = listOf(Type.WATER, Type.GROUND),
    sprite = R.drawable.marotoise,
    sex = R.string.female,
    moveIds = listOf(1,2,3,4),
    moveList = mutableListOf(
        MOVE_DRAGON_DANCE.clone(),
        MOVE_GROWL.clone(),
        MOVE_TACKLE.clone(),
        MOVE_THUNDER_WAVE.clone()
    )
)
val POKEMON_HORSEM = Pokemon(
    name = "horsem",
    hpMax = 113,
    hpCurrent = 27,
    attack = 78,
    defense = 152,
    spAttack = 65,
    spDefense = 133,
    speed = 78,
    types = listOf(Type.ROCK, Type.WATER),
    sprite = R.drawable.horsem,
    sex = R.string.male,
    moveIds = listOf(1,2,3,4),
    moveList = mutableListOf(
        MOVE_DRAGON_DANCE.clone(),
        MOVE_GROWL.clone(),
        MOVE_TACKLE.clone(),
        MOVE_THUNDER_WAVE.clone()
    )
)
val POKEMON_BULBTO = Pokemon(
    name = "bulbto",
    hpMax = 72,
    attack = 85,
    defense = 54,
    spAttack = 112,
    spDefense = 93,
    speed = 87,
    types = listOf(Type.GRASS, Type.NORMAL),
    sprite = R.drawable.bulbto,
    sex = R.string.genderless,
    moveIds = listOf(1,2,3,4),
    moveList = mutableListOf(
        MOVE_RAZOR_LEAF.clone(),
        MOVE_DRAGON_DANCE.clone(),
        MOVE_FORCE_PULL.clone(),
        MOVE_GROWL.clone()
    )
)
val POKEMON_SQUAD = listOf(
    POKEMON_BULBTO,
    POKEMON_HORSEM,
    POKEMON_MAROTOISE,
    POKEMON_MAROTOISE.clone(),
    POKEMON_MAROTOISE.clone(),
    POKEMON_MAROTOISE.clone()
)