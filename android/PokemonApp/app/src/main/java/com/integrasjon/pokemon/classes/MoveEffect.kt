package com.integrasjon.pokemon.classes

import androidx.annotation.StringRes
import com.integrasjon.pokemon.*

data class MoveEffect(
    val target: AnimationTarget,
    val applyChance: Int,
    val statusApplied: StatusCondition = StatusCondition.NONE,
    val statsModified: List<Int> = listOf(),
    val modifiedValue: Int = 0,
    val animFlag: Animation = Animation.NONE
)