package com.integrasjon.pokemon.classes

data class StatusResponse(
    val isImmobilized: Boolean,
    val messageFlag: MessageFlag?
)
