package com.integrasjon.pokemon.composables

import android.widget.Toast
import android.widget.Toast.makeText
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.gson.JsonObject
import com.integrasjon.pokemon.classes.HttpSingleton

@Composable
fun RegisterScreen(mainNavController: NavController) {
    var usernameState by remember { mutableStateOf("") }
    var passwordState by remember { mutableStateOf("") }
    var confirmPasswordState by remember { mutableStateOf("") }
    var passwordVisibility by remember { mutableStateOf(false) }

    val context = LocalContext.current

    fun toast(text: String) {
        makeText(
            context,
            text,
            Toast.LENGTH_SHORT
        ).show()
    }

    // FORM
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(Color.DarkGray)
    ) {
        // USERNAME
        TextField(
            value = usernameState,
            onValueChange = { usernameState = it },
            label = { Text("Username") }
        )
        // PASSWORD
        TextField(
            value = passwordState,
            onValueChange = { passwordState = it },
            label = { Text("Password") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation =
                if(passwordVisibility) VisualTransformation.None
                else PasswordVisualTransformation(),
            trailingIcon = {
                val image =
                    if(passwordVisibility) Icons.Filled.Visibility
                    else Icons.Filled.VisibilityOff

                IconButton(onClick = {
                    passwordVisibility = !passwordVisibility
                }) {
                    Icon(imageVector = image, "Toggle password visibility")
                }
            }
        )
        // CONFIRM PASSWORD
        TextField(
            value = confirmPasswordState,
            onValueChange = { confirmPasswordState = it },
            label = { Text("Confirm password") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation =
            if(passwordVisibility) VisualTransformation.None
            else PasswordVisualTransformation()
        )

        Spacer(modifier = Modifier.height(20.dp))

        Button(
            onClick = {
                if(passwordState != confirmPasswordState) {
                    // Passwords don't match
                    toast("Passwords must match!")

                    passwordState = ""
                    confirmPasswordState = ""
                } else {
                    // Passwords match; send register request
                    if (HttpSingleton.registerRequest(
                        username = usernameState,
                        password = passwordState,
                        confirm = confirmPasswordState
                    )) {
                        mainNavController.navigate("main_menu")
                    }
                    else {
                        toast("Register failed!")
                        passwordState = ""
                        confirmPasswordState = ""
                    }
                }
            }
        ) {
            Text("Register")
        }
    }
}

@Preview
@Composable
private fun RegisterPreview() {
    RegisterScreen(mainNavController = rememberNavController())
}