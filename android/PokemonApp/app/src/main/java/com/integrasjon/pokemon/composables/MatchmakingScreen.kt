package com.integrasjon.pokemon.composables

import android.app.DownloadManager
import android.content.Context
import android.graphics.Paint
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.json.JSONObject

private val CLR_MAIN_MENU_BACKGROUND = Color.DarkGray
private val CLR_MAIN_MENU_TEXT = Color.White

private const val CONFIG_MAIN_MENU_TEXT_SIZE = 60

@Composable
fun MatchmakingMenu (navController: NavHostController) {
    val mmNavController = rememberNavController()

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(CLR_MAIN_MENU_BACKGROUND)
    ) {
        Column (
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.5f)
        ) {
            MenuItem(
                text = "Create lobby",
                modifier = Modifier
                    .clickable { navController.navigate("mm_host_screen") }
            )
            MenuItem(
                text = "Join lobby",
                modifier = Modifier
                    .clickable { navController.navigate("mm_public_screen") }
            )
            MenuItem(
                text = "Enter code",
                modifier = Modifier
                    .clickable { navController.navigate("mm_private_screen") }
            )
        }
    }

    NavHost(navController = mmNavController, startDestination = "mm_menu") {
        composable("mm_menu") {}
        composable("mm_private_lobby") {  }
    }
}

@Preview
@Composable
private fun MMPreview(navController: NavHostController = rememberNavController()) {
    MatchmakingMenu(navController = navController)
}