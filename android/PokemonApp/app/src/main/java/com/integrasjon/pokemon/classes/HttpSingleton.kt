package com.integrasjon.pokemon.classes

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.util.concurrent.CountDownLatch
import kotlin.properties.Delegates

/**
 * Object storing information relevant to the http connection and performing web requests.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
object HttpSingleton {
    private val client: OkHttpClient = OkHttpClient()

    // 10.0.0.2 is an address that reaches localhost for the device hosting the emulator.
    // Simply using localhost will try to connect to the emulator's localhost, not the host's.
    private const val host = "http://10.0.2.2:8081"

    var loginCookie: String? = null
    var loginName: String? = null
    var loginId: Int? = null

    fun registerRequest(username: String, password: String, confirm: String): Boolean {
        var registerStatus = true

        val parameters = JSONUserRegister(username, password, confirm)

        val url = "$host/registerUser"

        val json = Gson().toJson(parameters)
        val requestBody = json.toRequestBody()

        val request = postRequest(url, requestBody)

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
                registerStatus = false
            }

            override fun onResponse(call: Call, response: Response) {
                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
        return registerStatus
    }

    /**
     * Gets a designated cookie that servers to verify whether or not a user is logged in upon
     * successful login.
     */
    fun loginRequest(username: String, password: String): Boolean {
        var result by Delegates.notNull<Boolean>()

        // Request url
        val url = "$host/login"

        // Create request body
        val credentials = JSONCredentials(username, password)
        val json = Gson().toJson(credentials)
        val requestBody = json.toRequestBody()

        // Create request
        var request = baseRequest(url).newBuilder()
            .post(requestBody)
            .build()

        // Add login cookie if set
        loginCookie?.let {
            request = request.newBuilder()
                .addHeader("Cookie", it)
                .build()
        }

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                result = false
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")

                // Convert JSON to Data Class
                val body = response.body?.string()
                val userInfo = Gson().fromJson(body, JSONUserInfo::class.java)

                // Set user name and id
                Log.println(Log.VERBOSE, "http/login", "username: ${userInfo.username}; userid: ${userInfo.id}")
                loginName = userInfo.username
                loginId = userInfo.id

                // Set cookie if not already set
                if(!hasCookie()) response.headers["Set-Cookie"]?.let { loginCookie = it }

                // Handle response
                result = response.code == 200
                countDownLatch.countDown()
            }
        })
        // Awaits response before returning
        countDownLatch.await()
        return result
    }

    fun createGameRequest(name: String, private: Boolean, socketId: String) {
        if(!hasCookie()) return

        // Request url
        val url = "$host/createGame"

        // Create request body
        val gameInfo = JSONCreateGame(name, private, socketId)
        val json = Gson().toJson(gameInfo)
        val requestBody = json.toRequestBody()

        // Create request
        val request = postRequest(url, requestBody)
//        println(request.body!!.toString())

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
    }

    fun findGameRequest(): JSONJoinGameResponse? {
        val url = "$host/findGame"
        val request = getRequest(url)
        var result: JSONJoinGameResponse? = null

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                result = Gson().fromJson(body, JSONJoinGameResponse::class.java)

                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")

                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
        return result
    }

    fun fetchUserRequest(user: Int): JSONUserResponse? {
        val url = "$host/retrieveUser"
        var result: JSONUserResponse? = null

        // Create request body
        val reqData = JSONUserRequest(user)
        val json = Gson().toJson(reqData)
        val requestBody = json.toRequestBody()

        // Create request
        val request = postRequest(url, requestBody)

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                result = Gson().fromJson(body, JSONUserResponse::class.java)

                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")

                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
        return result
    }

    fun fetchPokemonRequest(user: User): Array<JSONPokemonResponse>? {
        val url = "$host/retrievePokemonTeam"
        var result: Array<JSONPokemonResponse>? = null
        val ids = user.teamIds
        println(ids)

        // Create request body
        val reqData = JSONPokemonRequest(ids.toTypedArray())
        val json = Gson().toJson(reqData)
        val requestBody = json.toRequestBody()
        //println(requestBody.toString())

        // Create request
        val request = postRequest(url, requestBody)

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                result = Gson().fromJson(body, Array<JSONPokemonResponse>::class.java)

                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")

                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
        return result
    }

    fun fetchMovesRequest(moveList: List<Int>): Array<JSONMoveResponse>? {
        val url = "$host/retrieveMovesWithEffects"
        var result: Array<JSONMoveResponse>? = null

        // Create request body
        val reqData = JSONMoveRequest(moveList.toTypedArray())
        val json = Gson().toJson(reqData)
        val requestBody = json.toRequestBody()

        // Create request
        val request = postRequest(url, requestBody)

        // Enqueue request
        val countDownLatch = CountDownLatch(1)
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {
                println("!-- Failed to execute request!")
                println(e.printStackTrace())
                countDownLatch.countDown()
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                result = Gson().fromJson(body, Array<JSONMoveResponse>::class.java)

                Log.println(Log.VERBOSE, "http", "Request to $url -> Status: ${response.code}: ${response.message}")
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
        return result
    }

    /**
     * Used to check if the player is logged in.
     *
     * @see loginRequest
     */
    fun hasCookie(): Boolean {
        return loginCookie != null
    }

    private fun baseRequest(url: String): Request {
        return Request.Builder()
            .url(url)
            .addHeader("credentials", "include")
            .addHeader("Accept", "application/json")
            .addHeader("Content-Type", "application/json")
            .build()
    }

    private fun postRequest(url: String, requestBody: RequestBody): Request {
        return baseRequest(url).newBuilder()
            .addHeader("Cookie", "$loginCookie")
            .post(requestBody)
            .build()
    }

    private fun getRequest(url: String): Request {
        return baseRequest(url).newBuilder()
            .addHeader("Cookie", "$loginCookie")
            .build()
    }
}