package com.integrasjon.pokemon.composables

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.integrasjon.pokemon.POKEMON_HORSEM
import com.integrasjon.pokemon.classes.*
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.StatusCondition

// Status box color theme
private val CLR_STATUS_BAR_BACKGROUND_OUTER = Color(0xFF50685e)
private val CLR_STATUS_BAR_BACKGROUND_INNER = Color(0xFFe5f8cb)
private val CLR_STATUS_BAR_BORDER = Color(0xFF263118)
private val CLR_STATUS_BAR_TEXT = Color(0xFF404040)
private val CLR_STATUS_BAR_SHADOW = Color(0xFFd8d0b0)
private val CLR_VENUS = Color(0xFFf89890)
private val CLR_MARS = Color(0xFF40c8f8)

// HP bar color theme
private val CLR_HP_BAR_TEXT = Color(0xFFf2b57c)
private val CLR_HP_BAR_BORDER = Color(0xFFeff8eb)
private val CLR_HP_BAR_INNER_DARK = Color(0xFF58d080)
private val CLR_HP_BAR_INNER_LIGHT = Color(0xFF70f8a8)
private val CLR_HP_BAR_INNER_DARK_50 = Color(0xFFc8a808)
private val CLR_HP_BAR_INNER_LIGHT_50 = Color(0xFFf8e038)
private val CLR_HP_BAR_INNER_DARK_25 = Color(0xFFa84048)
private val CLR_HP_BAR_INNER_LIGHT_25 = Color(0xFFf85838)
private val CLR_HP_BAR_INNER_DARK_EMPTY = Color(0xFF484058)
private val CLR_HP_BAR_INNER_LIGHT_EMPTY = Color(0xFF506858)

// Status box config
const val CONFIG_STATUS_BAR_HEIGHT = 100
const val CONFIG_STATUS_BAR_HEIGHT_WITH_TEXT = 125
private const val CONFIG_STATUS_BAR_WIDTH = 320
private const val CONFIG_STATUS_BAR_TEXT_SIZE = 25
private const val CONFIG_STATUS_EFFECT_TEXT_SIZE = CONFIG_STATUS_BAR_TEXT_SIZE - 12
private val CONFIG_STATUS_BAR_FONT = FontFamily(Font(R.font.pokemon_rs_w4))

// HP bar config
private const val CONFIG_HP_BAR_HEIGHT = 20

/**
 * Displays a Pokémon's name, gender, level, HP, and active status condition.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
@Composable
fun PokemonStatusBar(
    pokemon: Pokemon,
    animateHp: Boolean,
    showText: Boolean = false,
    isSelf: Boolean = true
) {
    var hpMax = pokemon.hpMax
    var hpCurrent = pokemon.hpCurrent
    val hpFloat = hpCurrent.toFloat() / hpMax.toFloat()
    val statusBarHeight =
        if(showText) CONFIG_STATUS_BAR_HEIGHT_WITH_TEXT
        else CONFIG_STATUS_BAR_HEIGHT
    val s = if(isSelf) 1 else 0
    val f = if(!isSelf) 1 else 0

    Box (
        // Main status display visual
        modifier = Modifier
            .width(CONFIG_STATUS_BAR_WIDTH.dp)
            .height(statusBarHeight.dp)
            // Outmost border
            .border(
                5.dp, CLR_STATUS_BAR_BACKGROUND_OUTER,
                RoundedCornerShape(
                    topStart = (25 * s).dp,
                    bottomEnd = (25 * s).dp,
                    topEnd = (25 * f).dp,
                    bottomStart = (25 * f).dp
                )
            )
            .padding(5.dp)
            // Inner border
            .border(
                5.dp, CLR_STATUS_BAR_BORDER,
                RoundedCornerShape(
                    topStart = (20 * s).dp,
                    bottomEnd = (20 * s).dp,
                    topEnd = (20 * f).dp,
                    bottomStart = (20 * f).dp
                )
            )
            .padding(5.dp)
            // Border shadow
            .border(
                3.dp, CLR_STATUS_BAR_SHADOW,
                RoundedCornerShape(
                    topStart = (15 * s).dp,
                    bottomEnd = (15 * s).dp,
                    topEnd = (15 * f).dp,
                    bottomStart = (15 * f).dp
                )
            )
            .padding(3.dp)
            // Pads background to avoid clipping
            .border(
                5.dp, CLR_STATUS_BAR_BACKGROUND_INNER,
                RoundedCornerShape(
                    topStart = (10 * s).dp,
                    bottomEnd = (10 * s).dp,
                    topEnd = (10 * f).dp,
                    bottomStart = (10 * f).dp
                )
            )
            .padding(2.dp)
            // Main background
            .background(CLR_STATUS_BAR_BACKGROUND_INNER)
            .padding(start = 10.dp, end = 10.dp)
    ) {
        // Arranges text and HP bar
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.End,
            modifier = Modifier
                .fillMaxSize()
        ) {
            // Display Pokémon name and level
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Top,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Row {
                    // Displays Pokémon name
                    StatusBarText(pokemon.name.uppercase())
                    // Displays Pokémon sex
                    StatusBarText(
                        stringResource(id = pokemon.sex),
                        color = when (pokemon.sex) {
                            R.string.male -> CLR_MARS
                            R.string.female -> CLR_VENUS
                            else -> Color.Transparent
                        },
                        shadowColor =
                            if (pokemon.sex == R.string.genderless) Color.Transparent
                            else CLR_STATUS_BAR_SHADOW
                    )
                }
                // Displays Pokémon level
                StatusBarText("Lv${pokemon.level}")
            }

            // Display Pokémon HP graphic and status effect WITHOUT text
            if(!showText) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Box (modifier = Modifier.fillMaxWidth()){
                        StatusEffectSymbol(statusCondition = pokemon.statusCondition, modifier = Modifier.align(Alignment.CenterStart))
                        PokemonHPBar(pokemon.hpCurrent, pokemon.hpMax, animateHp, 0.75f, modifier = Modifier.align(Alignment.CenterEnd))
                    }
                }
            }
            // Display Pokémon HP graphic and status effect WITH text
             else {
                PokemonHPBar(pokemon.hpCurrent, pokemon.hpMax, animateHp, 0.75f)
                Row (
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.Bottom,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    // Display Pokémon status effect
                    StatusEffectSymbol(statusCondition = pokemon.statusCondition)

                    // Display Pokémon HP text
                    Row(
                        horizontalArrangement = Arrangement.End,
                        verticalAlignment = Alignment.Bottom,
                        modifier = Modifier
                            .fillMaxWidth()
                    ) {
                        StatusBarText(text = "$hpCurrent/${enforceHpTextSpacing(pokemon.hpMax)}")
                    }
                }
            }
        }
    }
}

@Composable
private fun StatusBarText(
    text: String = "",
    textSize: Int = CONFIG_STATUS_BAR_TEXT_SIZE,
    color: Color = CLR_STATUS_BAR_TEXT,
    shadowColor: Color = CLR_STATUS_BAR_SHADOW
) {
    Box {
        TextShadow(
            shadowColor = shadowColor,
            text = text,
            size = textSize,
            fontFamily = CONFIG_STATUS_BAR_FONT
        )
        Text(
            text = text,
            fontSize = textSize.sp,
            color = color,
            fontFamily = CONFIG_STATUS_BAR_FONT
        )
    }
}

@Composable
fun PokemonHPBar(hpCurrent: Int, hpMax: Int, animate: Boolean, width: Float = 1.0f, modifier: Modifier = Modifier) {
    // Animate HP bar transitions
    var targetHpPercentage by remember { mutableStateOf(hpCurrent.toFloat() / hpMax.toFloat())}
    targetHpPercentage = hpCurrent.toFloat() / hpMax.toFloat()
    val hpPercentage by animateFloatAsState(
        targetValue = targetHpPercentage,
        animationSpec =
        if(animate)
            spring(
                dampingRatio = 2f,
                stiffness = Spring.StiffnessLow
            )
        else
            tween(durationMillis = 0)
    )

    // Backmost layer
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth(width)
            .height(CONFIG_HP_BAR_HEIGHT.dp)
            .border(3.dp, CLR_STATUS_BAR_BACKGROUND_OUTER, RoundedCornerShape(10.dp))
            .padding(3.dp)
            .background(CLR_STATUS_BAR_BACKGROUND_OUTER)
            .padding(start = 6.dp)
    ) {
        // HP text
        Text(
            text = "HP",
            fontSize = 12.sp,
            color = CLR_HP_BAR_TEXT,
            fontFamily = FontFamily(Font(R.font.pokemon_hp)),
        )

        // White outer border
        Box(
            modifier = Modifier
                .fillMaxSize()
                .border(3.dp, CLR_HP_BAR_BORDER, RoundedCornerShape(10.dp))
                .padding(3.dp)
        ) {
            Box (
                // Upper half of underlying empty HP bar
                modifier = Modifier
                    .fillMaxSize()
                    .background(CLR_HP_BAR_INNER_DARK_EMPTY)
            ) {
                // Lower half of underlying empty HP bar
                Box(
                    modifier = Modifier
                        .align(Alignment.BottomCenter)
                        .fillMaxWidth()
                        .fillMaxHeight(0.5f)
                        .background(CLR_HP_BAR_INNER_LIGHT_EMPTY)
                )

                // HP bar color
                Column(
                    verticalArrangement = Arrangement.Bottom,
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth(hpPercentage)
                ) {
                    // Upper half of HP bar (darker)
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight(0.5f)
                            .background(
                                when (hpPercentage) {
                                    in 0.5f..1.0f -> CLR_HP_BAR_INNER_DARK
                                    in 0.25f..0.5f -> CLR_HP_BAR_INNER_DARK_50
                                    else -> CLR_HP_BAR_INNER_DARK_25
                                }
                            )
                    )
                    // Lower half of HP bar (lighter)
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(
                                when (hpPercentage) {
                                    in 0.5f..1.0f -> CLR_HP_BAR_INNER_LIGHT
                                    in 0.25f..0.5f -> CLR_HP_BAR_INNER_LIGHT_50
                                    else -> CLR_HP_BAR_INNER_LIGHT_25
                                }
                            )
                    )
                }
            }
        }
    }
}

@Composable
fun StatusEffectSymbol(
    statusCondition: StatusCondition,
    fontSize: Int = CONFIG_STATUS_EFFECT_TEXT_SIZE,
    modifier: Modifier = Modifier,
    padding: Int = 6
) {
    val statusColor = colorResource(id = getStatusColor(statusCondition))
    Box(
        modifier = modifier
            .border(padding.dp, statusColor, RoundedCornerShape(25f))
            .padding(padding.dp)
            .background(statusColor)
            .padding(start = 5.dp, end = 1.5.dp)
    ) {
        Text(
            text = stringResource(id = statusCondition.stringRes),
            color =
                if(statusCondition == StatusCondition.NONE) Color.Transparent
                else CLR_STATUS_BAR_BACKGROUND_INNER,
            fontSize = fontSize.sp,
            fontFamily = FontFamily(Font(R.font.pokemon_status))
        )
    }
}

fun enforceHpTextSpacing(hpMax: Int): String {
    var hpString = ""
    while(hpString.length + hpMax.toString().length < 3)
        hpString += " "
    hpString += hpMax.toString()

    return hpString
}

fun getStatusColor(status: StatusCondition): Int {
    val color = when(status) {
        StatusCondition.NONE -> R.color.status_none
        StatusCondition.BURN -> R.color.status_burn
        StatusCondition.FREEZE -> R.color.status_freeze
        StatusCondition.PARALYSIS -> R.color.status_para
        StatusCondition.SLEEP -> R.color.status_sleep
        else -> R.color.status_poison
    }
    return color
}

@Preview
@Composable
private fun StatusBarPreview() {
    Column {
//        POKEMON_SQUAD.forEach() {
//            PokemonStatusBar(pokemon = it, true)
//            Spacer(modifier = Modifier.height(20.dp))
//        }
        PokemonStatusBar(pokemon = POKEMON_HORSEM, true)
        Spacer(modifier = Modifier.height(20.dp))
        PokemonStatusBar(pokemon = POKEMON_HORSEM, animateHp = true, true)
    }
}