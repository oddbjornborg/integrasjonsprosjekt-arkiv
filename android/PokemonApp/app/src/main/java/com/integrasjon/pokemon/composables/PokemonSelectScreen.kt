package com.integrasjon.pokemon.composables

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.integrasjon.pokemon.Action
import com.integrasjon.pokemon.POKEMON_SQUAD
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.classes.*

// POKEMON BAR theme
private val CLR_BACKGROUND = Color(0xFF889828)
private val CLR_BACKGROUND_STRIPE_LIGHT = Color(0xFFc8d078)
private val CLR_BACKGROUND_STRIPE_DARK = Color(0xFFb0b058)
private val CLR_SELECTED_FRAME = Color(0xFFf87030)
private val CLR_POKEMON_BAR_FILL_LIGHT = Color(0xFF80c0d8)
private val CLR_POKEMON_BAR_FILL_DARK = Color(0xFF3890d8)
private val CLR_POKEMON_BAR_BORDER = Color(0xFF484860)
private val CLR_POKEMON_BAR_TEXT = Color(0xFFf8f8f8)
private val CLR_POKEMON_BAR_TEXT_SHADOW = Color(0xFF707070)

private val CLR_ANIM_BORDER_START = CLR_POKEMON_BAR_FILL_LIGHT
private val CLR_ANIM_BORDER_TARGET = Color(0xFFc0e0ec)
private val CLR_ANIM_FILL_START = CLR_POKEMON_BAR_FILL_DARK
private val CLR_ANIM_FILL_TARGET = Color(0xFF9cc8ec)

// TEXT BOX theme
private val CLR_TEXTBOX_BORDER = Color(0xFFd04838)
private val CLR_TEXTBOX_BACKGROUND_INNER = Color(0xFF68a0a0)
private val CLR_TEXTBOX_BORDER_SHADOW = Color(0xFF818986)


// GENDER SYMBOL theme
private val CLR_MARS = Color(0xFF40c8f8)
private val CLR_MARS_SHADOW = Color(0xFF006090)
private val CLR_VENUS = Color(0xFFf89890)
private val CLR_VENUS_SHADOW = Color(0xFF984038)

// CONFIG
private val CONFIG_POKEMON_BAR_FONT = FontFamily(Font(R.font.pokemon_rs_w4))
private const val CONFIG_POKEMON_BAR_HEIGHT = 80
private const val CONFIG_POKEMON_BAR_TEXT_SIZE = 19
private const val CONFIG_POKEMON_BAR_ICON_SIZE = 90
private const val CONFIG_STATUS_EFFECT_TEXT_SIZE = CONFIG_POKEMON_BAR_TEXT_SIZE - 7

@Composable
fun PokemonSelectScreen(
    pokemonList: List<Pokemon> = POKEMON_SQUAD,
    selectedPokemon: Int = 0,
    onPokeChange: (Action) -> Unit = {}
) {
    // BACKGROUND
    Column (
       modifier = Modifier
           .fillMaxSize()
           .background(CLR_BACKGROUND)
           .offset(x = 60.dp)
//           .padding(start = 0.dp, bottom = 20.dp)
           .border(20.dp, CLR_BACKGROUND, RoundedCornerShape(50.dp))
           .padding(20.dp)
           .background(CLR_BACKGROUND_STRIPE_DARK)
    ) {
        // Striped inner background
        for(i in 0 .. 100) {
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .background(CLR_BACKGROUND_STRIPE_LIGHT))
            Spacer(
                modifier = Modifier.weight(1f)
            )
        }
    }

    // MENU
    Column (
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 10.dp, top = 40.dp, bottom = 40.dp)
            .offset(x = 15.dp)
    ) {
        PokemonBarHolder(
            pokemonList = pokemonList,
            selected = selectedPokemon,
            onConfirm = {
                onPokeChange(Action.values()[it + 4])
            }
        )
    }
}

/**
 * Organizes all the user's Pokémon, and creates a clickable graphic for each of them.
 */
@Composable
fun PokemonBarHolder(
    pokemonList: List<Pokemon>,
    selected: Int,
    onConfirm: (Int) -> Unit
) {
    var currentSelected by remember { mutableStateOf(selected)}

    Column (
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.End,
        modifier = Modifier
            .fillMaxSize()
    ) {
        for(poke in 0 until 6) {
            PokemonBar(
                pokemon = pokemonList[poke],
                modifier =
                    if (poke == currentSelected)
                        Modifier.clickable {
                            SocketSingleton.chooseAction(Action.values()[poke+4])
                            onConfirm(poke)
                        }
                    else if(!pokemonList[poke].isFainted())
                        Modifier.clickable { currentSelected = poke }
                    else
                        Modifier,
                selected = poke == currentSelected
            )
        }
    }
}

/**
 * A single clickable Pokémon graphic.
 *
 * @see PokemonBarHolder
 */
@Composable
fun PokemonBar(
    pokemon: Pokemon,
    modifier: Modifier = Modifier,
    selected: Boolean = false,
    isFainted: Boolean = false
) {
    // ANIMATION
    val selectedBarAnim = rememberInfiniteTransition()
    val selectedAnimSpec: InfiniteRepeatableSpec<Color> = infiniteRepeatable(
        tween(durationMillis = 500, easing = LinearEasing),
        repeatMode = RepeatMode.Reverse
    )
    val animColorFill = selectedBarAnim.animateColor(
        initialValue = CLR_ANIM_FILL_START,
        targetValue = CLR_ANIM_FILL_TARGET,
        selectedAnimSpec
    )
    val animColorBorder = selectedBarAnim.animateColor(
        initialValue = CLR_ANIM_BORDER_START,
        targetValue = CLR_ANIM_BORDER_TARGET,
        selectedAnimSpec
    )


    Box (
        contentAlignment = Alignment.Center,
        modifier = modifier.height(CONFIG_POKEMON_BAR_ICON_SIZE.dp)
    ){
        // Bar visual body
        Row (
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.Bottom,
            modifier = Modifier
                .fillMaxWidth()
                .height(CONFIG_POKEMON_BAR_HEIGHT.dp)
                .border(
                    5.dp,
                    if (selected) CLR_SELECTED_FRAME else Color.Transparent,
                    RoundedCornerShape(topStartPercent = 50, bottomStartPercent = 50)
                )
                .padding(5.dp)
                .border(
                    5.dp, CLR_POKEMON_BAR_BORDER,
                    RoundedCornerShape(topStartPercent = 50, bottomStartPercent = 50)
                )
                .padding(5.dp)
                .border(
                    5.dp,
                    if (selected) animColorBorder.value else CLR_POKEMON_BAR_FILL_LIGHT,
                    RoundedCornerShape(topStartPercent = 50, bottomStartPercent = 50)
                )
                .padding(5.dp)
                .background(
                    when {
                        selected -> animColorFill.value
                        isFainted -> Color.Gray
                        else -> CLR_POKEMON_BAR_FILL_DARK
                    }
                )
        ) {
            Spacer(modifier = Modifier.width((CONFIG_POKEMON_BAR_ICON_SIZE).dp))

            // Pokemon name, level and sex
            Column (
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.Start,
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(0.48f)
                    .offset(x = (-5).dp)
            ) {
                // Display Pokémon name
                PokemonBarText(pokemon.name.uppercase())
                Row (
                    horizontalArrangement = Arrangement.SpaceAround,
                    verticalAlignment = Alignment.Bottom,
                    modifier = Modifier
                        .fillMaxSize()
                ){
                    // Display Pokémon level
                    PokemonBarText("Lv${pokemon.level}")
                    // Display Pokémon sex
                    GenderSymbol(pokemon.sex)
                }
            }

            // Display HP information
            Column (
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.End,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 5.dp)
            ) {
                // Display Pokémon HP graphic
                PokemonHPBar(pokemon.hpCurrent, pokemon.hpMax, false)
                Spacer(modifier = Modifier.height(2.dp))
                // Display Pokémon HP text
                Row (
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.Bottom,
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    // Display Pokémon status effect
                    Box (
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.fillMaxHeight())
                    {
                        StatusEffectSymbol(
                            statusCondition = pokemon.statusCondition,
                            fontSize = CONFIG_STATUS_EFFECT_TEXT_SIZE,
                            padding = 4
                        )
                    }
                    PokemonBarText(
                        text = "${pokemon.hpCurrent}/${enforceHpTextSpacing(pokemon.hpMax)}"
                    )
                }
                
            }
        }

        // Display Pokémon icon
        Box (
            contentAlignment = Alignment.CenterStart,
            modifier = Modifier
                .fillMaxSize()
                .align(Alignment.Center)
        ) {
            PokemonIcon(
                painterResource(id = pokemon.sprite),
                false,
                modifier = Modifier
                    .size(CONFIG_POKEMON_BAR_ICON_SIZE.dp)
                    .offset(y = (-5).dp)    // Makes icons appear slightly above center of bar
            )
        }
    }
}

@Composable
private fun PokemonBarText(
    text: String = "", 
    textSize: Int = CONFIG_POKEMON_BAR_TEXT_SIZE,
    textColor: Color = CLR_POKEMON_BAR_TEXT,
    shadowColor: Color = CLR_POKEMON_BAR_TEXT_SHADOW
) {
    Box {
        TextShadow(
            shadowColor = shadowColor,
            text = text,
            size = textSize,
            offset = 2.0f,
            fontFamily = CONFIG_POKEMON_BAR_FONT
        )
        Text(
            text = text,
            fontSize = textSize.sp,
            color = textColor,
            fontFamily = CONFIG_POKEMON_BAR_FONT
        )
    }
}

@Composable
private fun GenderSymbol(stringId: Int) {
    PokemonBarText(
        text = stringResource(id = stringId),
        textColor = when (stringId) {
            R.string.male -> CLR_MARS
            R.string.female -> CLR_VENUS
            else -> Color.Transparent
        },
        shadowColor = when (stringId) {
            R.string.male -> CLR_MARS_SHADOW
            R.string.female -> CLR_VENUS_SHADOW
            else -> Color.Transparent
        }
    )
}

// TODO: [LOW] Make unique text box for Confirm and Summary buttons
@Composable
fun PokemonSelectTextBox(text: String, modifier: Modifier = Modifier) {
    val splitText = text.split("\n")
    Column (
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.Start,
        modifier = modifier
//            .fillMaxWidth()
//            .background(CLR_TEXTBOX_BACKGROUND_OUTER)
//            .padding(top = 10.dp, bottom = 5.dp, start = 2.dp, end = 2.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(start = 5.dp, end = 5.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(start = 5.dp, end = 5.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(4.8.dp)
            .border(2.2.dp, CLR_TEXTBOX_BORDER_SHADOW, RoundedCornerShape(10.dp))
            .background(CLR_TEXTBOX_BACKGROUND_INNER)
            .padding(start = 10.dp, end = 5.dp, bottom = 5.dp)
    ) {
        splitText.forEach() { TextBoxText(text = it) }
    }
}

@Preview
@Composable
private fun SelectScreenPreview() {
    PokemonSelectScreen()
}