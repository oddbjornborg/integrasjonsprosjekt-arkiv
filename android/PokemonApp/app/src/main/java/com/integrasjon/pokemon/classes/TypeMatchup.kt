package com.integrasjon.pokemon.classes

import com.integrasjon.pokemon.Type

/**
 * Has one instance for each elemental type present in the game logic. Stores data about what
 * types a type are weak to, strong against, and immune against.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
class TypeMatchup(
    private val immune: IntArray = intArrayOf(),
    private val weak: IntArray = intArrayOf(),
    private val strong: IntArray = intArrayOf()
) {
    // Matchup for single type
    fun matchup(type: Type): Float {
        if(strong.any {it == type.stringRes})
            return 2.0f
        if(weak.any {it == type.stringRes})
            return 0.5f
        if(immune.any {it == type.stringRes})
            return 0.0f
        return 1.0f
    }

    // Matchup for list of types
    fun matchup(types: List<Type>): Float {
        var multiplier = 1f
        types.forEach {
            multiplier *= matchup(it)
        }
        return multiplier
    }
}