package com.integrasjon.pokemon.classes

import com.integrasjon.pokemon.Action

class User(
    val id: Int,
    val name: String,
    val socketID: String? = null,
    val teamIds: MutableList<Int> = mutableListOf(),
    var team: List<Pokemon>? = null,
    var nextAction: Action = Action.WAITING
) {
    // The index of the Pokémon from the team that is currently on the battlefield
    var activePokemon = 0

    fun getActivePokemon(): Pokemon { return team!![activePokemon] }
}