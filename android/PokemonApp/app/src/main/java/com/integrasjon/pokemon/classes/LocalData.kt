package com.integrasjon.pokemon.classes

import com.integrasjon.pokemon.*
import org.json.JSONObject

/**
 * Object that fetches and keeps data necessary to play a game of Pokémon.
 *
 * Keeps data about both users, their team of Pokémon, and the Pokémon's moves. Additionally also
 * keeps track of the current randomly generated numbers used to determine chance-based situations.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
object LocalData {
    var users: MutableList<User> = mutableListOf()
    var moves: MutableMap<Int, Move> = mutableMapOf()
    var rng: JSONObject? = null

    // FETCH FUNCTIONS

    fun fetchUser(user: Int, socket: String) {
        val userData = HttpSingleton.fetchUserRequest(user)
        userData?.let { res ->
            val pokemon = mutableListOf(
                res.poke1,
                res.poke2,
                res.poke3,
                res.poke4,
                res.poke5,
                res.poke6
            )
            users.add(User(
                id = res.id,
                name = res.username,
                socketID = socket,
                teamIds = pokemon
            ))
        }
    }

    fun fetchPokemon(isOwnTeam: Boolean) {
        lateinit var pokemonData: JSONPokemonResponse
        val user = if (isOwnTeam) getSelf() else getFoe()
        val response = HttpSingleton.fetchPokemonRequest(user)

        // If pokemonData NOT NULL, do:
        response?.let { pokemonArray ->
            val team = mutableListOf<Pokemon>()

            // For each row in result
            user.teamIds.forEach { pokemonId ->
                pokemonArray.forEach { if(it.id == pokemonId) pokemonData = it }
                println(pokemonData.toString())

                // Convert types from string to enum
                val types = mutableListOf( typeByString(pokemonData.type_primary)!! )
                if(pokemonData.type_secondary != "none") types.add(typeByString(pokemonData.type_secondary)!!)

                val pokemon = Pokemon(
                    name = pokemonData.name,
                    hpMax = pokemonData.hp,
                    attack = pokemonData.attack,
                    defense = pokemonData.defense,
                    spAttack = pokemonData.special_attack,
                    spDefense = pokemonData.special_defense,
                    speed = pokemonData.speed,
                    types = types.toList(),
                    sprite = R.drawable.dragonsect,     // TODO: GET IMAGE FROM DB [MEDIUM]
                    sex = when(pokemonData.sex) {
                        "male" -> R.string.male
                        "female" -> R.string.female
                        else -> R.string.genderless
                    },
                    moveIds = mutableListOf(
                        pokemonData.move_first,
                        pokemonData.move_second,
                        pokemonData.move_third,
                        pokemonData.move_fourth
                    )
                )
                team.add(pokemon)
            }

            user.team = team.toList()
            println("Set team for ${user.name}")
        } ?: println("-- POKEMON DATA NOT FOUND --")
    }

    /**
     * Fetches all moves necessary for the game, based on the Pokémon in it.
     *
     * @see getAllMoveIds
     */
    fun fetchMoves() {
        val moveData = HttpSingleton.fetchMovesRequest(getAllMoveIds())
        moveData!!.forEach { data ->
            println("Fetching ${data.name}")
            // True when move not present in map
            if(!moves.containsKey(data.id)) {
                // Create move and add to map
                moves[data.id] = Move(
                    name = data.name,
                    type = typeByString(data.type)!!,
                    power = data.power,
                    isPhysical = data.isPhysical == 1,
                    ppMax = data.pp,
                    accuracy = data.accuracy,
                    priority = data.priority,
                    critRatio = data.critRatio,
                    description = data.description,
                    animFlag = animationByString(data.anim)!!,
                    animTarget = targetByString(data.animTarget)!!
                )
                // Create move effect if any
                if (data.effectTarget != null)
                    moves[data.id]!!.moveEffects.add(createMoveEffect(data))
            }
            // Duplicate move data; add move effect to existing move
            else {
                if(data.effectTarget != null) moves[data.id]!!.moveEffects.add(createMoveEffect(data))
            }
        }
    }

    /**
     * Assigns moves from the move map to Pokémon based on the Pokémon's list of move ids.
     */
    fun assignMovesToPokemon() {
        println("USER COUNT: ${users.size}")
        users.forEach { user ->
            user.team?.let {
                println("-- TEAM FOUND FOR USER ${user.name.uppercase()} --")
                it.forEach { pokemon ->
                    pokemon.moveIds.forEach { move ->
                        pokemon.moveList.add(moves[move]!!.clone())
                    }
                }
            } ?: println("-- TEAM NOT FOUND FOR USER ${user.name.uppercase()} --")
        }
    }

    // HELPER FUNCTIONS

    fun getSelf(): User { return users[0] }

    fun getFoe(): User { return users[1] }

    /**
     * Creates a set of every move of all Pokémon in the lobby, without duplicates.
     */
    private fun getAllMoveIds(): List<Int> {
        val moveList = mutableSetOf<Int>()
        if(users.size < 2) {
            println("-- TOO FEW USERS --")
        }
        else {
            users.forEach { user ->
                user.team?.forEach { pokemon ->
                    moveList.addAll(pokemon.moveIds)
                }
            }
            println("MoveID set [All]: $moveList")
        }
        return moveList.toList()
    }

    /**
     * Creates a move effect from a database row.
     *
     * @see parseStats
     * @see statusByString
     * @see targetByString
     * @see animationByString
     */
    private fun createMoveEffect(moveEffect: JSONMoveResponse): MoveEffect {
        return MoveEffect(
            target = targetByString(moveEffect.effectTarget!!)!!,
            applyChance = moveEffect.applyChance!!,
            statusApplied = statusByString(moveEffect.statusApplied)
                ?: StatusCondition.NONE,
            statsModified = parseStats(moveEffect.statsModified!!),
            modifiedValue = moveEffect.modifiedValue ?: 0,
            animFlag = animationByString(moveEffect.effectAnim) ?: Animation.NONE
        )
    }

    /**
     * Parses affected stats for move effects that apply stat stages.
     *
     * Affected stats are stored as an array of seven chars in the database, representing the
     * seven different stats a Pokémon has. The value of the chars are 1 for affected stats, and
     * 0 for unaffected stats.
     *
     * @see createMoveEffect
     */
    private fun parseStats(stats: String): List<Int> {
        val statList = mutableListOf<Int>()
        for(i in (stats.toList().indices)) {
            if(stats[i].digitToInt() == 1) statList.add(i)
        }
        return statList.toList()
    }

    /**
     * Gets Type enum from a string.
     */
    private fun typeByString(key: String): Type? {
        var type: Type? = null
        Type.values().forEach {
            if( key == it.string ) type = it
        }
        return type
    }

    /**
     * Gets Animation enum from a string.
     */
    private fun animationByString(key: String?): Animation? {
        key.let {
            Animation.values().forEach { animation ->
                if(animation.name.lowercase() == key) return animation
            }
        }
        return null
    }

    /**
     * Gets AnimationTarget enum from a string.
     */
    private fun targetByString(key: String): AnimationTarget? {
        AnimationTarget.values().forEach {
            if(it.name.lowercase() == key) return it
        }
        return null
    }

    /**
     * Gets StatusCondition enum from a string.
     */
    private fun statusByString(key: String?): StatusCondition? {
        key?.let {
            StatusCondition.values().forEach { status ->
                if(status.name.lowercase() == it) return status
            }
        }
        return null
    }

    /**
     * Gets Action enum from a string.
     */
    private fun actionByString(key: String?): Action? {
        key?.let {
            Action.values().forEach { action ->
                if(action.name.lowercase() == it) return action
            }
        }
        return null
    }

    /**
     * Gets data necessary to perform a single turn.
     *
     * This includes the rng of both players, and their chosen actions.
     */
    fun getTurnData(data: JSONObject) {
        println(data)

        rng = data.get("rng") as JSONObject

        getSelf().nextAction =
            if(SocketSingleton.isHost()) actionByString(data.get("Host_Action") as String)!!
            else actionByString(data.get("Player_Action") as String)!!

        getFoe().nextAction =
            if(!SocketSingleton.isHost()) actionByString(data.get("Host_Action") as String)!!
            else actionByString(data.get("Player_Action") as String)!!
    }
}