package com.integrasjon.pokemon.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.integrasjon.pokemon.Action
import com.integrasjon.pokemon.POKEMON_MAROTOISE
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.classes.Move
import com.integrasjon.pokemon.classes.SocketSingleton

private val CLR_TEXTBOX_BACKGROUND_OUTER = Color(0xFF48424d)

// FIGHTMENU theme
private val CLR_FIGHTMENU_BORDER_OUTER = Color(0xFF283030)
private val CLR_FIGHTMENU_BORDER_INNER = Color(0xFF706880)
private val CLR_FIGHTMENU_BACKGROUND = Color(0xFFeff8eb)
private val CLR_FIGHTMENU_TEXT = Color(0xFF48424d)
private val CLR_FIGHTMENU_TEXT_SELECTED = Color.Red
private val CLR_FIGHTMENU_TEXT_SHADOW = Color(0xFFc9c6c4)

// CONFIG
private const val CONFIG_BOTTOM_MENU_HEIGHT = 150
private const val CONFIG_FIGHTMENU_TEXT_SIZE = 36

@Composable
fun MoveSelect(
    moveList: List<Move>,
    modifier: Modifier = Modifier,
    onMoveChosen: (Action) -> Unit = {}
) {
    var moveSelected by remember { mutableStateOf(0) }

    Box (modifier = Modifier.fillMaxSize()) {
        Column (
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = moveListModifier
        ) {
            // Display move list
            for(move in 0 .. 3) {
                Row {
                    Box(
                        contentAlignment = Alignment.CenterStart,
                        modifier = Modifier.width(25.dp)
                    ) {
                        if(move == moveSelected) MoveSelectText(text = "*")
                    }
                    MoveSelectText(
                        text = moveList[move].name.uppercase(),
                        modifier =
                            if(move == moveSelected)
                                Modifier.clickable { onMoveChosen(Action.values()[move]) }
                            else
                                Modifier.clickable { moveSelected = move }
                    )
                    Spacer(modifier = Modifier.width(25.dp))
                }
            }
        }

        Column (
            // Main design
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .height(CONFIG_BOTTOM_MENU_HEIGHT.dp)
                .background(CLR_TEXTBOX_BACKGROUND_OUTER)
                .padding(top = 5.dp, bottom = 0.dp, start = 2.dp, end = 2.dp)
                .border(5.dp, CLR_FIGHTMENU_BORDER_OUTER, RoundedCornerShape(10.dp))
                .padding(5.dp)
                .border(10.dp, CLR_FIGHTMENU_BORDER_INNER, RoundedCornerShape(5.dp))
                .padding(10.dp)
                .border(2.dp, CLR_FIGHTMENU_TEXT_SHADOW)
                .background(CLR_FIGHTMENU_BACKGROUND)
        ) {
            // Display current and max PP
            Row (
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .padding(start = 10.dp, end = 5.dp)
                    .fillMaxWidth()
            ) {
                FightMenuText(text = "PP")
                FightMenuText(text =
                "${moveList[moveSelected].ppCurrent}/${moveList[moveSelected].ppMax}")
            }

            // Display move typing
            Row (
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .padding(start = 10.dp, end = 5.dp)
                    .fillMaxWidth()
            ) {
                FightMenuText(
                    text = "TYPE/${stringResource(id = moveList[moveSelected].type.stringRes)}",
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    }
}

@Composable
fun MoveSelectText (
    text: String,
    modifier: Modifier = Modifier,
    selected: Boolean = false
) {
    Row (modifier = modifier) {
        Box {
            TextShadow(
                shadowColor = CLR_FIGHTMENU_TEXT_SHADOW,
                text = text,
                size = CONFIG_FIGHTMENU_TEXT_SIZE,
                fontFamily = FontFamily(Font(R.font.pokemon_rs_w5))
            )
            Text(
                text = text,
                fontSize = CONFIG_FIGHTMENU_TEXT_SIZE.sp,
                color = if(selected) CLR_FIGHTMENU_TEXT_SELECTED else CLR_FIGHTMENU_TEXT,
                fontFamily = FontFamily(Font(R.font.pokemon_rs_w5))
            )
        }
    }
}

private val moveListModifier = Modifier
    .fillMaxSize()
    .padding(bottom = CONFIG_BOTTOM_MENU_HEIGHT.dp)
    .background(CLR_TEXTBOX_BACKGROUND_OUTER)
    .padding(top = 5.dp, bottom = 0.dp, start = 2.dp, end = 2.dp)
    .border(5.dp, CLR_FIGHTMENU_BORDER_OUTER, RoundedCornerShape(10.dp))
    .padding(5.dp)
    .border(10.dp, CLR_FIGHTMENU_BORDER_INNER, RoundedCornerShape(5.dp))
    .padding(10.dp)
    .border(2.dp, CLR_FIGHTMENU_TEXT_SHADOW)
    .background(CLR_FIGHTMENU_BACKGROUND)

@Preview
@Composable
fun MoveSelectPreview() {
    MoveSelect(POKEMON_MAROTOISE.moveList)
}