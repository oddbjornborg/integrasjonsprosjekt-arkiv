package com.integrasjon.pokemon.classes

import android.util.Log
import com.google.gson.JsonObject
import com.integrasjon.pokemon.Action
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.coroutines.runBlocking
import org.json.JSONObject

/**
 * Object in charge of storing socket information and performing socket operations.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
object SocketSingleton {
    lateinit var socket: Socket
    var iAmHost = false
    var madeMove = false

    fun createSocketConnection() {
        runBlocking {
            socket = IO.socket("http://10.0.2.2:3000")
            Log.println(Log.VERBOSE, "net/socket", "Socket: $socket")

            socket.connect()
            while (socket.id() == null) {
                Thread.sleep(250)
            }

            Log.println(Log.VERBOSE, "net/socket", "Socket ID: ${socket.id()}")
        }
    }

    fun closeConnection() {
        socket.disconnect()
    }

    /**
     * Creates a lobby in the database and fetches own user data.
     *
     * @see HttpSingleton.createGameRequest
     * @see LocalData.fetchUser
     */
    fun createLobby(isPrivate: Boolean = false) {
        runBlocking {
            socket.id()?.let {
                // Create lobby
                HttpSingleton.createGameRequest("test-lobby", isPrivate, it)

                // Fetch own data
                LocalData.fetchUser(HttpSingleton.loginId!!, it) // Potentially risky !!
            }

            // Set self as host
            iAmHost = true

            // Emit room creation
            socket.emit("createRoom", socket.id())
            Thread.sleep(100)
            readyUp()
        }
    }

    /**
     * Gets data about both users and their Pokémon upon entering a game as the host.
     *
     * @see getGameData
     */
    fun hostGame() {
        Log.println(Log.VERBOSE, "http", "-- Creating game")
        runBlocking {
            getGameData()
        }
    }

    /**
     * Finds an existing lobby from the database to join.
     *
     * Fetches own user data upon running function, and host's user data upon finding a game to
     * join.
     *
     * @see LocalData.fetchUser
     * @see HttpSingleton.findGameRequest
     */
    fun findGame() {
        runBlocking {
            this.runCatching {  }
            LocalData.fetchUser(HttpSingleton.loginId!!, socket.id())
            val data = HttpSingleton.findGameRequest()
            data?.let {
                val parameters = JSONObject()
                    .put("room", it.lobby)
                    .put("user", it.user)

                socket.emit("joinGame", parameters)
                LocalData.fetchUser(it.host_id, it.host_socket)
            }
        }
    }

    /**
     * Gets data about both users and their Pokémon upon joining a game as a challenger.
     *
     * @see getGameData
     */
    fun joinGame() {
        Log.println(Log.VERBOSE, "http", "-- Joining game")
        runBlocking {
            getGameData()

            socket.on("private-message") { msg ->
                println("[Socket] Message received: $msg")
                Log.println(Log.VERBOSE, "http", "PM received: $msg")
            }
        }
    }

    /**
     * Gets data about both user's Pokémon and their moves.
     *
     * @see LocalData.fetchPokemon
     * @see LocalData.fetchMoves
     * @see LocalData.assignMovesToPokemon
     */
    private fun getGameData() {
        LocalData.fetchPokemon(isOwnTeam = true)
        LocalData.fetchPokemon(isOwnTeam = false)
        LocalData.fetchMoves()
        LocalData.assignMovesToPokemon()
    }


    fun readyUp() {
        val parameters = JSONObject()
            .put("isHost", iAmHost)
        socket.emit("im-ready", parameters)
    }

    /**
     * Sends a socket emit communicating the player's chosen action. Either performing a move or
     * swapping to a different Pokémon.
     */
    fun chooseAction(action: Action) {
        println("Chosen action: ${action.name}")

        val mParameters = JSONObject()
            .put("isHost", iAmHost)
            .put("user", LocalData.getSelf().id)
            .put("action", action.ordinal)

        socket.emit("choose action", mParameters)

        madeMove = true
    }

    // Not implemented
    fun swapOnFaint(pokemon: Int) {
        val mParameters = JSONObject()
            .put("pokemon", pokemon)

        socket.emit("swap on faint", mParameters)
    }

    fun isHost(): Boolean { return iAmHost }
}