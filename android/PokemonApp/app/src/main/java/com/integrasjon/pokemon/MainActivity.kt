package com.integrasjon.pokemon

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.integrasjon.pokemon.composables.*
import com.integrasjon.pokemon.classes.*

// Predefined pokemon teams are only used if no teams can be retrieved from the db.
// Largely deprecated.
private val gPokeListSelf = listOf(
    POKEMON_BULBTO.clone(),
    POKEMON_HORSEM.clone(),
    POKEMON_MAROTOISE.clone(),
    POKEMON_MAROTOISE.clone(),
    POKEMON_MAROTOISE.clone(),
    POKEMON_MAROTOISE.clone()
)
private val gPokeListFoe = listOf(
    POKEMON_HORSEM.clone(),
    POKEMON_MAROTOISE.clone(),
    POKEMON_BULBTO.clone()
)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SocketSingleton.createSocketConnection()

        setContent {
            MainScreen()
        }
    }
}

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    Box (
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        MainMenu(navController = navController)
        NavHost(navController = navController, startDestination = "main_menu") {
            composable("main_menu") { MainMenu(navController = navController) }
            composable("pregame_lobby") { PregameLobby(navController) }
            composable("battle_screen") {
                BattleScreen(
                    pokeListSelf =
                        if(LocalData.getSelf().team != null) LocalData.getSelf().team!!.toList()
                        else gPokeListSelf,
                    pokeListFoe =
                        if(LocalData.getFoe().team != null) LocalData.getFoe().team!!.toList()
                        else gPokeListFoe
                )
            }
            composable("pokemon_select") { PokemonSelectScreen() }
            composable("login_screen") { LoginScreen(navController) }
            composable("register_screen") { RegisterScreen(navController)}
        }
    }
}

@Preview
@Composable
fun MainPreview() {
    MainMenu(navController = rememberNavController())
}
