package com.integrasjon.pokemon

import androidx.annotation.StringRes
import com.integrasjon.pokemon.classes.TypeMatchup

enum class Action {
    MOVE1,
    MOVE2,
    MOVE3,
    MOVE4,
    POKEMON_SWAP1,
    POKEMON_SWAP2,
    POKEMON_SWAP3,
    POKEMON_SWAP4,
    POKEMON_SWAP5,
    POKEMON_SWAP6,
    WAITING,
    READY
}

// NB! Don't mess with the order!
enum class Animation {
    NONE,
    ATTACK,
    BUFF,
    DEBUFF,
    FAINT,
    PARALYSIS,
    BURN,
    POISON,
    SLEEP,
    FREEZE
}

enum class AnimationTarget {
    SELF, FOE
}

enum class Stat (@StringRes val stringRes: Int) {
    ATTACK(R.string.stat_attack),
    DEFENSE(R.string.stat_defense),
    SPECIAL_ATTACK(R.string.stat_special_attack),
    SPECIAL_DEFENSE(R.string.stat_special_defense),
    SPEED(R.string.stat_speed),
    ACCURACY(R.string.stat_accuracy),
    EVASION(R.string.stat_evasion);
}

enum class StatusCondition (@StringRes val stringRes: Int) {
    BURN(R.string.status_burn),
    FREEZE(R.string.status_freeze),
    PARALYSIS(R.string.status_para),
    POISON(R.string.status_poison),
    BAD_POISON(R.string.status_bad_poison),
    SLEEP(R.string.status_sleep),
    NONE(R.string.status_none);
}

enum class Gender (@StringRes val stringRes: Int) {
    MALE(R.string.male),
    FEMALE(R.string.female),
    GENDERLESS(R.string.genderless)
}

enum class Type (@StringRes val stringRes: Int, val string: String) {
    NORMAL(R.string.type_normal, "normal"),
    FIGHT(R.string.type_fight, "fight"),
    FLYING(R.string.type_flying, "flying"),
    POISON(R.string.type_poison, "poison"),
    GROUND(R.string.type_ground, "ground"),
    ROCK(R.string.type_rock, "rock"),
    BUG(R.string.type_bug, "bug"),
    GHOST(R.string.type_ghost, "ghost"),
    STEEL(R.string.type_steel, "steel"),
    FIRE(R.string.type_fire, "fire"),
    WATER(R.string.type_water, "water"),
    GRASS(R.string.type_grass, "grass"),
    ELECTRIC(R.string.type_electric, "electric"),
    PSYCHIC(R.string.type_psychic, "psychic"),
    ICE(R.string.type_ice, "ice"),
    DRAGON(R.string.type_dragon, "dragon"),
    DARK(R.string.type_dark, "dark");

    fun getByOrdinal(id: Int): Type {
        return values()[id]
    }
    fun getByString(key: String): Type? {
        var type: Type? = null
        values().forEach {
            if( key == it.string ) type = it
        }
        return type
    }
}

object Constants {
    // Stats
    const val STAT_ATTACK = 0
    const val STAT_SPECIAL_ATTACK = 1
    const val STAT_DEFENSE = 2
    const val STAT_SPECIAL_DEFENSE = 3
    const val STAT_SPEED = 4
    const val STAT_ACCURACY = 5
    const val STAT_EVASION = 6
    val STAT_TO_STRING = mapOf(
        0 to "attack",
        1 to "special attack",
        2 to "defense",
        3 to "special defense",
        4 to "speed",
        5 to "accuracy",
        6 to "evasion"
    )

    // Text box string
    const val TEXT_MISS = "But it missed!"
    const val TEXT_SUPER_EFFECTIVE = "It's super effective!"
    const val TEXT_NOT_EFFECTIVE = "It's not very effective..."
    const val TEXT_CRITICAL = "A critical hit!"
    const val TEXT_FAIL = "But it failed!"

    // Type effectiveness map
    val TYPE_MAP = mapOf(
        R.string.type_normal to TypeMatchup(
            immune = intArrayOf(
                R.string.type_ghost
            ),
            weak = intArrayOf(
                R.string.type_rock,
                R.string.type_steel
            )
        ),
        R.string.type_fight to TypeMatchup(
            immune = intArrayOf(
                R.string.type_ghost
            ),
            weak = intArrayOf(
                R.string.type_flying,
                R.string.type_poison,
                R.string.type_bug,
                R.string.type_psychic
            ),
            strong = intArrayOf(
                R.string.type_normal,
                R.string.type_rock,
                R.string.type_steel,
                R.string.type_ice,
                R.string.type_dark
            )
        ),
        R.string.type_flying to TypeMatchup(
            weak = intArrayOf(
                R.string.type_rock,
                R.string.type_steel,
                R.string.type_electric
            ),
            strong = intArrayOf(
                R.string.type_fight,
                R.string.type_bug,
                R.string.type_grass
            )
        ),
        R.string.type_poison to TypeMatchup(
            immune = intArrayOf(
                R.string.type_steel
            ),
            weak = intArrayOf(
                R.string.type_poison,
                R.string.type_ground,
                R.string.type_rock,
                R.string.type_ghost
            ),
            strong = intArrayOf(
                R.string.type_grass
            )
        ),
        R.string.type_ground to TypeMatchup(
            immune = intArrayOf(
                R.string.type_flying
            ),
            weak = intArrayOf(
                R.string.type_bug,
                R.string.type_grass
            ),
            strong = intArrayOf(
                R.string.type_poison,
                R.string.type_rock,
                R.string.type_steel,
                R.string.type_fire,
                R.string.type_electric
            )
        ),
        R.string.type_rock to TypeMatchup(
            weak = intArrayOf(
                R.string.type_fight,
                R.string.type_ground,
                R.string.type_steel
            ),
            strong = intArrayOf(
                R.string.type_flying,
                R.string.type_bug,
                R.string.type_fire,
                R.string.type_ice
            )
        ),
        R.string.type_bug to TypeMatchup(
            weak = intArrayOf(
                R.string.type_fight,
                R.string.type_flying,
                R.string.type_poison,
                R.string.type_ghost,
                R.string.type_steel,
                R.string.type_fire
            ),
            strong = intArrayOf(
                R.string.type_grass,
                R.string.type_psychic
            )
        ),
        R.string.type_ghost to TypeMatchup(
            immune = intArrayOf(
                R.string.type_normal
            ),
            weak = intArrayOf(
                R.string.type_steel,
                R.string.type_dark
            ),
            strong = intArrayOf(
                R.string.type_ghost,
                R.string.type_psychic
            )
        ),
        R.string.type_steel to TypeMatchup(
            weak = intArrayOf(
                R.string.type_steel,
                R.string.type_fire,
                R.string.type_water,
                R.string.type_electric
            ),
            strong = intArrayOf(
                R.string.type_rock,
                R.string.type_ice
            )
        ),
        R.string.type_fire to TypeMatchup(
            weak = intArrayOf(
                R.string.type_rock,
                R.string.type_fire,
                R.string.type_water,
                R.string.type_dragon
            ),
            strong = intArrayOf(
                R.string.type_bug,
                R.string.type_steel,
                R.string.type_grass,
                R.string.type_ice
            )
        ),
        R.string.type_water to TypeMatchup(
            weak = intArrayOf(
                R.string.type_water,
                R.string.type_grass,
                R.string.type_dragon
            ),
            strong = intArrayOf(
                R.string.type_ground,
                R.string.type_rock,
                R.string.type_fire
            )
        ),
        R.string.type_grass to TypeMatchup(
            weak = intArrayOf(
                R.string.type_flying,
                R.string.type_poison,
                R.string.type_bug,
                R.string.type_steel,
                R.string.type_fire,
                R.string.type_grass,
                R.string.type_dragon
            ),
            strong = intArrayOf(
                R.string.type_ground,
                R.string.type_rock,
                R.string.type_water
            )
        ),
        R.string.type_electric to TypeMatchup(
            immune = intArrayOf(
                R.string.type_ground
            ),
            weak = intArrayOf(
                R.string.type_grass,
                R.string.type_electric,
                R.string.type_dragon
            ),
            strong = intArrayOf(
                R.string.type_flying,
                R.string.type_water
            )
        ),
        R.string.type_psychic to TypeMatchup(
            immune = intArrayOf(
                R.string.type_dark
            ),
            weak = intArrayOf(
                R.string.type_steel,
                R.string.type_psychic
            ),
            strong = intArrayOf(
                R.string.type_fight,
                R.string.type_poison
            )
        ),
        R.string.type_ice to TypeMatchup(
            weak = intArrayOf(
                R.string.type_steel,
                R.string.type_fire,
                R.string.type_water,
                R.string.type_ice
            ),
            strong = intArrayOf(
                R.string.type_flying,
                R.string.type_ground,
                R.string.type_grass,
                R.string.type_dragon
            )
        ),
        R.string.type_dragon to TypeMatchup(
            weak = intArrayOf(
                R.string.type_steel
            ),
            strong = intArrayOf(
                R.string.type_dragon
            )
        ),
        R.string.type_dark to TypeMatchup(
            weak = intArrayOf(
                R.string.type_fight,
                R.string.type_steel,
                R.string.type_dark
            ),
            strong = intArrayOf(
                R.string.type_ghost,
                R.string.type_psychic
            )
        )
    )
}