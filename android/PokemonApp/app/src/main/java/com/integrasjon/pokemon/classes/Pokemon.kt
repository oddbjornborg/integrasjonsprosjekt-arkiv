package com.integrasjon.pokemon.classes

import android.util.Log
import com.integrasjon.pokemon.*
import com.integrasjon.pokemon.Constants.STAT_TO_STRING
import org.json.JSONObject
import java.util.*

class Pokemon(
    val name: String,
    val hpMax: Int,
    val attack: Int,
    val defense: Int,
    val spAttack: Int,
    val spDefense: Int,
    val speed: Int,
    val types: List<Type>,
    val sprite: Int,
    val sex: Int,
    val moveIds: List<Int>,
    var moveList: MutableList<Move> = mutableListOf(),
    var hpCurrent: Int = hpMax,
    var statusCondition: StatusCondition = StatusCondition.NONE,
    private var auxiliaryStatus: MutableList<Int> = mutableListOf() // Not used much atm
) {
    // *-- INIT --* //

    val level = 50
    var statStages = IntArray(7)
    private var statusCounter = 0
    private var statusDuration = 0

    // *-- MAIN POKEMON FUNCTIONALITY --* //

    /**
     * Performs a move from the Pokémon's move list.
     *
     * @see Move.use
     * @author Oddbjørn S. Borge-Jensen
     */
    fun useMove(slot: Int, target: Pokemon, rng: JSONObject? = null): Queue<MessageFlag> {
        return moveList[slot].use(this, target, rng)
    }

    /**
     * Checks for immobilizing status before performing a move.
     *
     * Immobilizing statuses are Sleep, Paralysis, and Frozen.
     *
     * @author Oddbjørn S. Borge-Jensen
     */
    fun checkImmobilizingStatus(isSelf: Boolean = true, rng: Int? = null): StatusResponse {
        var message: MessageFlag? = null
        val foe = if(SocketSingleton.isHost()) "" else "Foe "
        var immobilized = false
        val predetermined = rng != null

        // SLEEP
        if (statusCondition == StatusCondition.SLEEP) {
            Log.println(Log.DEBUG, "poke/status", "Status duration (sleep): $statusDuration")

            // Wake up
            if(statusDuration == 0) {
                Log.println(Log.DEBUG, "poke/status", "${name()} woke up")

                message = MessageFlag(
                    message = "${name()} woke up!",
                    animFlag = Animation.SLEEP,
                    animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
                )
                resetStatus()
            }
            // Fast asleep
            else {
                Log.println(Log.DEBUG, "poke/status", "${name()} is fast asleep")

                statusDuration--
                message = MessageFlag("$foe${name()} is fast asleep!")
                immobilized = true
            }
        }

        // PARALYSIS
        else if (statusCondition == StatusCondition.PARALYSIS) {

            val paraRoll = if(predetermined) rng!! else (0 until 100).random()
            Log.println(Log.DEBUG, "poke/status", "Paralysis roll: $paraRoll")

            if(paraRoll < 50) {
                Log.println(Log.DEBUG, "poke/status", "${name()} was immobilized by paralysis")
                message = MessageFlag(
                    message = "${name()} is paralyzed! It can't move!",
                    animFlag = Animation.PARALYSIS,
                    animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
                )
                immobilized = true
            } else Log.println(Log.DEBUG, "poke/status", "$foe${name()} was unaffected by paralysis")
        }

        // FREEZE
        else if (statusCondition == StatusCondition.FREEZE) {

            val unfreezeRoll = if(predetermined) rng!! else (0 until 100).random()
            Log.println(Log.DEBUG, "poke/status", "Freeze roll: $unfreezeRoll")

            if(unfreezeRoll < 20) {
                Log.println(Log.DEBUG, "poke/status", "${name()} was unfrozen")

                message = MessageFlag("${name()} is no longer frozen!")
                resetStatus()
            } else {
                Log.println(Log.DEBUG, "poke/status", "$foe${name()} is frozen")

                message = MessageFlag(
                    message = "${name()} is frozen solid!",
                    animFlag = Animation.FREEZE,
                    animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
                )
                immobilized = true
            }
        }

        return StatusResponse(immobilized, message)
    }

    /**
     * Checks for end of turn status after performing a move.
     *
     * End of turn statuses are Burn, Poison, and Bad Poison.
     *
     * @author Oddbjørn S. Borge-Jensen
     */
    fun checkEndOfTurnStatus(isSelf: Boolean = true): MessageFlag? {
        var message: MessageFlag? = null
        val foe = if(SocketSingleton.isHost()) "" else "Foe "

        // BURN
        if (statusCondition == StatusCondition.BURN) {
            Log.println(Log.DEBUG, "poke/status", "${name()} is hurt by its burn!")

            message = MessageFlag(
                message = "$foe${name()} is hurt by its burn!",
                animFlag = Animation.BURN,
                animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
            )

            this.takeDamage(hpMax / 8 )
        }

        // POISON
        else if (statusCondition == StatusCondition.POISON) {
            Log.println(Log.DEBUG, "poke/status", "${name()} is hurt by poison!")

            message = MessageFlag(
                message = "$foe${name()} is hurt by poison!",
                animFlag = Animation.POISON,
                animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
            )

            this.takeDamage(hpMax / 8 )
        }

        // BADLY POISONED
        else if (statusCondition == StatusCondition.BAD_POISON) {
            Log.println(Log.DEBUG, "poke/status", "$foe${name()} is hurt by bad poison!")
            Log.println(Log.DEBUG, "poke/status", "-- Status counter: $statusCounter")
            Log.println(Log.DEBUG, "poke/status", "-- Damage dealt: ${statusCounter.toFloat() / 8f}")

            message = MessageFlag(
                message = "$foe${name()} is hurt by poison!",
                animFlag = Animation.POISON,
                animTarget = if(isSelf) AnimationTarget.SELF else AnimationTarget.FOE
            )

            this.takeDamage((hpMax.toFloat() * (statusCounter.toFloat() / 8f)).toInt() )
            if(statusCounter < 8) statusCounter++

        }

        return message
    }

    fun takeDamage(damage: Int) {
        hpCurrent = kotlin.math.max(0, hpCurrent - damage)
        if(hpCurrent == 0) faint()
    }

    fun modifyStatStage(modifiedStat: Int, modifiedValue: Int) {
        Log.println(Log.VERBOSE, "MOVE", "-- Successfully applied stat changes to ${this.name()}!")

        statStages[modifiedStat] =
            if(modifiedValue > 0) minOf(statStages[modifiedStat] + modifiedValue, 6)
            else maxOf(statStages[modifiedStat] + modifiedValue, -6)

        Log.println(Log.VERBOSE, "MOVE", "-- Current ${STAT_TO_STRING[modifiedStat]} stage: ${statStages[modifiedStat]}!")
    }

    fun applyStatus(appliedStatus: StatusCondition): Boolean {
        return if(statusCondition == StatusCondition.NONE) {
            Log.println(Log.DEBUG, "mov/sc", "-- Successfully applied status to ${this.name()}!")

            resetStatus()
            statusCondition = appliedStatus

            // Set specific status parameters
            if (statusCondition == StatusCondition.SLEEP)
                statusDuration = LocalData.rng?.let {
                    val rng = if(SocketSingleton.isHost()) it.get("host") as JSONObject
                        else it.get("player") as JSONObject
                    rng.get("sleep") as Int
                } ?: (1..3).random()
            else if (statusCondition == StatusCondition.BAD_POISON) statusCounter = 1

            true
        } else {
            Log.println(Log.VERBOSE, "Status", "-- ${this.name()} already has a status condition!")
            false
        }
    }

    private fun resetStatStages() {
        for(i in statStages.indices) statStages[i] = 0
    }

    private fun resetStatus() {
        statusCounter = 0
        statusCondition = StatusCondition.NONE
    }

    private fun faint() {
        resetStatus()
        resetStatStages()
        auxiliaryStatus = mutableListOf(R.string.status_fainted)
    }

    fun isFainted(): Boolean {
        return (auxiliaryStatus.contains(R.string.status_fainted))
    }

    /**
     * @see Move.statMultiplier
     */
    private fun statMultiplier(stage: Int): Float {
        // Base multiplier:     2 / 2
        // For positive stages: 2 + stage / 2
        // For negative stages: 2 / 2 + stage

        Log.println(Log.DEBUG, "MOVE", "Stat stage: $stage")

        return if(stage > 0) {
            Log.println(Log.DEBUG, "MOVE", "-- Stat multiplier: ${2 + stage}/2")
            (2f + stage.toFloat()) / 2f
        }
        else {
            Log.println(Log.DEBUG, "MOVE", "-- Stat multiplier: 2/${2 - stage}")
            2f / (2f - stage.toFloat())
        }
    }

    /**
     * Determines which pokemon gets to act first.
     *
     * Returns true when self is faster, else false. In the case of a tie the first move is
     * rewarded randomly.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param foe Opposing pokemon to compare speed with.
     * @see speed
     */
    fun isFasterThan(foe: Pokemon): Boolean {
        val selfSpeed = speed()
        val foeSpeed = foe.speed()

        Log.println(Log.DEBUG, "mov/spd",
            "${name()}'s SPEED: $selfSpeed; ${"Foe " + foe.name()}'s SPEED: $foeSpeed")

        val res = when {
            selfSpeed > foeSpeed -> true
            selfSpeed < foeSpeed -> false

            // Speed tie! Use roll if provided, else random 50/50
            LocalData.rng != null -> {
                val roll = LocalData.rng!!.get("speed")
                Log.println(Log.DEBUG, "mov/spd",
                    "${name()}'s SPEED: $selfSpeed; ${"Foe " + foe.name()}'s SPEED: $foeSpeed")
                (roll == 1) == SocketSingleton.isHost()
            }
            else -> (0..1).random() == 1
        }

        Log.println(Log.DEBUG, "mov/spd",
            "${if(res) name() else "Foe " + foe.name()} goes first!")

        return res
    }

    // *-- GETTER FUNCTIONS --* //

    /**
     * Returns the Pokémon's name set to uppercase.
     */
    fun name(): String {
        return name.uppercase()
    }

    /**
     * Returns Pokémon's effective speed stat.
     *
     * Effective speed is the product of the Pokémon's speed stat and the current speed stage
     * multiplier. Additionally, speed is halved for Pokémon afflicted by paralysis.
     *
     * @author Oddbjørn S. Borge-Jensen
     */
    private fun speed(): Float {
        val paralysis = if(statusCondition == StatusCondition.PARALYSIS) 0.5f else 1.0f
        return speed * statMultiplier(statStages[Constants.STAT_SPEED]) * paralysis
    }

    fun clone(): Pokemon {
        return Pokemon(
            name = name,
            hpMax = hpMax,
            attack = attack,
            defense = defense,
            spAttack = spAttack,
            spDefense = spDefense,
            speed = speed,
            types = types,
            sprite = sprite,
            sex = sex,
            moveIds = moveIds,
            moveList = moveList
        )
    }

    // *-- DEPRECATED --* //

    fun forceAddMove(move: Move){
        if(moveList.size < 4)
            moveList.add(move)
        else {
            for (i in 3 downTo 1) moveList[i] = moveList[i - 1]
            moveList[0] = move
        }
    }
    fun addMove(move: Move, slot: Int){
        when {
            moveList.size < 4 -> moveList.add(move)
            slot in 0..3 -> moveList[slot] = move
            else -> Log.println(Log.ERROR, "FUN", "Couldn't add move!")
        }
    }
}