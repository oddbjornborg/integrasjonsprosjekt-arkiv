package com.integrasjon.pokemon.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.classes.HttpSingleton
import com.integrasjon.pokemon.classes.LocalData
import com.integrasjon.pokemon.classes.SocketSingleton

private val CLR_MAIN_MENU_BACKGROUND = Color.DarkGray
private val CLR_MAIN_MENU_TEXT = Color.White

private const val CONFIG_MAIN_MENU_TEXT_SIZE = 60

@Composable
fun MainMenu (navController: NavHostController) {
    var loginStatusAlpha by remember { mutableStateOf(1f) }

    Box (
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .background(CLR_MAIN_MENU_BACKGROUND)
        ) {
            Column(
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.5f)
            ) {
                if(!HttpSingleton.hasCookie()) {
                    MenuItem(
                        text = "Register",
                        modifier = Modifier
                            .clickable { navController.navigate("register_screen") }
                    )
                    MenuItem(
                        text = "Login",
                        modifier = Modifier
                            .clickable { navController.navigate("login_screen") }
                    )
                } else {
                    MenuItem(
                        text = "Create Game",
                        modifier = Modifier
                            .clickable {
                                SocketSingleton.createLobby()
                                navController.navigate("pregame_lobby")
                            }
                    )
                    MenuItem(
                        text = "Find game",
                        modifier = Modifier
                            .clickable {
                                SocketSingleton.findGame()
                                navController.navigate("pregame_lobby")
                            }
                    )
                }
            }
        }
        Text(
            text = if(!HttpSingleton.hasCookie()) "Not logged in!"
                else "Logged in as ${HttpSingleton.loginName}",
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .alpha(loginStatusAlpha)
        )
    }
}

@Composable
fun MenuItem(text: String, modifier: Modifier) {
    Row (
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        Text(
            text = text,
            fontSize = CONFIG_MAIN_MENU_TEXT_SIZE.sp,
            color = CLR_MAIN_MENU_TEXT,
            fontFamily = FontFamily(Font(R.font.pokemon_fire_red)),
        )
    }
}

@Preview
@Composable
private fun MainMenuPreview() {
    val nav = rememberNavController()
    MainMenu(nav)
}