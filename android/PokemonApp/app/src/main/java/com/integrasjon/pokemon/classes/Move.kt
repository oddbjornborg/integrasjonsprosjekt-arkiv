package com.integrasjon.pokemon.classes

import android.util.Log
import androidx.annotation.StringRes
import androidx.compose.ui.res.stringResource
import com.integrasjon.pokemon.*
import com.integrasjon.pokemon.Constants.STAT_TO_STRING
import com.integrasjon.pokemon.Constants.TYPE_MAP
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.random.Random

class Move(
    val name: String,
    val type: Type,
    val power: Int = 0,
    val isPhysical: Boolean,
    val moveEffects: MutableList<MoveEffect> = mutableListOf(),
    val ppMax: Int,
    val accuracy: Int,
    val priority: Int = 0,
    val critRatio: Int = 0,
    val description: String = "No description",
    val animFlag: Animation = Animation.NONE,
    val animTarget: AnimationTarget = AnimationTarget.SELF
) {
    // Initialization
    var ppCurrent = ppMax
    var predetermined = false
    var rng: JSONObject? = null

    // *-- MAIN MOVE FUNCTION --* //

    /**
     * Performs move. All other non-getter move functions are called from here.
     *
     * Checks whether a move has an effect on the target depending on type matchup, then whether or
     * not the move hits. Then the damage of the move is calculated and any move effects are
     * executed. Any triggered effect or result that has an associated message flag will have said
     * flag added to the message queue to be returned.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return A queue of MessageFlags, containing a text message, an animation,
     * and an animation target.
     *
     * @see hasEffect
     * @see doesMoveHit
     * @see calculateMoveDamage
     * @see executeMoveEffects
     */
    fun use(user: Pokemon, target: Pokemon, preRoll: JSONObject? = null): Queue<MessageFlag> {
        predetermined = preRoll != null
        if(predetermined) rng = preRoll

        Log.println(Log.VERBOSE, "move", "${user.name()} used ${name()}!")

        // Initialize queue of message flags (text, animation, animation target)
        val messageQueue = LinkedList<MessageFlag>()

        // Deduct PP
        ppCurrent--

        // Type matchup calculation
        if(!hasEffect(target)) {
            return messageQueue.apply {
                add(MessageFlag("It had no effect on opposing ${target.name()}..."))
            }
        }

        // Move hit calculation
        if(!doesMoveHit(user, target)) {
            return messageQueue.apply {
                add(MessageFlag("${user.name()}'s attack missed!"))
            }
        }

        // Calculate and deal damage
        if(power > 0) {
            // Response -> Pair(damage: Int, queue: Queue<String>)
            val response = calculateMoveDamage(user, target)
            response.second.forEach { messageFlag ->
                messageQueue.add(messageFlag)
            }
            target.takeDamage(response.first)
        }

        // Execute move effects
        val effectQueue = executeMoveEffects(user, target)
        effectQueue.forEach { messageQueue.add(it) }

        return messageQueue
    }

    // *-- MOVE EXECUTION HELPER FUNCTIONS --* //

    /**
     * Checks whether move has any effect on the opposing Pokémon.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param target Pokemon on receiving end of move
     * @returns False if move type has no effect against any of target's types; else true
     */
    private fun hasEffect(target: Pokemon): Boolean {
        // If target contains type move has no effect against
        if(target.types.any { TYPE_MAP[type.stringRes]!!.matchup(it) == 0.0f }) {
            // Some moves are exempt; e.g. Growl (normal type) can hit ghost types
            // This condition might need some refinement
            if(power > 0 || moveEffects.any{it.statusApplied != StatusCondition.NONE}) {
                Log.println(Log.VERBOSE, "MOV", "Move has no effect against opponent")
                return false
            }
        }
        return true
    }

    /**
     * Determines whether or not move successfully hits the target.
     *
     * Rolls a random integer in the range 0-99. Determines whether a move hits by checking if
     * this number is less than the move's accuracy times a multiplier of user's accuracy minus
     * target's evasion. Moves with 0 accuracy are considered always-hit-moves, and are exempt
     * from this formula, always returning true.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return True if move hits, false if miss
     * @see getHitStageSum
     * @see hitStatMultiplier
     * @see use
     */
    private fun doesMoveHit(user: Pokemon, target: Pokemon): Boolean {
        // Check for always hit moves
        if(accuracy == 0) {
            Log.println(Log.DEBUG, "MOV", "Hit: Always hit move")
            return true
        }

        // User's accuracy minus target's evasion determines stat stage to use for calculation
        val hitStageSum = getHitStageSum(user, target)

        // Set target hit value that must be rolled less than
        val toHit = this.accuracy * hitStatMultiplier(hitStageSum)

        // Actual hit roll, a random number 0-99
        val hitRoll = if(predetermined) rng!!.get("accuracy") as Int
            else Random.nextInt(100)

        // Boolean to be returned. Returns true for roll less than target
        val isHit = hitRoll < toHit

        val hitText = if(isHit) "Hit!" else "Miss!"
        Log.println(Log.VERBOSE, "move/hit", "$hitText |  To hit: $toHit |  Hit roll: $hitRoll")

        return isHit
    }

    /**
     * Returns remainder of user's accuracy minus target's evasion.
     *
     * Return value is used to determine the hit stat multiplier.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return User's accuracy minus target's evasion; max absolute value of 6
     * @see doesMoveHit
     * @see hitStatMultiplier
     */
    private fun getHitStageSum(user: Pokemon, target: Pokemon): Int {
        // User's accuracy minus target's evasion determines stat stage to use for calculation
        var hitStageSum = user.statStages[Constants.STAT_ACCURACY] - target.statStages[Constants.STAT_EVASION]
        // Ensures value within range (-6..6)
        if(hitStageSum > 6) hitStageSum = 6
        else if (hitStageSum < -6) hitStageSum = -6

        return hitStageSum
    }

    /**
     * Unique stat multiplier formula for accuracy and evasion.
     *
     * Takes the base fraction 3/3 and adds the absolute value of the given stage to either the
     * numerator or the denominator, depending on whether the value of the stage is negative or
     * positive.
     *
     * E.g. with a given stage og 2 the resulting fraction will be 5/3, and a stage of
     * -3 will give 3/6. Max value of both the numerator and denominator is 9.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param stage The remainder of user's accuracy minus target's evasion
     * @return A float in the range 0.33f - 3.00f
     * @see doesMoveHit
     */
    private fun hitStatMultiplier(stage: Int): Float {
        // Base multiplier:     3 / 3
        // For positive stages: 3 + abs(stage) / 3
        // For negative stages: 3 / 3 + abs(stage)

        return if(stage > 0)
            (3f + stage.toFloat()) / 3f
        else
            3f / (3f - stage.toFloat())
    }

    /**
     * Calculates move damage and generates damage related message flags.
     *
     * Performs all operations related to calculating a move's damage. This includes
     * fetching the correct stats from the participating pokemon depending on whether the move
     * is physical or special, applying multipliers from stat stages, and calling all
     * functions related to deciding other damage related multipliers.
     *
     * Note: Upon a critical hit only stat changes that benefit the user apply. Negative attack
     * stages and positive defense stages will be completely ignored.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return Pair containing the damage dealt as an integer and any resulting message
     * flags as a queue.
     *
     * @see statMultiplier
     * @see type
     * @see crit
     * @see base
     * @see stab
     * @see burn
     * @see random
     */
    private fun calculateMoveDamage(user: Pokemon, target: Pokemon): Pair<Int, Queue<MessageFlag>> {
        val textBoxQueue = LinkedList<MessageFlag>()

        // Get relevant stat stages
        var userStatStage =
            if(isPhysical) user.statStages[Constants.STAT_ATTACK]
            else user.statStages[Constants.STAT_SPECIAL_ATTACK]
        var targetStatStage =
            if(isPhysical) target.statStages[Constants.STAT_DEFENSE]
            else target.statStages[Constants.STAT_SPECIAL_DEFENSE]

        // Check type advantage
        val type = type(target)
        when {
            type > 1f -> textBoxQueue.add(MessageFlag(Constants.TEXT_SUPER_EFFECTIVE))
            type == 0.5f -> textBoxQueue.add(MessageFlag(Constants.TEXT_NOT_EFFECTIVE))
        }

        // Check critical; remove unfavorable stat stages on crit
        val crit = crit()
        if(crit == 1.5f) {
            userStatStage = kotlin.math.max(userStatStage, 0)
            targetStatStage = kotlin.math.min(targetStatStage, 0)
            textBoxQueue.add(MessageFlag(Constants.TEXT_CRITICAL))
        }

        // Apply stat stages
        var attackStat = if(isPhysical) user.attack.toFloat() else user.spAttack.toFloat()
        attackStat *= statMultiplier(userStatStage)

        var defenseStat = if(isPhysical) target.defense.toFloat() else target.spDefense.toFloat()
        defenseStat *= statMultiplier(targetStatStage)

        // Calculate damage
        val base = base(user, attackStat.toInt(), defenseStat.toInt())
        val damageMax = (base * type * crit * stab(user) * burn(user)).toInt()
        val damage = (damageMax * random()).toInt()

        Log.println(Log.VERBOSE, "move/dmg",
            "Possible damage range: ${(damageMax * 0.85f).toInt()} - $damageMax | Damage dealt: $damage")

        return Pair(damage, textBoxQueue)
    }

    /**
     * Stat multiplier formula for all stats except accuracy and evasion
     *
     * Takes the base fraction 2/2 and adds the absolute value of the given stage to either the
     * numerator or the denominator, depending on whether the value of the stage is negative or
     * positive. E.g. with a given stage og 2 the resulting fraction will be 4/2, and a stage of
     * -3 will give 2/5. Max value of both the numerator and denominator is 6.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param stage The current stat stage of the relevant stat
     * @return An float in the range 0.25f - 4.00f
     * @see calculateMoveDamage
     */
    private fun statMultiplier(stage: Int): Float {
        // Base multiplier:     2 / 2
        // For positive stages: 2 + stage / 2
        // For negative stages: 2 / 2 + stage

        //Log.println(Log.DEBUG, "move/dmg", "Stat stage: $stage")

        return if(stage > 0) {
            Log.println(Log.DEBUG, "move/dmg",
                "Stat stage: $stage | Stat multiplier: ${2 + stage}/2")
            (2f + stage.toFloat()) / 2f
        }
        else {
            Log.println(Log.DEBUG, "move/dmg",
                "Stat stage: $stage | Stat multiplier: 2/${2 - stage}")
            2f / (2f - stage.toFloat())
        }
    }

    /**
     * Executes all additional move effects for the current move. These come in the form of
     * either applying a specific status effect to the target, or modifying the stat stages
     * of either the user or the target.
     *
     * Effects only trigger if the target is not fainted and it passes a hit roll.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move.
     * @param target Pokemon on receiving end of move.
     * @return A queue of potential message flags created by executed move effects.
     *
     * @see applyStatus
     * @see applyStatStage
     */
    private fun executeMoveEffects(user: Pokemon, target: Pokemon): Queue<MessageFlag> {
        val messageQueue = LinkedList<MessageFlag>()

        val rngArray = if(predetermined) rng!!.get("apply") as JSONArray else null
        var statusCount = 0

        moveEffects.forEach { effect ->
            // Skips effects targeting fainted pokemon
            if(!target.isFainted()) {

                val hitRoll = if(predetermined) rngArray!!.get(statusCount++) as Int
                    else Random.nextInt(100)

                if (hitRoll < effect.applyChance) {

                    // Set effect target
                    val effectTarget = if (effect.target == AnimationTarget.FOE) target else user

                    // Apply status condition
                    if (effect.statusApplied != StatusCondition.NONE) {
                        val response = applyStatus(effect, effectTarget)
                        messageQueue.add(response)
                    }

                    // Apply stat stage
                    if (effect.statsModified.isNotEmpty()) {
                        val response = applyStatStage(effect, effectTarget)
                        response.forEach { messageQueue.add(it) }
                    }
                }
                else Log.println(Log.VERBOSE, "MOV", "-- Effect missed")
            }
        }
        return messageQueue
    }

    /**
     * Applies a status condition to the target pokemon. Fails if target already has a
     * status condition.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param effect The relevant move effect of current move.
     * @param target Pokemon who will receive the effect.
     * @return Returns a message flag based on status applied.
     *
     * @see executeMoveEffects
     */
    private fun applyStatus(effect: MoveEffect, target: Pokemon): MessageFlag {
        // TODO: [MEDIUM] Status applied animations, time with application
        var message: String

        if(target.applyStatus(effect.statusApplied)) {
            message = "${target.name()} "
            message += when(effect.statusApplied) {
                StatusCondition.NONE -> "was burned!"
                StatusCondition.BURN -> "was burned!"
                StatusCondition.FREEZE -> "was frozen solid!"
                StatusCondition.PARALYSIS -> "was paralyzed!"
                StatusCondition.POISON -> "was poisoned!"
                StatusCondition.BAD_POISON -> "was badly poisoned!"
                StatusCondition.SLEEP -> "fell asleep!"
            }
        }
        else message = "But it failed!"

        return MessageFlag(message)
    }

    /**
     * Modifies a Pokémon's stat stage values.
     *
     * Applies a positive or negative change to all target's stats listed in the move effect.
     * Maximum modifying value of a single move effect is 3. A pokemon's stat can go no higher
     * than 6 and no lower than -6.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param effect MoveEffect containing list of stats and value to modify by.
     * @param target Pokemon who will receive the effect.
     * @return Queue of MessageFlags, containing textbox messages and animation info.
     *
     * @see executeMoveEffects
     * @see statMultiplier
     */
    private fun applyStatStage(effect: MoveEffect, target: Pokemon): Queue<MessageFlag> {
        val messageQueue = LinkedList<MessageFlag>()

        effect.statsModified.forEach { stat ->
            var message = "${target.name()}'s ${STAT_TO_STRING[stat]} "

            if(effect.modifiedValue > 0 && target.statStages[stat] == 6)
                message += "won't go higher!"
            else if(effect.modifiedValue < 0 && target.statStages[stat] == -6)
                message += "won't go any lower!"
            else {
                target.modifyStatStage(stat, effect.modifiedValue)
                message += when(effect.modifiedValue) {
                    1 -> "rose!"
                    2 -> "rose sharply!"
                    3 -> "rose drastically!"
                    -1 -> "fell!"
                    -2 -> "harshly fell!"
                    -3 -> "severely fell!"
                    else -> "went to shit"  //TODO: Add exception
                }
            }
            messageQueue.add(
                MessageFlag(
                    message = message,
                    animFlag =
                        if(effect.modifiedValue > 0)  Animation.BUFF
                        else Animation.DEBUFF,
                    animTarget = effect.target
                )
            )
        }
        return messageQueue
    }

    // *-- DAMAGE CALCULATION HELPER FUNCTIONS --* //

    /**
     * Calculates the move's base damage.
     *
     * This base damage value is what all other damage multipliers are applied to. Based on move's
     * power and relevant attack and defense stat values.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param attack User's relevant attack stat; either Attack or Special Attack
     * @param defense Target's relevant defense stat; either Defense or Special Defense
     * @return Base damage of move
     *
     * @see calculateMoveDamage
     */
    private fun base(user: Pokemon, attack: Int, defense: Int): Int {
        val damage = ((2 * user.level / 5 + 2) * power * attack / defense) / 50 + 2
        Log.println(Log.DEBUG, "move/dmg", "Base damage: $damage")
        return damage
    }

    /**
     * Decides critical hit damage multiplier.
     *
     * Critical hit ratio is a unique property whose effects apply non-linearly. In the current
     * build the highest possible crit ratio is 1.
     *
     * Rolls a random integer 0-23. If this value is lower than the target value associated with
     * the move's crit ratio, move lands a critical hit dealing 1.5 times damage.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @return 1.5 on crit, else 1.0
     * @see calculateMoveDamage
     */
    private fun crit(): Float {
        // Crit chance scales non-linearly with move's crit ratio
        // There are other ways to boost crit chance, but we probably won't implement them
        val toCrit = when(critRatio) {
            0 -> 1
            1 -> 3
            2 -> 12
            3 -> 24
            //TODO: Add exception
            else -> 1   // Should never happen
        }
        val critRoll = if(predetermined) rng!!.get("crit") as Int
            else Random.nextInt(24)

        val critMultiplier = if(critRoll < toCrit) 1.5f else 1.0f

        Log.println(Log.DEBUG, "move/dmg",
            "Crit multiplier: $critMultiplier | To crit: $toCrit | Crit roll: $critRoll")
//        Log.println(Log.DEBUG, "move/dmg", "-- To crit: $toCrit")
//        Log.println(Log.DEBUG, "move/dmg", "-- Crit roll: $critRoll")

        return critMultiplier
    }

    /**
     * Decides random damage multiplier.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @return Float in range 0.85 - 1.00
     *
     * @see calculateMoveDamage
     */
    private fun random(): Float {
        val randomMultiplier = (
            if(predetermined) rng!!.get("random") as Int
            else (85..100).random()
        ).run { this.toFloat() / 100f }

        Log.println(Log.DEBUG, "move/dmg", "Random multiplier: $randomMultiplier")
        return randomMultiplier
    }

    /**
     * Decides STAB damage multiplier.
     *
     * Same Type Attack Bonus. STAB applies if the move is of the same type as one of the user's
     * types, dealing 1.5 times damage.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @return 1.5 if STAB is active, else 1.0
     *
     * @see calculateMoveDamage
     */
    private fun stab(user: Pokemon): Float {
        val stab = if(type in user.types) 1.5f else 1f

        if(stab == 1.5f) Log.println(Log.DEBUG, "move/dmg", "STAB active")
        else Log.println(Log.DEBUG, "move/dmg", "STAB not active")

        return stab
    }

    /**
     * Decides type effectiveness damage multiplier.
     *
     * Returns the product of the move's type effectiveness against both the target's types.
     * Possible type effectiveness values are:
     *
     * Not Very Effective (0.5f)
     *
     * Effective (1.0f)
     *
     * Super Effective (2.0f)
     *
     * Lowest and highest possible values are 0.25f for double NVE and 4.0f for double SE.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param target Pokemon on receiving end of move
     * @return Float in range 0.25 - 4.00
     *
     * @see calculateMoveDamage
     * @see use
     */
    private fun type(target: Pokemon): Float {
        val typeMultiplier = TYPE_MAP[type.stringRes]!!.matchup(target.types)
        Log.println(Log.DEBUG, "move/dmg", "Type multiplier: $typeMultiplier")
        return typeMultiplier
    }

    /**
     * Decides burn damage multiplier.
     *
     * Burn halves the damage dealt by the afflicted Pokémon's physical moves.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @return 0.5f when burned, else 1.0f
     *
     * @see calculateMoveDamage
     */
    private fun burn(user: Pokemon): Float {
        val burn = if(user.statusCondition == StatusCondition.BURN && this.isPhysical) 0.5f else 1f
        if(burn == 0.5f) Log.println(Log.DEBUG, "mod/dmg", "Damage halved from burn!")
        return burn
    }

    // *-- GETTER FUNCTIONS --* //
    fun name(): String { return name.uppercase() }
    fun anim(): Pair<Animation, AnimationTarget> { return Pair(animFlag, animTarget)}

    /**
     * Completely copies an existing move. If moves weren't clones one Pokémon using a certain move
     * would also deplete the power points of all other Pokémon with the same move.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @returns A new instance of an existing move
     */
    fun clone(): Move {
        return Move(
            name = name,
            type = type,
            power = power,
            isPhysical = isPhysical,
            moveEffects = moveEffects,
            ppMax = ppMax,
            accuracy = accuracy,
            priority = priority,
            critRatio = critRatio,
            description = description,
            animFlag = animFlag,
            animTarget = animTarget
        )
    }
}