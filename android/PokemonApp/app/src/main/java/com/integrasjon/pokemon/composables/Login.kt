package com.integrasjon.pokemon.composables

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.integrasjon.pokemon.classes.HttpSingleton

@Composable
fun LoginScreen(mainNavController: NavController) {
    var usernameState by remember {mutableStateOf("oddbjorn")}
    var passwordState by remember {mutableStateOf("12345678")}
    var loginSuccess: Boolean? = null

    val failedToast = Toast.makeText(
        LocalContext.current,
        "Login failed!",
        Toast.LENGTH_SHORT
    )

    fun login(username: String, password: String) {
        loginSuccess = HttpSingleton.loginRequest(username, password)

        loginSuccess?.let {
            if(it) {
                mainNavController.navigate("main_menu")
            }
            else {
                passwordState = ""
                failedToast.show()
            }
        }
    }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(Color.DarkGray)
    ) {
        TextField(
            value = usernameState,
            onValueChange = { usernameState = it },
            label = { Text("Username") }
        )
        TextField(
            value = passwordState,
            onValueChange = { passwordState = it },
            label = { Text("Password") }
        )
        Spacer(
            modifier = Modifier
                .height(20.dp)
        )
        Button(
            onClick = { login(usernameState, passwordState) }
        ) {
            Text("Log in")
        }
    }
}

@Preview
@Composable
private fun LoginPreview() {
    LoginScreen(rememberNavController())
}