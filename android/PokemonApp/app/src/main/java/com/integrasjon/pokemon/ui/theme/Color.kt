package com.integrasjon.pokemon.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

//val CLR_TEXTBOX_BORDER = Color(0xFFd04838)
//val CLR_TEXTBOX_BACKGROUND_OUTER = Color(0xFF48424d)
//val CLR_TEXTBOX_BACKGROUND_INNER = Color(0xFF68a0a0)
//val CLR_TEXTBOX_TEXT = Color(0xFFeff8eb)
//val CLR_TEXTBOX_TEXT_SHADOW = Color(0xFF685870)
//val CLR_TEXTBOX_BORDER_SHADOW = Color(0xFF818986)