package com.integrasjon.pokemon.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import com.google.gson.Gson
import com.integrasjon.pokemon.classes.JSONPlayerJoined
import com.integrasjon.pokemon.classes.LocalData
import com.integrasjon.pokemon.classes.SocketSingleton
import com.integrasjon.pokemon.classes.User

/**
 * Pregame lobby where host waits for a challenger to join their game.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
@Composable
fun PregameLobby(navController: NavController) {
    val isHost = SocketSingleton.iAmHost
    var player: User? by remember {mutableStateOf(null)}
    var ready by remember {mutableStateOf(false)}

    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(Color.DarkGray)
    ) {
        // HOST
        UserBlob(LocalData.getSelf(), true)

        // HOST - GO TO GAME
        if(isHost) {
            Button(
                onClick = {
                    SocketSingleton.hostGame()
                    navController.navigate("battle_screen")
                },
                content = { Text(
                    if(ready) "Go to game!"
                    else "Waiting..."
                )},
                enabled = ready
            )
        }
        // PLAYER - READY UP
        else if(!ready) {
            Button(
                onClick = { SocketSingleton.readyUp() },
                content = { Text(text = "Ready!") }
            )
        }
        // PLAYER - GO TO GAME
        else {
            Button(
                onClick = {
                    SocketSingleton.joinGame()
                    navController.navigate("battle_screen")
                },
                content = { Text(text = "Go to game!") }
            )
        }

        // PLAYER
        if(isHost) UserBlob(player, false)
        else UserBlob(LocalData.getFoe(), false)
    }

    // Listen for player joining
    if(isHost && LocalData.users.size < 2) {
        if (LocalData.users.size < 2) {
            SocketSingleton.socket.once("player-joined") { parameters ->
                if(LocalData.users.size < 2) {
                    val user = Gson().fromJson(
                        parameters[0].toString(),
                        JSONPlayerJoined::class.java
                    )
                    println("[Socket] Player joined: ${user.user_id} - ${user.socket_id}")

                    LocalData.fetchUser(user.user_id, user.socket_id)
                    player = LocalData.getFoe()    // Spooky
                }
            }
        } else if (!ready) {
            SocketSingleton.socket.on("match-begin") { ready = true }
        }
    }
    else {
        SocketSingleton.socket.once("match-begin") { ready = true }
    }
}

@Composable
private fun UserBlob(user: User?, isSelf: Boolean) {
    Column(
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Text(if(isSelf) "YOU:" else "OPPONENT:")
        Text("Name: ${user?.name ?: "not yet joined"}")
        Text("Socket: ${user?.socketID ?: "not yet joined"}")
    }
}