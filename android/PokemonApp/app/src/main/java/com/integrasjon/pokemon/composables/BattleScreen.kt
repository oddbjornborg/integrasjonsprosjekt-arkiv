package com.integrasjon.pokemon.composables

import android.os.Message
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.integrasjon.pokemon.*
import com.integrasjon.pokemon.R
import com.integrasjon.pokemon.R.*
import com.integrasjon.pokemon.classes.*
import okhttp3.internal.wait
import org.json.JSONObject
import java.util.*
import kotlin.concurrent.thread

// TEXTBOX theme
private val CLR_TEXTBOX_BORDER = Color(0xFFd04838)
private val CLR_TEXTBOX_BACKGROUND_OUTER = Color(0xFF48424d)
private val CLR_TEXTBOX_BACKGROUND_INNER = Color(0xFF68a0a0)
private val CLR_TEXTBOX_TEXT = Color(0xFFeff8eb)
private val CLR_TEXTBOX_TEXT_SHADOW = Color(0xFF685870)
private val CLR_TEXTBOX_BORDER_SHADOW = Color(0xFF818986)

// FIGHTMENU theme
private val CLR_FIGHTMENU_BORDER_OUTER = Color(0xFF283030)
private val CLR_FIGHTMENU_BORDER_INNER = Color(0xFF706880)
private val CLR_FIGHTMENU_BACKGROUND = Color(0xFFeff8eb)
private val CLR_FIGHTMENU_TEXT = Color(0xFF48424d)
private val CLR_FIGHTMENU_TEXT_SELECTED = Color.Red
private val CLR_FIGHTMENU_TEXT_SHADOW = Color(0xFFc9c6c4)

// CONFIG
private const val CONFIG_FIGHTMENU_TEXT_SIZE = 48
private const val CONFIG_TEXTBOX_TEXT_SIZE = 28
private const val CONFIG_TEXTBOX_TEXT_SHADOW_OFFSET = 2.2f
private val CONFIG_TEXTBOX_FONT = FontFamily(Font(font.pokemon_rs_w5))
private const val CONFIG_IMAGE_SIZE = 170
private const val CONFIG_BOTTOM_MENU_HEIGHT = 150

// !! Queue containing all messages for the text box, as well as animation flags
private var textBoxQueue: Queue<MessageFlag> = LinkedList()

/**
 * Inneholder mesteparten av spillogikken hva angår å utføre handlinger og runder.
 * Inneholder også underordnede composables som viser tekst til brukeren og menyen der brukeren
 * kan velge hva slags handling de vil utføre.
 *
 * @author Oddbjørn S. Borge-Jensen
 */
@Composable
fun BattleScreen(pokeListSelf: List<Pokemon>, pokeListFoe: List<Pokemon>) {
    // NavController for navigating submenus of the battle screen (e.g. pokemon select)
    val battleScreenNavController = rememberNavController()

    // Helper variables to avoid having to call getter functions several times
    val self = LocalData.getSelf()
    val foe = LocalData.getFoe()
    val pokeSelf = self.getActivePokemon()
    val pokeFoe = foe.getActivePokemon()

    // States; battle screen is recomposed every time any of these values change
    var renderFightMenu by remember { mutableStateOf(true) }
    var textBoxContent by remember { mutableStateOf("What will\n${pokeSelf.name} do?") }
    var selfGoesFirst by remember { mutableStateOf(true) }

    // Decides whether the HP bar should be animated when a health bar's HP percentage changes
    // Set to be true when a Pokémon takes damage, and false when a new Pokémon is swapped in
    var animateHp by remember { mutableStateOf(false) }

    // Toggle to animate buff effects
    var iconAnim by remember { mutableStateOf(Animation.NONE) }
    var iconAnimTarget by remember { mutableStateOf(AnimationTarget.SELF) }

    // "Semaphores" for move execution
    var moveOngoing by remember { mutableStateOf(false) }
    var performingMoves by remember { mutableStateOf(false) }
    var actionOneComplete by remember { mutableStateOf(false) }
    var actionTwoComplete by remember { mutableStateOf(false) }
    var statusLock by remember { mutableStateOf(false) }

    // Checks for and executes active pokemon's end of turn statuses [BURN, POISON]
    fun endOfTurnStatus(isSelf: Boolean) {
        val pokemon =
            if (isSelf) LocalData.getSelf().getActivePokemon()
            else LocalData.getFoe().getActivePokemon()

        // Check end of turn statuses
        if (!statusLock) {
            thread {
                statusLock = true

                val messageFlag = pokemon.checkEndOfTurnStatus(isSelf)
                messageFlag?.let {
                    // Status message
                    textBoxContent = "${if (!isSelf) "Foe " else ""}${it.message}"
                    // Start delay
                    waitInThread(300)
                    // Animation (1000ms)
                    iconAnimTarget = it.animTarget
                    iconAnim = it.animFlag
                    waitInThread(1000)
                    iconAnim = Animation.NONE
                    // End delay
                    waitInThread(700)
                }

                statusLock = false
            }
        }
    }

    // Performs a single move
    fun executeMove(userIsSelf: Boolean, action: Action, userIsHost: Boolean) {
        println("-- EXECUTING MOVE -- ")

        var rng: JSONObject? = null
        val move = action.ordinal

        // Set predetermined rng if provided
        LocalData.rng?.let {
            rng = if (userIsHost) it.get("host") as JSONObject
            else LocalData.rng!!.get("player") as JSONObject
        }

        // Set user and target pokemon
        val user = if (userIsSelf) self.getActivePokemon()
        else foe.getActivePokemon()
        val target = if (userIsSelf) foe.getActivePokemon()
        else self.getActivePokemon()

        // Check immobilizing status
        val statusResponse = user.checkImmobilizingStatus(
            isSelf = userIsSelf,
            rng = rng?.run { this.get("status") as Int }
        )
        val immobilized = statusResponse.isImmobilized

        // "(Foe) POKEMON used MOVE" if successful; status message if immobilized
        if (immobilized)
            textBoxQueue.add(
                statusResponse.messageFlag?.run { this }
                    ?: MessageFlag("Missing message flag!")
            )
        else {
            statusResponse.messageFlag?.let {
                textBoxQueue.add(it)
            }
            textBoxQueue.add(
                MessageFlag(
                    "${if (!userIsSelf) "Foe " else ""}${user.name()} used\n${user.moveList[move].name()}!"
                )
            )
        }

        // Hide menus
        renderFightMenu = false
        battleScreenNavController.navigate("empty")

        while (textBoxQueue.isNotEmpty()) {
            val messageFlag = textBoxQueue.remove()
            textBoxContent = messageFlag.message
        }

        // Not immobilized; perform move
        if (!immobilized) {
            thread {
                // Set HP bar to animate changes
                animateHp = true

                // Get main move animation flag and target
                val anim = user.moveList[move].anim()
                var animTarget = anim.second

                // Reverse targeting when foe uses move
                if (!userIsSelf) {
                    if (animTarget == AnimationTarget.SELF)
                        animTarget = AnimationTarget.FOE
                    else if (animTarget == AnimationTarget.FOE)
                        animTarget = AnimationTarget.SELF
                }

                // Attack animation
                when (anim.first) {
                    Animation.ATTACK -> {
                        // Start delay
                        waitInThread(500)
                        // Animation (500ms)
                        iconAnimTarget = animTarget
                        iconAnim = anim.first
                        waitInThread(400)
                        textBoxQueue = user.useMove(move, target, rng)
                        waitInThread(100)
                        iconAnim = Animation.NONE
                        // End delay
                        waitInThread(1000)
                    }
                    // Buff and debuff animation
                    in listOf(Animation.BUFF, Animation.DEBUFF) -> {
                        // Start delay
                        waitInThread(100)
                        // Animation (1100ms)
                        iconAnimTarget = animTarget
                        iconAnim = anim.first
                        waitInThread(250)
                        textBoxQueue = user.useMove(move, target, rng)
                        waitInThread(850)
                        iconAnim = Animation.NONE
                        // End delay
                        waitInThread(800)
                    }
                    // No animation
                    else -> {
                        textBoxQueue = user.useMove(move, target)
                        // Delay
                        waitInThread(2000)
                    }
                }

                // Iterate through message flags
                while (textBoxQueue.isNotEmpty()) {
                    // Get next message flag and display message
                    val nextMessageFlag = textBoxQueue.remove()
                    textBoxContent = nextMessageFlag.message
                    animTarget = nextMessageFlag.animTarget

                    // Reverse targeting when foe uses move
                    if (!userIsSelf) {
                        if (animTarget == AnimationTarget.SELF)
                            animTarget = AnimationTarget.FOE
                        else if (animTarget == AnimationTarget.FOE)
                            animTarget = AnimationTarget.SELF
                    }

                    // Play animation and sleep (total 2000ms)
                    when {
                        // Buff animations
                        animIsBuff(nextMessageFlag.animFlag) -> {
                            // Start delay
                            waitInThread(100)
                            // Animation (1100ms)
                            iconAnimTarget = animTarget
                            iconAnim = nextMessageFlag.animFlag
                            waitInThread(1100)
                            iconAnim = Animation.NONE
                            // End delay
                            waitInThread(900)
                        }
                        else -> {
                            // Delay
                            waitInThread(2000)
                        }
                    }
                }

                // Swap if Pokémon fainted
                //TODO: Make pokemon swap look nice [LOW]
                Log.println(Log.DEBUG, "Logic", "Checking for fainted Pokémon")
                if (pokeFoe.isFainted()) {
                    for (i in pokeListFoe.indices)
                        if (!pokeListFoe[i].isFainted()) {
                            animateHp = false
                        }
                }

                moveOngoing = false
                if (actionOneComplete) actionTwoComplete = true
                else actionOneComplete = true
            }
        }
        // When immobilized by status condition
        else {
            thread {
                if (statusResponse.messageFlag!!.animFlag != Animation.NONE) {
                    val mf = statusResponse.messageFlag

                    // Status animations
                    if (animIsStatus(mf.animFlag)) {
                        // Start delay
                        waitInThread(300)
                        // Animation (1000ms)
                        iconAnimTarget = mf.animTarget
                        iconAnim = mf.animFlag
                        waitInThread(1000)
                        iconAnim = Animation.NONE
                        // End delay
                        waitInThread(700)
                    }
                } else {
                    waitInThread(2000)
                }
                moveOngoing = false
                if (actionOneComplete) actionTwoComplete = true
                else actionOneComplete = true
            }
        }
    }

    // Performs a pokemon swap
    fun executeSwap(userIsSelf: Boolean, action: Action, onFaint: Boolean = false) {
        println("-- EXECUTING SWAP -- ")

        thread {
            val user = LocalData.run { if (userIsSelf) this.getSelf() else this.getFoe() }

            // Hide menu
            renderFightMenu = false

            if(!onFaint && userIsSelf) {
                textBoxContent = "${user.getActivePokemon().name()}! Come back!"
                waitInThread(2000)
            }

            user.activePokemon = action.ordinal - 4

            textBoxContent = if(userIsSelf) "Go, ${user.getActivePokemon().name()}!"
            else "Foe ${user.name} sent out ${user.getActivePokemon().name()}!"

            waitInThread(2000)

            moveOngoing = false
            if (actionOneComplete) actionTwoComplete = true
            else actionOneComplete = true
        }
    }

    // Performs a single pokemon's turn, playing animations and iterating message flags
    fun executeAction(userIsSelf: Boolean, action: Action) {
        moveOngoing = true

        println("-- EXECUTING ACTION -- ")
        val userIsHost = SocketSingleton.isHost() == userIsSelf

        when {
            action.ordinal <= 3 -> executeMove(userIsSelf, action, userIsHost)
            action.ordinal <= 9 -> executeSwap(userIsSelf, action)
            else -> println("INVALID ACTION")
        }
    }

    // Not implemented
    fun checkFainted(): Boolean {
        var fainted = false
        if (pokeSelf.isFainted()) {
            if(self.team!!.all { it.isFainted() }) {
                println("${self.name} has no more usable Pokémon!")
            }
            else {
                println("${pokeSelf.name()} fainted!")
                battleScreenNavController.navigate("pokemon_select_on_faint")
                fainted = true
            }
        }
        if(pokeFoe.isFainted()) {
            println("Foe ${pokeFoe.name()} fainted!")
            if(self.team!!.all { it.isFainted() }) {
                println("${self.name} has no more usable Pokémon!")
            }
            else {
                SocketSingleton.socket.once("faint swap pokemon") { parameters ->
                    println(">> PARAMETER -  ${parameters[0] as Int}")
                    executeSwap(false, Action.values()[parameters[0] as Int + 4], true)
                }
            }
            fainted = true
        }
        return fainted
    }

    // Performs a turn
    if (performingMoves) {
        // Execute first action
        if (!actionOneComplete && !moveOngoing) {
            if (selfGoesFirst) {
                executeAction(true, self.nextAction)
                endOfTurnStatus(false)
//                checkFainted()
            } else {
                executeAction(false, foe.nextAction)
                endOfTurnStatus(true)
//                checkFainted()
            }
        }

        // Execute second action
        if (actionOneComplete && !actionTwoComplete && !moveOngoing) {
            if (!selfGoesFirst) {
                executeAction(true, self.nextAction)
                endOfTurnStatus(true)
            } else {
                executeAction(false, foe.nextAction)
                endOfTurnStatus(false)
            }
        }

        // Trigger end of turn phase
        if (actionOneComplete && actionTwoComplete) {

            // Reset "semaphores"
            performingMoves = false
            actionOneComplete = false
            actionTwoComplete = false

            // Return to fight menu
            renderFightMenu = true
        }
    }

    // Background
    Box(modifier = Modifier.fillMaxSize()) {
        Image(
            painter = painterResource(id = R.drawable.battle_bg),
            contentDescription = null,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
        )

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
            // TODO: Make an interesting background. Please. [EXTREMELY HIGH]
    //            .background(Color.LightGray)
        ) {
            Box {
                // Pokemon sprites
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(bottom = 160.dp, top = 10.dp, start = 10.dp, end = 10.dp)
                ) {
                    // Display Pokémon
                    PokemonVisual(pokeSelf, pokeFoe, animateHp, iconAnim, iconAnimTarget)
                }

                // Text box and fight menu
                Row(
                    verticalAlignment = Alignment.Bottom,
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.BottomCenter)
                ) {
                    Box {
                        PokemonTextBox(
                            text = textBoxContent
                        )
                        PokemonFightMenu(
                            render = renderFightMenu,
                            onOptionSelected = { battleScreenNavController.navigate(it) },
                        )
                    }
                }
            }
        }
    }
    // Navigates sub-routes of battle screen
    // Move select, pokemon select, item select
    NavHost(navController = battleScreenNavController, startDestination = "empty") {
        composable("empty") {}
        composable("item_select") {}
        composable("pokemon_select") {
            PokemonSelectScreen(
                pokemonList = pokeListSelf,
                selectedPokemon = LocalData.getSelf().activePokemon,
                onPokeChange = { pokemon ->
                    SocketSingleton.chooseAction(pokemon)
                    animateHp = false
                    battleScreenNavController.navigate("empty")

                    SocketSingleton.socket.once("player-turn") { data ->
                        println("-- PERFORM TURN --")

                        SocketSingleton.madeMove = false
                        LocalData.getTurnData(data[0] as JSONObject)

                        // Determine who goes first
                        selfGoesFirst = whoGoesFirst(pokeSelf, pokeFoe)

                        performingMoves = true
                    }
                }
            )
        }
        composable("move_select") {
            MoveSelect(
                moveList = pokeSelf.moveList,
                onMoveChosen = { move ->
                    SocketSingleton.chooseAction(move)
                    battleScreenNavController.navigate("empty")

                    SocketSingleton.socket.once("player-turn") { data ->
                        println("-- PERFORM TURN --")

                        SocketSingleton.madeMove = false
                        LocalData.getTurnData(data[0] as JSONObject)

                        // Determine who goes first
                        selfGoesFirst = whoGoesFirst(pokeSelf, pokeFoe)

                        performingMoves = true
                    }
                }
            )
        }
        composable("pokemon_select_on_faint") {
            PokemonSelectScreen(
                pokemonList = pokeListSelf,
                selectedPokemon = LocalData.getSelf().activePokemon,
                onPokeChange = { pokemon ->
                    SocketSingleton.swapOnFaint(pokemon.ordinal-4)
                    animateHp = false
                    battleScreenNavController.navigate("empty")
                }
            )
        }
    }
}

// Status boxes (name,level,HP) and Pokémon sprites
@Composable
fun PokemonVisual(
    pokeSelf: Pokemon,
    pokeFoe: Pokemon,
    animateHp: Boolean,
    playAnim: Animation = Animation.NONE,
    animTarget: AnimationTarget = AnimationTarget.SELF
) {
    // Set Pokémon sprite size
    val modifier = Modifier.size(CONFIG_IMAGE_SIZE.dp)

    // Pokemon sprite and status boxes
    Box (
        modifier = Modifier.fillMaxSize()
    ) {
        // Pokemon sprites
        Box (
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    start = 20.dp,
                    end = 20.dp,
                    top = CONFIG_STATUS_BAR_HEIGHT.dp,
                    bottom = CONFIG_STATUS_BAR_HEIGHT.dp
                )
        ) {
            // Foe pokemon sprite
            Box (modifier = Modifier.align(Alignment.TopEnd)) {
                PokemonIcon(
                    painter = painterResource(id = pokeFoe.sprite),
                    faceRight = false,
                    modifier = modifier,
                    playAnim = if(animTarget == AnimationTarget.FOE) playAnim else Animation.NONE
                )
            }
            // Self pokemon sprite
            Box (modifier = Modifier.align(Alignment.BottomStart)) {
                PokemonIcon(
                    painter = painterResource(id = pokeSelf.sprite),
                    faceRight = true,
                    modifier = modifier,
                    playAnim = if(animTarget == AnimationTarget.SELF) playAnim else Animation.NONE
                )
            }
        }
        // Status boxes
        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.Start,
            modifier = Modifier.fillMaxSize()
        ) {
            // Foe pokemon status bar
            PokemonStatusBar(pokemon = pokeFoe, animateHp = animateHp)
            Box (modifier = Modifier.align(Alignment.End)) {
                // Self pokemon status bar
                PokemonStatusBar(pokemon = pokeSelf, animateHp = animateHp, showText = true)
            }
        }
    }
}

// Textbox to communicate with the player
@Composable
fun PokemonTextBox(text: String, modifier: Modifier = Modifier) {
//    val lines = text.split("\n")
    Column (
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
        modifier = modifier
            .height(CONFIG_BOTTOM_MENU_HEIGHT.dp)
            .fillMaxWidth()
            .background(CLR_TEXTBOX_BACKGROUND_OUTER)
            .padding(top = 10.dp, bottom = 5.dp, start = 2.dp, end = 2.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(start = 5.dp, end = 5.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(start = 5.dp, end = 5.dp)
            .border(5.dp, CLR_TEXTBOX_BORDER, RoundedCornerShape(15.dp))
            .padding(4.8.dp)
            .border(2.2.dp, CLR_TEXTBOX_BORDER_SHADOW, RoundedCornerShape(10.dp))
            .background(CLR_TEXTBOX_BACKGROUND_INNER)
            .padding(start = 15.dp, end = 15.dp)
    ) {
        TextBoxText(text = text, lineHeight = 45, modifier = Modifier)
//        TextBoxText(text = if(lines.size > 1) lines[1] else "")
    }
}

// Menu active during fights where player can choose what action to perform
@Composable
fun PokemonFightMenu(
    modifier: Modifier = Modifier,
    render: Boolean = true,
    onOptionSelected: (String) -> Unit
) {
    if(!render) return

    val textModifier = Modifier
        .padding(start = 30.dp)

    Column (
        // Main design
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.Start,
        modifier = modifier
            .height(CONFIG_BOTTOM_MENU_HEIGHT.dp)
            .fillMaxWidth()
            .background(CLR_TEXTBOX_BACKGROUND_OUTER)
            .padding(top = 5.dp, bottom = 0.dp, start = 2.dp, end = 2.dp)
            .border(5.dp, CLR_FIGHTMENU_BORDER_OUTER, RoundedCornerShape(10.dp))
            .padding(5.dp)
            .border(10.dp, CLR_FIGHTMENU_BORDER_INNER, RoundedCornerShape(5.dp))
            .padding(10.dp)
            .border(2.dp, CLR_FIGHTMENU_TEXT_SHADOW)
            .background(CLR_FIGHTMENU_BACKGROUND)
    ) {
        // Menu buttons; upper row (FIGHT, BAG)
        Row (
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(start = 10.dp, end = 5.dp)
                .fillMaxWidth()
        ) {
            FightMenuText(
                text = "FIGHT",
                modifier = textModifier
                    .fillMaxWidth(0.5f)
                    .clickable { onOptionSelected("move_select") }
            )
            FightMenuText(
                text = "PING",
                modifier = textModifier
                    .fillMaxWidth()
                    .clickable { }
            )
        }

        // Menu buttons; lower row (POKéMON, RUN)
        Row (
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(start = 10.dp, end = 5.dp)
        ) {
            FightMenuText(
                text = "POKéMON",
                modifier = textModifier
                    .fillMaxWidth(0.5f)
                    .clickable { onOptionSelected("pokemon_select") }
            )
            FightMenuText(
                text = "QUIT",
                modifier = textModifier
                    .fillMaxWidth()
                    .clickable {
                        onOptionSelected("")
                    }
            )
        }
    }
}

@Composable
fun TextBoxText (text: String, modifier: Modifier = Modifier, lineHeight: Int = 33) {
    Box(
        contentAlignment = Alignment.TopStart,
        modifier = modifier
    ) {
        TextShadow(
            shadowColor = CLR_TEXTBOX_TEXT_SHADOW,
            text = text,
            size = CONFIG_TEXTBOX_TEXT_SIZE,
            fontFamily = CONFIG_TEXTBOX_FONT,
            offset = CONFIG_TEXTBOX_TEXT_SHADOW_OFFSET,
            lineHeight = lineHeight
        )
        Text(
            text = text,
            fontSize = CONFIG_TEXTBOX_TEXT_SIZE.sp,
            color = CLR_TEXTBOX_TEXT,
            fontFamily = CONFIG_TEXTBOX_FONT,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            lineHeight = lineHeight.sp
        )
    }
}

@Composable
fun FightMenuText (
    text: String,
    modifier: Modifier = Modifier,
    selected: Boolean = false
) {
    Row (modifier = modifier) {
        Box {
            TextShadow(
                shadowColor = CLR_FIGHTMENU_TEXT_SHADOW,
                text = text,
                size = CONFIG_FIGHTMENU_TEXT_SIZE
            )
            Text(
                text = text,
                fontSize = CONFIG_FIGHTMENU_TEXT_SIZE.sp,
                color = if(selected) CLR_FIGHTMENU_TEXT_SELECTED else CLR_FIGHTMENU_TEXT,
                fontFamily = FontFamily(Font(font.pokemon_fire_red))
            )
        }
    }
}

// Custom text shadows that match those present in Ruby/Sapphire
// Rewrites text with offsets (x:1, y:0), (x:0, y:1), (x:1, y:1)
@Composable
fun TextShadow(
    shadowColor: Color,
    text: String,
    size: Int,
    offset: Float = 2.8f,
    fontFamily: FontFamily = FontFamily(Font(font.pokemon_fire_red)),
    lineHeight: Int = 0
) {
    Box {
        Text(
            text = text,
            fontSize = size.sp,
            color = shadowColor,
            fontFamily = fontFamily,
            lineHeight = if(lineHeight != 0) lineHeight.sp else 33.sp,
            modifier = Modifier.offset(offset.dp, 0.dp)
        )
        Text(
            text = text,
            fontSize = size.sp,
            color = shadowColor,
            fontFamily = fontFamily,
            lineHeight = if(lineHeight != 0) lineHeight.sp else 33.sp,
            modifier = Modifier.offset(0.dp, offset.dp)
        )
        Text(
            text = text,
            fontSize = size.sp,
            color = shadowColor,
            fontFamily = fontFamily,
            lineHeight = if(lineHeight != 0) lineHeight.sp else 33.sp,
            modifier = Modifier.offset(offset.dp, offset.dp)
        )
    }
}

@Preview
@Composable
fun ComposablePreview() {
    BattleScreen(pokeListSelf = POKEMON_SQUAD, pokeListFoe = POKEMON_SQUAD)
}
