package com.integrasjon.pokemon

import com.integrasjon.pokemon.classes.*

fun waitInThread(duration: Long) {
    Thread.sleep(duration)
}

fun animIsBuff(anim: Animation): Boolean {
    return anim.ordinal in (2..3)
}

fun animIsStatus(anim: Animation): Boolean {
    return anim.ordinal in (5..9)
}

/**
 * Determines what Pokémon will move first on any given turn.
 *
 * Pokémon swaps always precede moves. When both players swap Pokémon the order doesn't matter.
 * If both players use a move the fastest Pokémon goes first, unless one of the moves used has a
 * higher priority than the other (e.g. Quick Attack has a priority of 1, as opposed to the usual 0).
 * Upon a speed tie where both Pokémon have the exact same speed, the first turn is given randomly.
 *
 * @see Pokemon.isFasterThan
 * @author Oddbjørn S. Borge-Jensen
 */
fun whoGoesFirst (pokeSelf: Pokemon, pokeFoe: Pokemon): Boolean {
    var movePriorities: List<Int>? = null

    val actions = listOf(
        LocalData.getFoe().nextAction.ordinal,
        LocalData.getSelf().nextAction.ordinal
    )

    if(actions.all { it < 4 }) {
        movePriorities = listOf(
            pokeSelf.moveList[actions[0]].priority,
            pokeFoe.moveList[actions[1]].priority
        )
    }

    return when {
        // Both actions are pokemon swaps, order doesn't matter
        actions.all { it > 3 } -> true
        // One action is pokemon swap, swap always goes first
        actions.any { it > 3 } -> LocalData.getSelf().nextAction.ordinal > 3
        // Both actions are moves, check move priority
        movePriorities!![0] != movePriorities[1] -> {
            println("Checking move priority: ${movePriorities[0]} vs ${movePriorities[1]}")
            movePriorities[0] > movePriorities[1]
        }
        // Moves have same priority, faster pokemon goes first
        else -> pokeSelf.isFasterThan(pokeFoe)
    }
}