import express, { application } from 'express';
import { createServer} from 'http';
import { Server } from 'socket.io';
import cors from 'cors';
import pg from 'pg';
import multer from 'multer';              //module comes with middleware for handling form data sent to our express application
import cookieParser from 'cookie-parser'; //module with features for parsing and handling cookies in browser from requests
import { login } from './src/components/login.js';
import { registerUser } from './src/components/registerUser.js';
import { retrieveAllUsers } from './src/components/retrieveAllUsers.js';
import { findGame } from './src/components/findGame.js';
import { getUserInfo } from './src/components/getUserInfo.js';
import { getPokemons } from './src/components/getPokemons.js';
import { createGame } from './src/components/createGame.js';
import { getLobby } from './src/components/getLobby.js'
import { getHostInfo } from './src/components/getHostInfo.js';
import { validateHost } from './src/components/validateHost.js';
import { getPlayerInfo } from './src/components/getPlayerInfo.js';
import { resetActions, rng, swapPokemon } from './src/components/gameLogic.js';
//import { SocketAddress } from 'net';

// definere konstanter
const app = express();
const port = 8081;
const host = 'localhost';
const multerDecode = multer(); //decoder for handling formdata from requests


//Variabel for å få utskrift fra diverse console log meldinger. NB alle error meldinger blir printet ut uansett
var pingCount = 0;
var DEBUGG = true;
export { DEBUGG };

// lage en listener på konstantens port
app.listen(port, () => {
    if (DEBUGG) console.log(`-- listening on port: ${port}`);
});

// definasjon av en mulig konfig av cors, ikke i bruk.
var corsOptions = {
    origin: 'http://localhost:8080',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

// definere hva express skal forvente når den mottar http forespørsler
//app.use(cors(corsOptions));  
app.use(express.urlencoded({ extended : true }));
app.use(express.json());
app.use(cookieParser('abcdef-12345'));

// definere headere innholdet til http forspørslene
app.use((req, res, next) => {  
    res.append('Access-Control-Allow-Origin', ['http://localhost:8080']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use(cors({                          //all requests which requires a user to be logged in will have their information sent
    origin: "http://localhost:8080",      //with requests where credentials are included
    credentials: true,
}));   

/******************************
 * Cockroachdb seksjon
 * 
 * kongigurasjon av postgres.
 */
var config = {
    user: 'admin',
    host: '10.212.138.197',
    database: 'defaultdb',
    port: 26257,
};

// definere pool, pool er en postgres pool connection.
const pool = new pg.Pool(config);  
export { pool };    // eksportere pool til de eksterne modulene

// lager en http server og lar socket.io bruke serveren.
const httpServer = createServer(app);
const io = new Server(httpServer, {
    cors: { // cors for socket.io
        origin: "http://localhost:8080",
        methods: ["GET", "POST"],
    },
});

//Når socket kobling opprettes
io.on("connection", (socket) => {
    if (DEBUGG) console.log(`-- new user, id: ${socket.id}`);
    
    // når en bruker emiter msg eventet
    socket.on('msg', (msg) => {
        if (DEBUGG) console.log('message: ' + msg);
    });

    // når en bruker emiter player-fetch eventet
    socket.on('player-fetch', (msg) => {
        if (DEBUGG) console.log('message: ' + msg);
        var roomName = "room" + socket.id;
        console.log("I player-fetch: " + roomName)
        socket.to(roomName).emit('player-fetchpokemon', socket.id); // retur melding    
    });

    socket.on('readyUpp', (parameters) => {
            //deprecated
    });
   
    // sender melding til et room
    socket.on('roomMsg', (parameters) => {
        //if (DEBUGG) console.log("Du er i roomMsg. Parameters.room: " + parameters.room + ". Parameters.msg: " + parameters.msg);
        console.log("-> Pinging " + getLobby(socket))
        socket.to(roomArr[1]).emit("private-message", parameters.message);
    });

    // sender melding til et room
    socket.on('roomMsgAndroid', (parameters) => {
        if (DEBUGG) console.log("Room message fra Android: " + parameters);
        socket.to(parameters).emit("private-message", "heiii");
        // socket.to("public").emit("private-message", parameters.msg);
    });

    // på eventet, lager et rom
    socket.on("createRoom", (data) => {
        var roomName = "room" + socket.id;
        socket.join(roomName);  // socket tilkoblingen blir med i et rom og dermed lager det

        console.log("Room created: " + roomName + "; Host: " + socket.id);
    });

    // på eventet, blir med i rommet
    socket.on("joinGame", (data) => {
        if (DEBUGG) console.log("Du er i join game med socketNr: " + data.room + ", og bruker: " + data.user);
        socket.join(data.room); // socket tilkoblingen blir med i rommet

        //socket.to(data.room).emit('player-joined', socket.id);

        let parameters = {"user_id" : data.user, "socket_id" : socket.id}
        socket.to(data.room).emit('player-joined', parameters); // sender melding til alle andre i rommet
        
        let query = "UPDATE player_lobbies SET player_id = $1, player_socket = $2 WHERE socket_id = $3";
        let values = [data.user, socket.id, data.room];
        
        // oppdaterer databasen, player join player_lobbies
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (DEBUGG) console.log("User " + data.user + " joined room " + data.room);
            }
        });
    });

    // på event, sier man er klar, når begge er klare starter spillet
    socket.on("im-ready", (data) => {
        let lobby = getLobby(socket)    
        let query, isHost;
        //if (DEBUGG) console.log("Du er i join game med socketNr: " + data.room);
        isHost = data.isHost

        
        //Sjekk om denne koblingen er host eller player
        if (data.isHost == true) {
            query = "UPDATE player_lobbies SET host_action = 'ready' WHERE socket_id = $1";
        } else {
            query = "UPDATE player_lobbies SET player_action = 'ready' WHERE socket_id = $1";            
        }
        let values = [lobby];
        //if (DEBUGG) console.log("Lobby: " + lobby);
        
        //oppdater denne spilleren til å være ready
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log("Error i im-ready")
                console.log(err);
            } else {
                if (DEBUGG) console.log("Player " + socket.id + " is ready in " + lobby);

                //se om begge er ready
                let query = "SELECT * from player_lobbies where socket_id = $1 and host_action = 'ready' and player_action = 'ready'";
                pool.query(query, values, (err, result) => {
                    if (err) {
                        console.log(err);
                    } else {
                        if (result.rowCount > 0) {
                            var host_active_pokemon = result.rows[0].host_active_pokemon;
                            var player_active_pokemon = result.rows[0].player_active_pokemon;

                            console.log("Aktive pokemon: " + host_active_pokemon + ", " + player_active_pokemon);

                            // oppdaterer databasen
                            let query = "UPDATE player_lobbies SET host_action = 'waiting', player_action = 'waiting' where socket_id = $1";
                            pool.query(query, values, (err, result) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                  //if (DEBUGG) console.log("Begge spillere er klare, rowCount: " + result.rowCount + ", sender emit til room: " + lobby)
                                    console.log("> Both players are ready!")
                                    var answer = {"host_active_pokemon" : host_active_pokemon, "player_active_pokemon" : player_active_pokemon}
                                    io.to(lobby).emit('match-begin', (answer));  
                                }
                            });
                           
                        } else {
                            console.log("> Waiting for other player to ready up...")
                        }
                    }
                });
            }
        });           
    });

    // på event, oppdaterer databasen og redirekter deg til annen funksjon
    socket.on("swap on faint", (parameters) => {
        let pokemonToSwap = parameters.pokemon;
        let lobby = getLobby(socket)

        let field;
        if(parameters.isHost)
            field = "host_active_pokemon"
        else
            field = "player_active_pokemon"

        query = "UPDATE player_lobbies SET " + field + " = $1 WHERE socket_id = $2"
        values = [pokemonToSwap, lobby]

        // oppdaterer databasen.
        pool.query(query, values, (err, result) => {
            if (err) console.log(err)
            else {
                socket.emit("faint swap pokemon", parameters)   // starter event
            }
        })            
    })

    // på event, sjekker om det er gjort et move og oppdaterer databasen,
    // hvis begge har gjort et move utføres movene.
    socket.on("choose action", (parameters) => {
        let verbose = true
        
        let query, values, field, host_action, player_action, nextAction, bothChosen, user, action;
        let lobby = getLobby(socket)        //Retrieve game lobby socket emit was sent from
        //console.log("Lobby: " + lobby)

        if(parameters.isHost)
            user = "Host"
        else
            user = "Player"

        if(verbose) console.log(user + " has chosen an action [" + parameters.action + "]");
        
        // Query: Get host and player action
        query = "SELECT host_action, player_action FROM player_lobbies WHERE socket_id = $1"
        values = [lobby]

        // Has user already made a move?
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                host_action = result.rows[0].host_action
                player_action = result.rows[0].player_action
                
                // Get appropriate previous action
                if(parameters.isHost == true) {
                    action = host_action
                } else {
                    action = player_action
                }

                if(verbose) console.log("| Prev action: " + action)

                // User has not already chosen an action
                if (action == "waiting") {

                    // Set appropriate field for query
                    if(parameters.isHost == true)  {
                        field = "host_action";
                    } else {
                        field = "player_action";
                    }

                    // Set next action
                    if(parameters.action <= 3 && parameters.action >= 0) {  
                        let n = +parameters.action + 1
                        nextAction = "move" + n
                    } 
                    else if (parameters.action >= 4 && parameters.action <= 9) { 
                        let n = +parameters.action - 3
                        nextAction = "pokemon_swap" + n
                        swapPokemon(lobby, parameters.isHost, nextAction)
                    } 
                    else {
                        console.error("-- ERROR > ACTION OUT OF BOUNDS");
                    }

                    if(verbose) console.log("| Next action: " + nextAction)

                    // Query: Update host or player action
                    query = "UPDATE player_lobbies SET " + field + "= $1 WHERE socket_id = $2;";
                    values = [nextAction, lobby]
    
                    pool.query(query, values, (err, result) => {
                        if (err) {
                            console.log(err);
                        } else {
                            // Check if both users have chosen a move
                            if(parameters.isHost) {
                                bothChosen = player_action != "waiting"
                                host_action = nextAction
                            }
                            else {
                                bothChosen = host_action != "waiting"
                                player_action = nextAction
                            }

                            // Begge har valgt move!
                            if(bothChosen) {
                                if(verbose) console.log("> Both players have chosen a move!")

                                if(parameters.isHost == true) {
                                    action = host_action
                                } else {
                                }
                                //sende tilbake hva som har skjedd til begge spillere
                                var response = {"Player_Action" : player_action, "Host_Action" : host_action, "rng": rng()}

                                console.log(response)
                                
                                io.to(lobby).emit('player-turn', (response)); // utfører movene
                                // Reset host and player actions
                                resetActions(lobby)
                            } else {
                                if(verbose) console.log("> One player has chosen a move!")
                            }
                        }
                    });
                }
                else {
                    if(verbose) console.log("> " + user + " has already chosen an action!");
                }
            }
        });
    });

    // hvis socket koblingen blir avsluttet.
    socket.on("disconnect", (reason) => {
        io.allSockets().then((alleSockets) => {
            if (DEBUGG) console.log("-- Active sockets: " + alleSockets.size)
        });

        /*
        for (const room of socket.rooms) {
            if (room !== socket.id) {
              socket.to(room).emit("msg", "User, " + socket.id + ", has left.");
            }
        }
        */

        // hvis det er noen i rommet
        while(socket.rooms.size > 1) {
            socket.to(getLobby(socket)).emit("msg", "User " + socket.id + " has left the room.")
        }

        //Kanskje ha sjekk i fremtiden på om det fortsatt er andre spillere med i lobby/socket/room

        //Slett socket fra database
        let query = 'DELETE FROM player_lobbies WHERE host_socket=$1;';
        let values = [socket.id];
    
        // oppdaterer databasen
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (DEBUGG) console.log("har sukksessfullt slettet database entry");               
            }
        });
    });   
});

// lager en listener for http servern
httpServer.listen(3000, () => {
    console.log("-- httpServer lisening on port 3000");
});

// når api'et mottar http forespørsel til url /
app.get('/', (req, res) => {
    res.send("Hello world");
});

// retunerer alle brukere
app.get('/retrieveAllUsers', (req, res) => {
   retrieveAllUsers(req, res);
});

// sjekker om brukeren har en cookie og dermed har logget inn
app.get('/isAuth', (req, res) => {
    if (!req.signedCookies.user) {
        res.send(false);
    } else {
        res.send(true);
    }
});

// sammenlikener brukerens oppgitte credintials med informasjonen i databasen
app.post('/login', (req, res) => {
    login(req, res);
});

/**
 * This route creates a new user
 * @see /server/src/components/UserClass - the class of a new user
 */
app.post('/registerUser',multerDecode.none(), function (req,res) {
    registerUser(req, res);
});

/**
 * Gets info about user and users pokemon team.
 * @see /api/src/components/getUserInfo.js
 * @author Fabian Kongelf
 */
app.get('/user', (req, res) => {
    getUserInfo(req, res);
});

// get pokemons
app.get('/pokemons', (req, res) => {
    getPokemons(req, res);
});

/**
 * Gets info about a pokemon.
 * 
 * @author Fabian Kongelf
 */
app.post('/pokemon', (req, res) => {
    let query = "SELECT * FROM pokemon WHERE id = $1";
    let values = [req.body.id];

    pool.query(query, values, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.end(JSON.stringify(result.rows));
        }
    });
});

/**
 * Gets info about a move.
 * 
 * @author Fabian Kongelf
 */
app.post('/moveLite', (req, res) => {
    let query = "SELECT * FROM moves WHERE id = $1";
    let values = [req.body.id];

    pool.query(query, values, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.end(JSON.stringify(result.rows));
        }
    });
});

/**
 * Changes users team.
 * 
 * @author Fabian Kongelf
 */
app.post('/updateTeam', (req, res) => {
    if (DEBUGG) console.log("[/updateTeam] - post - req to /updateTeam");

    let query, values
    
    /*
    switch(req.body.slot) {
        case "pokemon_first": 
            query = "UPDATE users SET pokemon_first = $1 WHERE id = $2"; break;
        case "pokemon_second": 
            query = "UPDATE users SET pokemon_second = $1 WHERE id = $2"; break;
        case "pokemon_third": 
            query = "UPDATE users SET pokemon_third = $1 WHERE id = $2"; break;
        case "pokemon_fourth": 
            query = "UPDATE users SET pokemon_fourth = $1 WHERE id = $2"; break;
        case "pokemon_fifth": 
            query = "UPDATE users SET pokemon_fifth = $1 WHERE id = $2"; break;
        case "pokemon_sixth": 
            query = "UPDATE users SET pokemon_sixth = $1 WHERE id = $2"; break;
    }
    */

    // sjekker om forespørslene er riktig formatert
    if(!req.body.slot in ["pokemon_first", "pokemon_second", "pokemon_third", "pokemon_fourth", "pokemon_fifth", "pokemon_sixth"])
        console.log("-- INVALID QUERY IN UPDATE TEAM")
    else {  
        query = "UPDATE users SET " + req.body.slot + " = $1 WHERE id = $2"
        values = [req.body.new, req.signedCookies.user];

        // bytter ut pokemon i brukerens team
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                console.log("> Successfully swapped pokemon!")
                res.end(JSON.stringify({"msg" : "Changed team composition."}));
            }
        })
    }
});

// finner ledig spill
app.get('/findGame', (req, res) => {
    findGame(req, res);
});

// lager et spill
app.post('/createGame', (req, res) => {
    createGame(req, res);
});

/**
 * Selects all rows from table `moves`
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.get('/retrieveAllMoves', (req, res) => {
    pool.query('SELECT * FROM moves;', function (err, result) {
        if (err) {      
            console.log(err)
            res.status(500)    
        } else {
            JSON.stringify(result);
            if (DEBUGG) console.log("-- Selecting all from moves")
            if (DEBUGG) console.log("> " + result.rowCount + " rows found")

            res.send(result.rows)
        }
    })
});

/**
 * Selects all rows from table `effects`
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.get('/retrieveAllEffects', (req, res) => {
    pool.query('SELECT * FROM effects;', function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result);
            if (DEBUGG) console.log("-- Selecting all from effects")
            if (DEBUGG) console.log("> " + result.rowCount + " rows found")
            
            res.send(result.rows)
        }
    })
});

/**
 * Selects all rows from table `move_has_effects`
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.get('/retrieveMoveEffectRelations', (req, res) => {
    pool.query('SELECT * FROM move_has_effects;', function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result);
            if (DEBUGG) console.log("-- Selecting all from move_has_effects")
            if (DEBUGG) console.log("> " + result.rowCount + " rows found")

            res.send(result.rows)
        }
    })
});

// henter informasjon om game host
app.post('/getHostInfo', (req, res) => {
    getHostInfo(req, res);
})

// henter informasjon om spilleren 
app.post('/getPlayerInfo', (req, res) => {
    getPlayerInfo(req, res);
})

// dobbel sjekker at hosten er den faktiske hosten
app.post('/validateHost', (req, res) => {
    validateHost(req, res);
})

/**
 * Selects id, username, and pokemon ids from a single user
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.post('/retrieveUser', (req, res) => {
    console.log("Du er i retrieveUser, user:" + req.body.user);
    let query = 'SELECT id, username, pokemon_first, pokemon_second, pokemon_third, pokemon_fourth, pokemon_fifth, pokemon_sixth FROM users WHERE id = $1;'
    let values = [req.body.user]

    //console.log(query)

    // henter ut bruker informasjon
    pool.query(query, values, function(err, result) {
        if(err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result)
            res.send(result.rows[0]).status(200)
        }
    })
})

/**
 * Selects a team of six pokemon from
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.post('/retrievePokemonTeam', (req, res) => {
    let team = req.body.team

    let query = 'SELECT * FROM pokemon WHERE id IN ($1, $2, $3, $4, $5, $6);'
    let values = [team[0], team[1], team[2], team[3], team[4], team[5]]

    // henter informasjon om pokemonen i brukerens team
    pool.query(query, values, function (err, result) {
        if(err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result)
            console.log("-- Retrieving team")
            console.log("> Successfully retrieved " + result.rowCount + " pokemon")
            res.send(result.rows).status(200)
        }
    })
})

/**
 * DEPRECATED - Selects moves from a list of moveids
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.post('/retrieveMovelist', (req, res) => {
    let query = 'SELECT * FROM moves WHERE name IN ($1);'
    let moves = ""

    // listen med move id'er fra klienten
    const movelist = req.body.movelist
    movelist.forEach(move => { moves += "\'" + move + "\', " });    //Create comma-seperated string form array
    moves = [moves.substring(0, moves.length-2)]                    //Remove lingering ", "

    console.log(query.substr(0, query.length-4) + moves[0] + ");")
    let cheatQuery = query.substr(0, query.length-4) + moves[0] + ");"

    // henter ut moves
    pool.query(cheatQuery, function (err, result) {
        if(err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result)
            console.log("-- Retrieving movelists")
            console.log("> Successfully retrieved " + result.rowCount + " unique moves")

            res.send(result.rows).status(200)
        }
    })
});

/**
 * Gets all moves from a list of ids, with move effects
 * 
 * @author Oddbjørn S. Borge-Jensen
 */
app.post('/retrieveMovesWithEffects', (req, res) => {
    const movelist = req.body.movelist

    // bygger query
    let query = "SELECT moves.id, moves.name, type, power, moves.target AS move_target, is_physical, power_points, accuracy, priority, crit_ratio, description, moves.animation AS move_anim, "
    query += "moves.animation_target AS move_anim_target, effects.target AS effect_target, apply_chance, status_applied, stats_modified, modified_value, effects.animation AS effect_anim, "
    query += "effects.animation_target AS effect_anim_target FROM moves LEFT OUTER JOIN move_has_effects on moves.id = move_id LEFT OUTER JOIN effects ON effects.id = effect_id "
    query += "WHERE moves.id IN ($1);"

    let moves = ""
    movelist.forEach(move => { moves += "\'" + move + "\', " });        // Create comma-seperated string from array
    moves = [moves.substring(0, moves.length-2)]                        // Remove lingering ", "

    let cheatQuery = query.substr(0, query.length-4) + moves[0] + ");"  // Don't worry about how it works it's stupid
    //console.log(cheatQuery)
    
    // henter moves
    pool.query(cheatQuery, function (err, result) {
        if(err) {
            console.log(err)
            res.status(500)
        } else {
            JSON.stringify(result)
            console.log("-- Retrieving movelists")
            console.log("> Successfully retrieved " + result.rowCount + " unique moves")

            //console.log(result.rows)
            res.send(result.rows).status(200)
        }
    })
})

// returnerer rng
app.get('/rng', (err, res) => {
    res.send(rng())
})