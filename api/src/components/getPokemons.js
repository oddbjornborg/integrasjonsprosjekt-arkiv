import { pool, DEBUGG } from '../../api.js';

/**
 * Returnerer alle pokemons
 * @param req - info fra klient
 * @param res - info som skal til klient
 */
export function getPokemons(req, res) {
    if (DEBUGG) console.log("[/pokemons] - get - req to /pokemons");

    let query = 'SELECT id, name, sprite FROM pokemon';
    
    // henter ut alle pokemons
    pool.query(query, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.end(JSON.stringify(result.rows)); // returnerer resultatet fra databasen
        }
    });
}