import { pool } from "../../api.js"

/**
 * Resets user actions of given player lobby
 * 
 * @param {String} room ID of player lobby 
 * @author Oddbjørn S. Borge-Jensen
 */
export function resetActions(room) {
    let query = "UPDATE player_lobbies SET (player_action, host_action) = (\'waiting\', \'waiting\') WHERE socket_id = $1;"
    let values = [room]

    pool.query(query, values, (res, err) => {
        if(err) {
            console.log(err)
        }
        else {
            console.log("Lobby actions reset!")
        }
    })
}

/**
 * Swaps user's active pokemon based on chosen action.
 * 
 * @param {String} room ID of player lobby
 * @param {Boolean} userIsHost True when user is host
 * @param {String} action String representing the chosen action
 * @author Oddbjørn S. Borge-Jensen
 */
export function swapPokemon(room, userIsHost, action) {
    let user_active_pokemon
    if(userIsHost) user_active_pokemon = "host_active_pokemon"
    else user_active_pokemon = "player_active_pokemon"

    let pokemon_slot
    switch (action) {
        case "pokemon_swap1": pokemon_slot = "pokemon_first"; break;
        case "pokemon_swap2": pokemon_slot = "pokemon_second"; break;
        case "pokemon_swap3": pokemon_slot = "pokemon_third"; break;
        case "pokemon_swap4": pokemon_slot = "pokemon_fourth"; break;
        case "pokemon_swap5": pokemon_slot = "pokemon_fifth"; break;
        case "pokemon_swap6": pokemon_slot = "pokemon_sixth"; break;
        default: console.error("!-- [swapPokemon] ACTION OUT OF EXPECTED RANGE"); break;
    }
    
    let query = "UPDATE player_lobbies SET " + user_active_pokemon + " = $1 WHERE socket_id = $2;"
    let values = [action, room]

    pool.query(query, values, (res, err) => {
        if(err) console.log(err)
        else {
            console.log("> Pokemon swapped!")
        }
    })
}

/**
 * Rolls all necessary rng for performing a turn.
 * 
 * @returns {object} JSON object
 * @see rollUser
 * @see roll
 * @author Oddbjørn S. Borge-Jensen
 */
export function rng() {
    return {
        "speed": roll(2),       // For determining speed ties
        "host": rollUser(), 
        "player": rollUser()
    }
}

/**
 * Rolls a single user's rng.
 * 
 * @returns {object} A single user's rng rolls for a single turn
 * @see roll
 * @author Oddbjørn S. Borge-Jensen
 */
function rollUser() {
    return {
        "accuracy": roll(100), 
        "crit": roll(24), 
        "random": roll(15) + 85, 
        "status": roll(100), 
        "apply": [roll(100), roll(100), roll(100)],  // Will ALMOST definitely never need three values, but will DEFINITELY never need four
        "sleep": roll(3) + 1
    }     
}

/**
 * Rolls a single random number.
 * 
 * @param {Number} roof possible roll (not inclusive) 
 * @returns {Number} A random int between 0 and given roof
 */
function roll(roof) {
    return Math.floor(Math.random() * roof) 
}

