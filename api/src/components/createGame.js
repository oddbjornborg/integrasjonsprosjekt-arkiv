import { pool, DEBUGG } from '../../api.js';

/**
 * Lager et spill
 * @param req - info fra klient
 * @param res - info fra api som går til klient
 */
export function createGame(req, res) {
    // console.log("[/createGame] - post - with socket-id: " + req.body.socket);
    //if (DEBUGG) console.log(req.body);
   
    // definerer om spillet er privat eller ikke.
    let isPrivate = 0
    if(req.body.is_private) isPrivate = 1
    else isPrivate = 0

    let query = 'INSERT INTO player_lobbies(socket_id, name, host_id, host_socket, is_private) VALUES($1, $2, $3, $4, $5)';
    let values = ["room" + req.body.host_socket, req.body.lobby_name, req.signedCookies.user, req.body.host_socket, isPrivate];

    // sender spillet til databasen
    pool.query(query, values, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            if (DEBUGG) console.log("Lobby created: room" + req.body.host_socket);
            res.status(200).end();  // returnerer suksess
        }
    });
}