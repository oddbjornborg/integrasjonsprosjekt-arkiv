import { pool, DEBUGG } from '../../api.js';

/*********************************
 * Validate if user is host or not
 * @author Nicholas Sellevåg
 ********************************/
export function validateHost(req, res) {
    //if (DEBUGG) console.log('[/user] - post - with user-id: ' + req.body.id);

    let query = 'select host_socket from player_lobbies where host_socket = $1';
    let values = [req.body.socket];

    //Query all data about hosts that matches the socket connection
    pool.query(query, values, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            //if there are resulting data the user is in fact the host 
            if (result.rows.length != 0) {
                console.log("validateHost -- er ikke null: " + result.rows.length);
                res.end(JSON.stringify({ "status" : "host"}));
            }
            //if there are no resulting data the user is not the host
            else {
                console.log("validateHost -- er null: " +  result.rows.length);
                res.end(JSON.stringify({ "status" : "user"}))
            }
        }
    });
}