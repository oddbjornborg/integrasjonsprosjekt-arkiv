import { pool, DEBUGG } from '../../api.js';
/*********************************************************
 * Retrieve user information about player who joined lobby
 * @author Nicholas Sellevåg
 ********************************************************/
export function getPlayerInfo(req, res) {

    let query = 'SELECT player_id FROM player_lobbies WHERE host_socket = $1';
    let values = [req.body.socket];
    console.log("i getOpponentinfo -- req.body.socket " + req.body.socket)
    //query information player information
    pool.query(query, values, (err, result) => {
        if (err) {
            console.log("Det ble error i getOpponentInfo");
            console.log(err);
            res.end("alt er dårlig");
        } else {
            // console.log("I getOpponentInfo -- result" + result)
            // console.log("I getOpponentInfo -- rows" + result.rows)
            // console.log("I getOpponentInfo -- playerid" + result.rows[0].player_id)
            query = 'SELECT * FROM users WHERE id = $1';
            values = [result.rows[0].player_id];
            
            pool.query(query, values, (err, result) => {
                const object = result.rows[0];
                var newObject = {};
                newObject["team"] = {};
    
                //Create user's team from responce key values
                (async () => {
                    for (const [key, index] of Object.entries(object)) {
                        if (key.includes("pokemon")) {
                            if (DEBUGG) console.log("old key: " + key + ", index: " + index);
                            let result = await convertIdtoObject(index);
                            newObject["team"][key] = result;
                            if (DEBUGG) console.log("new key: " + key + ", index: " + result);
                        } else {
                            newObject[key] = index;
                        }
                    }
                    res.end(JSON.stringify(newObject));
                })();
            })
        }
    });
}

function convertIdtoObject(id) {
    return new Promise((resolve, reject) => {
        let query = 'SELECT name, level, type_primary, type_secondary FROM pokemon WHERE id = $1'
        let values = [id];
        
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                resolve(result.rows[0]);
            }
        });
    });
}