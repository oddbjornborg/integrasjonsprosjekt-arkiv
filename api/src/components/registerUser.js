import { pool, DEBUGG } from '../../api.js';
import bcrypt from 'bcryptjs';
import {person} from './userClass.js'; //Our own class for which users/moderators/administrators are created from

export function registerUser(req, res) {

var usernameExist = false; //Does username exist?
    console.log(req.body)

    //const hashedPassword = await bcrypt.hash(req.body.password, 10) //- Sjekk om denne kan fjernet med Nicholas
    var salt = bcrypt.genSaltSync(10); //Generate salt
    var hashedPassword = bcrypt.hashSync(req.body.password, salt); //Hasshing the userPassword
    
    var regPers = new person(req.body); //Create a new user based on requests formdata
   
    //If the new users information matches the repeted information and the formdata requirements then chek if username exist
    if (DEBUGG) console.log("er brukernavn tegn gyldig: " + regPers.validateInputUserName());
    if(regPers.matcingInfo() && regPers.validateInputUserName() &&regPers.validatePassword()){
        if (DEBUGG) console.log("All information maching and regex is okay for user: " + regPers.username); //Consoe log out status
  
        //Chek if username exist in DB
        let query = "SELECT COUNT(username) AS numberOfMatch FROM users WHERE username =$1;";
        pool.query(query, [regPers.username], function (err,result) {
            if(err){
                throw err;
            }
      
            //If username not exist
            if(result.rows[0].numberofmatch == 0 ){
                if (DEBUGG) console.log("The Desired username " + regPers.username +" Does not exit and can be used to create a new user");
                usernameExist = false;
                pool.query('INSERT INTO users (username, password_hash, pokemon_first, pokemon_second, pokemon_third, pokemon_fourth, pokemon_fifth, pokemon_sixth) VALUES ($1, $2, 6, 1, 2, 3, 4, 5);',[regPers.username,hashedPassword], function (err, result) {
                    if (err)
                        throw err;
                        if (DEBUGG) console.log("User: " +regPers.username + " Succesfully registerd in DB");
                    res.send("ok"); //send respons to frontend
                });
            }
            else {
                //If username exist
                if (DEBUGG) console.log("Username " + regPers.username + "does exist and cant be used");
                usernameExist = true;
                res.send("UsernameExist") //If username exist
            }
        });        
    }
    //else if from the big if statment, if the username have invalid characthers, send respons frontend and DO NOT REGISTER
    else if(regPers.validateInputUserName() == false) {
        if (DEBUGG) console.log("Username characther not valid for user");
      res.send("UserNameCharNot");
    }
    else if(!regPers.validatePassword()) { //If the password enterd is to short
      res.send("pwToChort");
    }
    else {
      res.send("missMatch"); //hvis inpund data ikke matcher 
    }
}
