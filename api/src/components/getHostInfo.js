import { pool, DEBUGG } from '../../api.js';

/************************************************
 * Retrieve user information about gameLobby host
 * @author Nicholas Sellevåg
 ***********************************************/
export function getHostInfo(req, res) {

    let query = 'SELECT host_id FROM player_lobbies WHERE socket_id = $1';
    let values = [req.body.room];
    // console.log("i getHostinfo -- req.body.socket " + req.body.room)
    pool.query(query, values, (err, result) => {
        if (err) {
            console.log("Det ble error i getOpponentInfo");
            console.log(err);
            res.end("alt er dårlig");
        } else {
            query = 'SELECT * FROM users WHERE id = $1';
            values = [result.rows[0].host_id];
            //Retrieve all information about host
            pool.query(query, values, (err, result) => {
                const object = result.rows[0];
                var newObject = {};
                newObject["team"] = {};
                //For each entry in responce, create team from the responce key values
                (async () => {
                    for (const [key, index] of Object.entries(object)) {
                        if (key.includes("pokemon")) {
                            // if (DEBUGG) console.log("old key: " + key + ", index: " + index);
                            let result = await convertIdtoObject(index);
                            newObject["team"][key] = result;
                            // if (DEBUGG) console.log("new key: " + key + ", index: " + result);
                        } else {
                            newObject[key] = index;
                        }
                    }
                    res.end(JSON.stringify(newObject));
                })();
            })
        }
    });
}

/*******************************
 * Retrieve pokemons attributes
 * @author Nicholas Sellevåg
 ******************************/
function convertIdtoObject(id) {
    return new Promise((resolve, reject) => {
        let query = 'SELECT name, level, type_primary, type_secondary FROM pokemon WHERE id = $1'
        let values = [id];
        
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                resolve(result.rows[0]);
            }
        });
    });
}