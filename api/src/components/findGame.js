import { pool, DEBUGG } from '../../api.js';

/**
 * finner et spill
 * @param req - info fra klient
 * @param res - info fra api som går til klient
 */
export function findGame(req, res) {
    let query = 'SELECT * FROM player_lobbies where player_id IS NULL AND is_private = 0 LIMIT 1';

    // sjekker om det er noen ledig spill
    pool.query(query, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // gjør dataen fra databasen lesbar
            let games = JSON.stringify(result.rows);
            games = JSON.parse(games);
            
            // hvis det er minst et spill
            if (games.length > 0) {
                let game = games[0]; // legger alle spillene i en variable
                game["user"] = req.signedCookies.user;

                //if (DEBUGG) console.log(JSON.stringify(game));
                res.end(JSON.stringify(game)); // returnerer alle spillene
            } else {
                res.status(400).end(JSON.stringify({"msg" : "Ingen games."}));
            }       
        }
    });
}