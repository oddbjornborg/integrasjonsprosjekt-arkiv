import { pool, DEBUGG } from '../../api.js';

/**
 * Returnerer alle brukere
 * @param req - info fra klient
 * @param res - info som skal til klient
 */
export function retrieveAllUsers(req, res) {
    // henter litt info om alle brukere
    pool.query('SELECT username, wins, losses FROM users;', function (err, result) {
        if (err) {      //If the Sql query fails
            console.log("Det var en error i retrieveAllUsersQuery")
            res.send("SQL did not work")    //Should put a warning in the response instead
        } else {
            JSON.stringify(result);
            if (DEBUGG) console.log("------------------------Nytt Sok------------------------")

            if (DEBUGG) console.log(result.rows)
            var allUsers = result.rows;
            
            if (DEBUGG) console.log(allUsers)
            res.send(allUsers)
        }
      })
}