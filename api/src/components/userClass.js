import { DEBUGG } from '../../api.js';

/**
 * This class is used when register a new user to the forum
 */
class person{
    constructor(dataE){
      this.password = dataE.password;
      this.repeatPassword =dataE.repeatPassword;
      this.username = dataE.username;
    }
    matcingInfo(){
      //validating thath input data maches
      if(this.password === this.repeatPassword){
        if (DEBUGG) console.log('Match');
        return true;
      }
      else {
        if (DEBUGG) console.log('noMatch');
        return false;
      }
    }
    //Input validation of username
    validateInputUserName(){
      var userNameExp = new RegExp("[a-z0-9A-Z]{2,63}$"); //what we axept of valid characthers of a username
      if(userNameExp.test(this.username)){ //If the username only consist of valid characthers
        if (DEBUGG) console.log("Username charcther valid for user with email: " + this.email);
        return true;
      }
      else{ //If the username consist of ileagal characthers
        if (DEBUGG) console.log("Username characthers not valid! for user with email: " + this.email);
        return false;
      }
    }
    validatePassword(){
      if (this.password.length >= 8){ //Chek if the pw is grather than 8 characthers
        if (DEBUGG) console.log("Pw lengt for user " + this.username + "is okay")
        return true;
      }
      else{ //If the pw iss less than 8 characther
        if (DEBUGG) console.log("Pw lengt for user " + this.username + " is lower then requerd 8 ");
        return false;
      }
      
    }
  }
export{person}