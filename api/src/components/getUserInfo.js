import { pool, DEBUGG } from '../../api.js';
export function getUserInfo(req, res) {
    //if (DEBUGG) console.log('[/user] - post - with user-id: ' + req.body.id);

    let query = 'SELECT * FROM users WHERE id = $1';
    let values = [req.signedCookies.user];

    //Retrieve all user info from user with id
    pool.query(query, values, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            const object = result.rows[0];
            var newObject = {};
            newObject["team"] = {};

            //Parse user's pokemon team data from responce key values 
            (async () => {
                for (const [key, index] of Object.entries(object)) {
                    if (key.includes("pokemon")) {
                        //if (DEBUGG) console.log("old key: " + key + ", index: " + index);
                        let result = await convertIdtoObject(index);
                        newObject["team"][key] = result;
                        //if (DEBUGG) console.log("new key: " + key + ", index: " + result);
                    } else {
                        newObject[key] = index;
                    }
                }
                res.end(JSON.stringify(newObject));
            })();
        }
    });
}

/************************************************
 * Retrieve all information about pokemon with id
 * @author Nicholas Sellevåg
 ************************************************/
function convertIdtoObject(id) {
    return new Promise((resolve, reject) => {
        let query = 'SELECT id, name, level, type_primary, type_secondary FROM pokemon WHERE id = $1'
        let values = [id];
        
        pool.query(query, values, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                resolve(result.rows[0]);
            }
        });
    });
}