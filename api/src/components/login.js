import { pool, DEBUGG } from '../../api.js';
import bcrypt from 'bcryptjs';

export function login(req, res) {
    if (DEBUGG) console.log("Inside login");
    
    const username = req.body.username;
    const password = req.body.password;

    if (!req.signedCookies.user || req.signedCookies.user) {    // NB!! ALWAYS TRUE FOR TESTING PURPSES - DONT FORGET TO REVERT THIS SHIT
        //if (DEBUGG) console.log("-- loging in user");
        
        //let query = 'SELECT username, password_hash, id FROM users;'
        let query = 'SELECT username, password_hash, id FROM users WHERE username = $1'
        let values = [username]

        pool.query(query, values, (err, result) => {
            if (err) {      //If the Sql query fails
                console.log("-- Cannot reach database");
                console.log(err);
                res.status(500).end("Cannot reach database");                           //Internal server error
            } 
            else {
                let users = result.rows;   
                // if (DEBUGG) console.log(users);
                const found = users.find(element => element.username == username);      //See if request's username matches an entry in database 
            
                if (found == null) {                                                    //user was not found/registered in database
                    if (DEBUGG) console.log("-- Cannot find user with username " + username);
                    res.status(401).end("Unknow username or password");
                } 
                else {                                                                  //user wat found in database
                    //if (DEBUGG) console.log("found.id: " + found.id);

                    if (username == found.username && bcrypt.compareSync(password, found.password_hash)) {
                        res.cookie('user', found.id, { signed:true });
                        //if (DEBUGG) console.log("-- Login successfull");
                        if(DEBUGG) console.log("-- Logged in: " + found.id + " - " + result.rows[0].username)

                        let json = {"username": result.rows[0].username, "id": result.rows[0].id}
                        //console.log(json)
                        res.status(200).send(json);
                    } 
                    else {
                        if (DEBUGG) console.log("-- user used wrong password");
                        res.status(401).end("Unknow username or password");
                    }    
                }
            }
        });    
    } 
    else {                                                                              //user already has a 'user' cookie and is as of now considered authenticated
        if (DEBUGG) console.log("-- user allready logged in");
        res.status(400).send("allready logged in");
    }
}