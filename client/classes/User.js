export class User {
    constructor(parameter) {

        this.id= parameter.id;
        this.name = parameter.username;
        // this.socketID = parameter.socketID;
        this.team = [];

        this.teamIds = [];
        for (var key in parameter) {
            if (parameter.hasOwnProperty(key)) {
                // console.log(key + " -> " + data[key]);
                if(key.startsWith("pokemon_")) {
                    this.teamIds.push(parameter[key])
                }
            }
        }
    }

    setTeam(pokemons) {
        this.team = pokemons;
    }
    
    print() {
        console.log("id: " + this.id)
        console.log("name: " + this.name)
        console.log("teamIds: " + this.teamIds)
        console.log("team: " + this.team)
        // console.log("socketID: " + this.socketID)
    }
}