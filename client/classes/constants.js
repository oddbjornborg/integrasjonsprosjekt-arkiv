// TODO: Change to using enum instead of constants [LOW]
import { TypeMatchup } from "./TypesMatch"

export const Action = {
    MOVE1: "MOVE1",  
    MOVE2: "MOVE1",  
    MOVE3: "MOVE1",  
    MOVE4: "MOVE1",  
    POKE1: "POKE1",  
    POKE2: "POKE2",  
    POKE3: "POKE3",  
    POKE4: "POKE4",  
    POKE5: "POKE5",  
    POKE6: "POKE6",  
    WAITING: "WAITING",      
}

// NB! Don't mess with the order!
export const Animation = {
    NONE: "NONE",
    ATTACK:"ATTACK",
    BUFF:"BUFF",
    DEBUFF:"DEBUFF",
    FAINT: "FAINT",
    PARALYSIS: "PARALYSIS",
    BURN:"BURN",
    POISON:"POISON",
    SLEEP:"SLEEP",
    FREEZE:"FREEZE",
}

export const AnimationTarget = {
    SELF: "SELF",
    FOE: "FOE",
}

export const Stat = {
    ATTACK: "stat_attack",
    DEFENSE: "stat_defense",
    SPECIAL_ATTACK: "stat_special_attack",
    SPECIAL_DEFENSE: "stat_special_defense",
    SPEED: "stat_speed",
    ACCURACY: "stat_accuracy",
    EVASION: "stat_evasion",

    // fun getRes(): Int {return stringRes}
}

export const StatusCondition = {
    NONE:"none",
    BURN:"burn",
    FREEZE:"freeze",
    PARALYSIS:"para",
    POISON:"poison",
    BAD_POISON:"bad_poison",
    SLEEP:"sleep",
}

// TODO: Change to using enum if you dare [LOW]
export const Type = {
    NORMAL: "normal",
    FIGHT: "fight",
    FLYING: "flying",
    POISON: "poison",
    GROUND: "ground",
    ROCK: "rock",
    BUG: "bug",
    GHOST: "ghost",
    STEEL: "steel",
    FIRE: "fire",
    WATER: "water",
    GRASS: "grass",
    ELECTRIC: "electric",
    PSYCHIC: "psychic",
    ICE: "ice",
    DRAGON: "dragon",
    DARK: "dark",
}

export class Constants {
    // Stats
    constructor() {

        this.STAT_ATTACK = 0;
        this.STAT_SPECIAL_ATTACK = 1;
        this.STAT_DEFENSE = 2;
        this.STAT_SPECIAL_DEFENSE = 3;
        this.STAT_SPEED = 4;
        this.STAT_ACCURACY = 5;
        this.STAT_EVASION = 6;
        // this.STAT_TO_STRING = {
        //     ZERO: "attack",
        //     ONE: "special attack",
        //     TWO: "defense",
        //     THREE: "special defense",
        //     FOUR: "speed",
        //     FIVE: "accuracy",
        //     SIX: "evasion",
        // } 
        
    }   
    // static get constant1() {
    
    // }
            
}
         
export const STAT_TO_STRING = {
    0: "attack",
    1: "special attack",
    2: "defense",
    3: "special defense",
    4: "speed",
    5: "accuracy",
    6: "evasion",
}

// Text box string
export const TEXT_MISS = "But it missed!"
export const TEXT_SUPER_EFFECTIVE = "It's super effective!"
export const TEXT_NOT_EFFECTIVE = "It's not very effective..."
export const TEXT_CRITICAL = "A critical hit!"
export const TEXT_FAIL = "But it failed!"

export let TYPE_MAP= {};

// TYPE_MAP.set(Type.ROCK,new TypeMatchup(
//     [], 
    
//     [Type.FIGHT, Type.GROUND, Type.STEEL], 
    
//     [Type.FLYING, Type.BUG, Type.FIRE, Type.ICE], 
//     ))
// TYPE_MAP.set(Type.BUG,new TypeMatchup(
//     [], 
    
//     [Type.FIGHT, Type.FLYING, Type.POISON, Type.GHOST, Type.STEEL, Type.FIRE], 
    
//     [Type.GRASS, Type.PSYCHIC], 
//     ))
// TYPE_MAP.set(Type.GHOST, new TypeMatchup(
//     [Type.NORMAL], 
    
//     [Type.STEEL, Type.DARK], 
    
//     [Type.GHOST, Type.PSYCHIC], 
//     ))
// TYPE_MAP.set(Type.STEEL,new TypeMatchup(
//     [], 
    
//     [Type.STEEL, Type.FIRE, Type.WATER, Type.ELECTRIC], 
    
//     [Type.ROCK, Type.ICE], 
//     ))
// TYPE_MAP.set(Type.FIRE,new TypeMatchup(
//     [], 
    
//     [Type.ROCK, Type.FIRE, Type.WATER, Type.DRAGON], 
    
//     [Type.BUG, Type.STEEL, Type.GRASS, Type.ICE], 
//     ))
// TYPE_MAP.set(Type.WATER,new TypeMatchup(
//     [], 
    
//     [Type.WATER, Type.GRASS, Type.DRAGON], 
    
//     [Type.GROUND, Type.ROCK, Type.FIRE], 
//     ))
// TYPE_MAP.set(Type.GRASS,new TypeMatchup(
//     [], 
    
//     [Type.FLYING, Type.POISON, Type.BUG, Type.STEEL, Type.FIRE, Type.GRASS, Type.DRAGON], 
    
//     [Type.GROUND, Type.ROCK, Type.WATER], 
//     ))
// TYPE_MAP.set(Type.ELECTRIC,new TypeMatchup(
//     [Type.GROUND], 
    
//     [Type.GRASS, Type.ELECTRIC, Type.DRAGON], 
    
//     [Type.FLYING, Type.WATER], 
//     )
// )
// TYPE_MAP.set(Type.PSYCHIC,new TypeMatchup(
//     [Type.DARK], 
    
//     [Type.STEEL, Type.PSYCHIC], 
    
//     [Type.FIGHT, Type.POISON], 
//     ))
// TYPE_MAP.set(Type.ICE,new TypeMatchup(
//     [], 
    
//     [Type.STEEL, Type.FIRE, Type.WATER, Type.ICE], 
    
//     [Type.FLYING, Type.GROUND, Type.GRASS, Type.DRAGON], 
//     ))
// TYPE_MAP.set(Type.DRAGON,new TypeMatchup(
//     [], 
    
//     [Type.STEEL], 
    
//     [Type.DRAGON], 
//     ))
// TYPE_MAP.set(Type.DARK,new TypeMatchup(
//     [], 
    
//     [Type.FIGHT, Type.STEEL, Type.DARK], 
    
//     [Type.GHOST, Type.PSYCHIC], 
//     ))



    







    
TYPE_MAP[Type.NORMAL] = new TypeMatchup(
    [Type.GHOST], 
    
    [Type.ROCK, Type.STEEL], 
    
    [])

TYPE_MAP[Type.FIGHT] = new TypeMatchup(
    [Type.GHOST], 
    
    [Type.FLYING, Type.POISON, Type.BUG, Type.PSYCHIC], 
    
    [Type.NORMAL, Type.ROCK, Type.STEEL, Type.ICE, Type.DARK], 
    )

TYPE_MAP[Type.FLYING] = new TypeMatchup(
    [], 
    
    [Type.ROCK, Type.STEEL, Type.ELECTRIC], 
    
    [Type.FIGHT, Type.BUG, Type.GRASS], 
    )

TYPE_MAP[Type.POISON] = new TypeMatchup(
    [Type.STEEL], 
    
    [Type.POISON, Type.GROUND, Type.ROCK, Type.GHOST], 
    
    [Type.GRASS], 
    )

TYPE_MAP[Type.GROUND] = new TypeMatchup(
    [Type.FLYING], 
    
    [Type.BUG, Type.GRASS], 
    
    [Type.POISON, Type.ROCK, Type.STEEL, Type.FIRE, Type.ELECTRIC], 
    )

TYPE_MAP[Type.ROCK] = new TypeMatchup(
    [], 
    
    [Type.FIGHT, Type.GROUND, Type.STEEL], 
    
    [Type.FLYING, Type.BUG, Type.FIRE, Type.ICE], 
    )

TYPE_MAP[Type.BUG] = new TypeMatchup(
    [], 
    
    [Type.FIGHT, Type.FLYING, Type.POISON, Type.GHOST, Type.STEEL, Type.FIRE], 
    
    [Type.GRASS, Type.PSYCHIC], 
    )

TYPE_MAP[Type.GHOST] = new TypeMatchup(
    [Type.NORMAL], 
    
    [Type.STEEL, Type.DARK], 
    
    [Type.GHOST, Type.PSYCHIC], 
    )

TYPE_MAP[Type.STEEL] = new TypeMatchup(
    [], 
    
    [Type.STEEL, Type.FIRE, Type.WATER, Type.ELECTRIC], 
    
    [Type.ROCK, Type.ICE], 
    )

TYPE_MAP[Type.FIRE] = new TypeMatchup(
    [], 
    
    [Type.ROCK, Type.FIRE, Type.WATER, Type.DRAGON], 
    
    [Type.BUG, Type.STEEL, Type.GRASS, Type.ICE], 
    )
TYPE_MAP[Type.WATER] = new TypeMatchup(
    [], 
    
    [Type.WATER, Type.GRASS, Type.DRAGON], 
    
    [Type.GROUND, Type.ROCK, Type.FIRE], 
    )

TYPE_MAP[Type.GRASS] = new TypeMatchup(
    [], 
    
    [Type.FLYING, Type.POISON, Type.BUG, Type.STEEL, Type.FIRE, Type.GRASS, Type.DRAGON], 
    
    [Type.GROUND, Type.ROCK, Type.WATER], 
    )

TYPE_MAP[Type.ELECTRIC] = new TypeMatchup(
    [Type.GROUND], 
    
    [Type.GRASS, Type.ELECTRIC, Type.DRAGON], 
    
    [Type.FLYING, Type.WATER], 
    )

TYPE_MAP[Type.PSYCHIC] = new TypeMatchup(
    [Type.DARK], 
    
    [Type.STEEL, Type.PSYCHIC], 
    
    [Type.FIGHT, Type.POISON], 
    )

TYPE_MAP[Type.ICE] = new TypeMatchup(
    [], 
    
    [Type.STEEL, Type.FIRE, Type.WATER, Type.ICE], 
    
    [Type.FLYING, Type.GROUND, Type.GRASS, Type.DRAGON], 
    )

TYPE_MAP[Type.DRAGON] = new TypeMatchup(
    [], 
    
    [Type.STEEL], 
    
    [Type.DRAGON], 
    )

TYPE_MAP[Type.DARK] = new TypeMatchup(
    [], 
    
    [Type.FIGHT, Type.STEEL, Type.DARK], 
    
    [Type.GHOST, Type.PSYCHIC], 
    )
