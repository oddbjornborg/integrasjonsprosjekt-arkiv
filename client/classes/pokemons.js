import * as constant from './constants'
import {StatusResponse} from './StatusResponse'
import { Move } from './Move';

var constantClass = new constant.Constants();
export class Pokemon {
    constructor(parameters) {
        this.name = parameters.name;
        this.level = parameters.level;
        this.hpMax = parameters.hp;
        this.attack = parameters.attack;
        this.defense = parameters.defense;
        this.spAttack = parameters.special_attack;
        this.spDefense = parameters.special_defence;
        this.speed = parameters.speed;
        this.types = [];
        this.types.push(parameters.type_primary);
        this.types.push(parameters.type_secondary);

        // for (var key in parameters.types) {
        //     if (parameters.types[key].hasOwnProperty(key)) {
        //     this.types.push(parameters.types[key]);
        //     };
        // }

        this.sprite = parameters.sprite;
        this.sex = parameters.sex;
        this.moveIds = [];
        this.moveIds.push(parameters.move_first)
        this.moveIds.push(parameters.move_second)
        this.moveIds.push(parameters.move_third)
        this.moveIds.push(parameters.move_fourth)

        this.moveList = [];

        // for (var key in parameters.types) {
        //     if (parameters.types[key].hasOwnProperty(key)) {
        //     this.types.push(parameters.types[key]);
        //     };
        // }

        this.statStages = [0,0,0,0,0,0,0];
        this.hpCurrent = this.hpMax;
        this.statusCondition = constant.StatusCondition.NONE;
        this.auxiliaryStatus = [];
        this.statusCounter = 0;
        this.statusDuration = 0;
    }
    
    printPokemon() {
        // console.log("name: " + this.name)
        console.log("hp: " + this.hpMax)
        console.log("level: " + this.level)
        console.log("movelist: " + this.moveList)
    }

    useMove(slot, target, rolls) {
        this.moveList[slot].use(this, target, rolls)
    }

    checkImmobilizingStatus() {
        var message = [];
        var immobilized = false

        // SLEEP
        if (this.statusCondition == constant.StatusCondition.SLEEP) {
            console.log("Status duration (sleep): " + this.statusDuration)
            if(this.statusDuration == 0) {
                message.push(this.name + " woke up")
                resetStatus()
            } else {
                Log.println(Log.DEBUG, "poke/status", "${name()} is fast asleep")
                this.statusDuration--
                message.push(this.name + "is fast asleep!")
                immobilized = true
            }
        }

        // PARALYSIS
        else if (this.statusCondition == constant.StatusCondition.PARALYSIS) {
            var paraRoll = this.getRandomArbitrary(0, 100);
            console.log("Paralysis roll: " + paraRoll)
            if(paraRoll < 50) {
                console.log(this.name + "was immobilized by paralysis")
                message.push(this.name + "was immobilized by paralysis");
                immobilized = true
            } else console.log(this.name + "was unaffected by paralysis")
        }

        // FREEZE
        else if (this.statusCondition == constant.StatusCondition.FREEZE) {
            var unfreezeRoll = this.getRandomArbitrary(0, 100);
            console.log("Freeze roll: " + unfreezeRoll)
            if(unfreezeRoll < 20) {
                console.log(this.name + " was unfrozen")
                message.push(this.name + " is no longer frozen!")
                this.resetStatus()
            } else {
                console.log(this.name + " is frozen")
                message.push(this.name + " is frozen solid!")
                immobilized = true
            }
        }

        //usikker på hvordan riktig returnering blir her
        return new StatusResponse(immobilized, message)
    }

    checkEndOfTurnStatus() {
        var message = [];

        // BURN
        if (this.statusCondition == constant.StatusCondition.BURN) {
            console.log(this.name + " is hurt by its burn!")

            message.push(this.name + " is hurt by its burn!")

            this.takeDamage(this.hpMax / 8 )
        }

        // POISON
        else if (this.statusCondition == constant.StatusCondition.POISON) {
            console.log(this.name + " is hurt by poison!")

            message.push(this.name + " is hurt by poison!")

            this.takeDamage(this.hpMax / 8 )
        }

        // BADLY POISONED
        else if (this.statusCondition == constant.StatusCondition.BAD_POISON) {
            console.log(this.name + " is hurt by bad poison!")
            console.log("-- Status counter: " + this.statusCounter)
            var dmgDealt = this.statusCounter / 8;
            console.log("-- Damage dealt: " + dmgDealt)

            message.push(this.name + " is hurt by poison!")

            this.takeDamage((this.hpMax * (this.statusCounter / 8)))
            if(this.statusCounter < 8) this.statusCounter++

        }

        if (message.isNotEmpty()) {
            return message
        } else {
            return null
        }
        // return if (message.isNotEmpty()) MessageFlag(message, anim) else null
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    forceAddMove(move){
        if(this.moveList.length < 4)
            this.moveList.push(move)
        else {
            for (var i = 3; i > 0; i--) {
                this.moveList[i] = this.moveList[i - 1]
            }

            this.moveList[0] = move
        }
    }

    takeDamage(damage) {
        if ((this.hpCurrent - damage) < 0) {
            this.hpCurrent == 0;
            this.faint();
        } else {
            this.hpCurrent = this.hpCurrent - damage;
        }
    }

    modifyStatStage(modifiedStat, modifiedValue) {
        console.log("-- Successfully applied stat changes to " + this.name)

        var temp;
        if(modifiedValue > 0){
            temp = Math.min(this.statStages[modifiedStat] + modifiedValue, 6)
        } else {
            temp = Math.max(this.statStages[modifiedStat] + modifiedValue, -6)
        }
        this.statStages[modifiedStat] = temp

        console.log("-- Current " + constant.STAT_TO_STRING[modifiedStat] + " stage: " + this.statStages[modifiedStat])
    }

applyStatus(appliedStatus) {
    if(this.statusCondition == constant.StatusCondition.NONE) {
        console.log("-- Successfully applied status to " + this.name)

            this.resetStatus()
            this.statusCondition = appliedStatus

            // Set specific status parameters
        if (this.statusCondition == constant.StatusCondition.SLEEP) {
            this.statusDuration = this.getRandomArbitrary(1, 3)
            return true
        }
        else if (this.statusCondition == constant.StatusCondition.BAD_POISON) this.statusCounter = 1
            return true

    } else {
        console.log("-- " + this.name + " already has a status condition!")
        return false
        }
    }

    resetStatus() {
        this.statusCounter = 0
        this.statusCondition = constant.StatusCondition.NONE
    }

    faint() {
        this.resetStatus()
        this.auxiliaryStatus = ["status_fainted"]
    }
    isFainted() {
        return (this.auxiliaryStatus.includes("status_fainted"))
    }

     /**
     * @see Move.statMultiplier
     */
      statMultiplier(stage) {
        // Base multiplier:     2 / 2
        // For positive stages: 2 + stage / 2
        // For negative stages: 2 / 2 + stage

        console.log("Stat stage: " + stage)

        // return
        if(stage > 0) {
            var temp = stage + 2;
            console.log("-- Stat multiplier: " + temp + "/2")
            return ((2 + stage) / 2)
        }
        else {
            var temp = 2 - stage;
            console.log("-- Stat multiplier: 2/" + temp)
            return (2 / (2 - stage))
        }
    }

    speedCheck() {
        var paralysis;
        if(this.statusCondition == constant.StatusCondition.PARALYSIS) {
            paralysis = 0.5;
        } else {
            paralysis = 1.0;
        }
        return this.speed * this.statMultiplier(this.statStages[constantClass.STAT_SPEED]) * paralysis
    }

      /**
     * Determines which pokemon gets to act first.
     *
     * Returns true when self is faster, else false. In the case of a tie the first move is
     * rewarded randomly.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param foe Opposing pokemon to compare speed with.
     */
       isFasterThan(foe) {
        var selfSpeed = this.speedCheck()
        var foeSpeed = foe.speedCheck()

        console.log(this.name + " SPEED: " + selfSpeed + ",Foe: " + foe.name + "'s SPEED: " + foeSpeed)

        var res;
        if(selfSpeed > foeSpeed) {
           res = true;
        } else if(selfSpeed < foeSpeed) {
            res = false;
        } else {
            res = this.getRandomArbitrary(0, 1);
        }

        if (res) {
            console.log(this.name + " goes first")
        } else {
            console.log("Foe goes first")
        }

        return res
    }

    myName() {
        return String.prototype.toUpperCase(this.name)
    }


}