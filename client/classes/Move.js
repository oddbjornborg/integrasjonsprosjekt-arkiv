import * as constants from './constants';
import { Pair } from './pair';
import { MoveEffect } from './MoveEffects';

var constantClass = new constants.Constants();
export class Move {
    constructor(parameters) {
        // console.log("Move---: " + parameters.name + ", har parametre: ")
        //    for (var key in parameters) {
        //         if (parameters.hasOwnProperty(key)) {
        //             console.log(key + " -> " + parameters[key]);
        //             for (var nkey in parameters[key]) {
        //                 if (parameters[key].hasOwnProperty(nkey)) {
        //                     console.log(nkey + " -> " + parameters[key][nkey]);
        //                 }
        //             }
        //         }
        //     }
        this.name = parameters.name;
        this.type = parameters.type;
        this.power = parameters.power;
        this.isPhysical = parameters.is_physical;
        this.moveEffects = [];

        if(parameters.effect_target) {
            // console.log("addded effect i construktor på move: " + this.name)
            this.moveEffects.push(this.addMoveEffect(parameters));
        }
        this.ppMax = parameters.power_points;
        this.accuracy = parameters.accuracy;
        this.priority = parameters.priority;
        this.critRatio = parameters.crit_ratio;
        this.description = parameters.description; 
        this.animFlag = parameters.move_anim;
        this.animTarget = parameters.move_anim_target;
        this.ppCurrent = this.ppMax
        
    }

    addMoveEffect(parameters) {
        // console.log("addMoveEffect: " + parameters.effect_target)
        this.moveEffects.push(new MoveEffect(
            parameters.effect_target, 
            parameters.apply_chance, 
            parameters.status_applied, 
            this.parseStats(parameters.stats_modified), 
            parameters.modified_value, 
            parameters.effect_anim)); 
    }

    parseStats(stats) {
        let statList = []
        for(var i = 0; i < stats.length; i++) {
            if(stats[i] == 1) {
                statList.push(i)
            }
        }
        return statList
    }



    // *-- MAIN MOVE FUNCTION --* //

    /**
     * Performs move. All other non-getter move functions are called from here.
     *
     * Checks whether a move has an effect on the target depending on type matchup, then whether or
     * not the move hits. Then the damage of the move is calculated and any move effects are
     * executed. Any triggered effect or result that has an associated message flag will have said
     * flag added to the message queue to be returned.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return A queue of MessageFlags, containing a text message, an animation,
     * and an animation target.
     *
     * @see hasEffect
     * @see doesMoveHit
     * @see calculateMoveDamage
     * @see executeMoveEffects
     */
    use(user, target, rolls) {
        console.log(user.name + " used " + this.getName())
        // Initialize queue of message flags (text, animation, animation target)
        var messageQueue = [];

        // Deduct PP
        this.ppCurrent--
        
        
        // Type matchup calculation
        if(!this.hasEffect(target)) {
            return messageQueue.push("It had no effect on opposing " + target.name + "...")
        }

        // Move hit calculation
        if(!this.doesMoveHit(user, target)) {
            return messageQueue.push(user.name + "'s attack missed!")
        }

        // Calculate and deal damage
        if(this.power > 0) {
            // Response -> Pair(damage: Int, queue: Queue<String>)
            var response = this.calculateMoveDamage(user, target)
                        
            response.second.forEach (messageFlag => { 
                messageQueue.push(messageFlag)
            })
            console.log("HP før: " + target.hpCurrent)
            target.takeDamage(response.first)
            console.log("HP etter: " + target.hpCurrent)
        }

        // Execute move effects
        var effectQueue = this.executeMoveEffects(user, target)
        effectQueue.forEach (it => { messageQueue.push(it) })

        return messageQueue
    }


       /**
     * Determines whether or not move successfully hits the target.
     *
     * Rolls a random integer in the range 0-99. Determines whether a move hits by checking if
     * this number is less than the move's accuracy times a multiplier of user's accuracy minus
     * target's evasion. Moves with 0 accuracy are considered always-hit-moves, and are exempt
     * from this formula, always returning true.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return True if move hits, false if miss
     * @see getHitStageSum
     * @see hitStatMultiplier
     * @see use
     */
        doesMoveHit(user, target) {
            // Check for always hit moves
            if(this.accuracy == 0) {
                console.log("Hit: Always hit move")
                return true
            }
    
            // User's accuracy minus target's evasion determines stat stage to use for calculation
            var hitStageSum = this.getHitStageSum(user, target)
    
            // Set target hit value that must be rolled less than
            var toHit = this.accuracy * this.hitStatMultiplier(hitStageSum)
    
            // Actual hit roll, a random number 0-99
            var hitRoll = this.getRandomArbitrary(0, 99)
    
            // Boolean to be returned. Returns true for roll less than target
            var isHit = (hitRoll < toHit)
    
            var hitText;
            if(isHit) {
                hitText ="Hit!"    
            } 
            else {
                hitText = "Miss!"
            }

            console.log(hitText + " |  To hit: " + toHit + "|  Hit roll: " + hitRoll)
    
            return isHit
        }


         /**
     * Unique stat multiplier formula for accuracy and evasion.
     *
     * Takes the base fraction 3/3 and adds the absolute value of the given stage to either the
     * numerator or the denominator, depending on whether the value of the stage is negative or
     * positive.
     *
     * E.g. with a given stage og 2 the resulting fraction will be 5/3, and a stage of
     * -3 will give 3/6. Max value of both the numerator and denominator is 9.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param stage The remainder of user's accuracy minus target's evasion
     * @return A float in the range 0.33f - 3.00f
     * @see doesMoveHit
     */
    hitStatMultiplier(stage) {
        // Base multiplier:     3 / 3
        // For positive stages: 3 + abs(stage) / 3
        // For negative stages: 3 / 3 + abs(stage)
        var res;
        if(stage > 0) {
            res = (3 + stage) / 3
        }
        else {
            res = 3 / (3 - stage)
        }
        console.log("hitStatMultiplier: " + res)
        return res
    }

         /**
     * Returns remainder of user's accuracy minus target's evasion.
     *
     * Return value is used to determine the hit stat multiplier.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return User's accuracy minus target's evasion; max absolute value of 6
     * @see doesMoveHit
     * @see hitStatMultiplier
     */
    getHitStageSum(user, target) {
        
        // User's accuracy minus target's evasion determines stat stage to use for calculation
        console.log("STAT_ACCURACY: " + constantClass.STAT_ACCURACY)
        var hitStageSum = user.statStages[constantClass.STAT_ACCURACY] - target.statStages[constantClass.STAT_EVASION]
        // Ensures value within range (-6..6)
        if(hitStageSum > 6) hitStageSum = 6
        else if (hitStageSum < -6) hitStageSum = -6

        console.log("hitStageSum: " + hitStageSum)
        return hitStageSum
    }


      /**
     * Checks whether move has any effect on the opposing Pokémon.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param target Pokemon on receiving end of move
     * @returns False if move type has no effect against any of target's types; else true
     */
       hasEffect(target) {
        // If target contains type move has no effect against
        
        //For hver type i motstander sin type
        for (var type in target.types) {
            if (target.types.hasOwnProperty(type)) {
                // console.log(key + " -> " + p[key]);

                //Dersom motstanderen 
                // console.log("TYPE_MAP: " + constants.TYPE_MAP.get("fight").weak)
                if (constants.TYPE_MAP[this.type].matchup(type) == 0.0) {   //Sjekk om motstander har en type hvor effekten blir nullifisert, for eks fight kan ikke slå ghost
                    console.log("Move TYPE har this.type")
                    
                    if (this.power > 0 || this.moveEffects) {

                    }
                }
            }
        }

        // if(target.types.any { TYPE_MAP[type.stringRes]!!.matchup(it) == 0.0f }) {
        //     // Some moves are exempt; e.g. Growl (normal type) can hit ghost types
        //     // This condition might need some refinement
        //     if(power > 0 || moveEffects.any{it.statusApplied != StatusCondition.NONE}) {
        //         Log.println(Log.VERBOSE, "MOV", "Move has no effect against opponent")
        //         return false
        //     }
        // }
        return true
    }

        /**
     * Calculates move damage and generates damage related message flags.
     *
     * Performs all operations related to calculating a move's damage. This includes
     * fetching the correct stats from the participating pokemon depending on whether the move
     * is physical or special, applying multipliers from stat stages, and calling all
     * functions related to deciding other damage related multipliers.
     *
     * Note: Upon a critical hit only stat changes that benefit the user apply. Negative attack
     * stages and positive defense stages will be completely ignored.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @param target Pokemon on receiving end of move
     * @return Pair containing the damage dealt as an integer and any resulting message
     * flags as a queue.
     *
     * @see statMultiplier
     * @see type
     * @see crit
     * @see base
     * @see stab
     * @see burn
     * @see random
     */
         calculateMoveDamage(user, target) {
            var textBoxQueue = [];
    
            // Get relevant stat stages
            var userStatStage;

            if (this.isPhysical) {
                userStatStage = user.statStages[constantClass.STAT_ATTACK]
            } else {
                userStatStage = user.statStages[constantClass.STAT_SPECIAL_ATTACK]
            }

            var targetStatStage;

            if(this.isPhysical) {
                targetStatStage = target.statStages[constantClass.STAT_DEFENSE]
            } 
            else {
                targetStatStage = target.statStages[constantClass.STAT_SPECIAL_DEFENSE]
            }
    
            // Check type advantage
            var type = this.typeEffectivenes(target)
            if (type > 1) {
                textBoxQueue.push(constants.TEXT_SUPER_EFFECTIVE)
            } else if (type == 0.5) {
                textBoxQueue.push(constants.TEXT_NOT_EFFECTIVE)
            } 

    
            // Check critical; remove unfavorable stat stages on crit
            var crit = this.crit()
            if(crit == 1.5) {
                userStatStage = Math.max(userStatStage, 0)
                targetStatStage = Math.max(targetStatStage, 0)
                textBoxQueue.push(constants.TEXT_CRITICAL)
            }
    
            // Apply stat stages
            var attackStat;
            if(this.isPhysical) {
                attackStat = user.attack 
            } 
            else {
                attackStat = user.spAttack
            } 
            
            attackStat *= this.statMultiplier(userStatStage)
    
            var defenseStat;
            if(this.isPhysical) {
                defenseStat = target.defense
            } 
            else {
                defenseStat = target.spDefense
            }

            defenseStat *= this.statMultiplier(targetStatStage)
    
            // Calculate damage
            var base = this.baseDamage(user, attackStat, defenseStat)
            var damageMax = (base * type * crit * this.stabDamage(user) * this.burnDamage(user))
            var damage = (damageMax * this.random())
            damage = Math.floor(damage)
            base =  Math.floor(base)
            
            var temp = (damageMax * 0.85);
            damageMax =  Math.floor(damageMax)
            temp =  Math.floor(temp)

            console.log("Possible damage range: " + temp + " - " + damageMax)
            console.log("Damage dealt: " + damage)
            var pair = new Pair(damage, textBoxQueue);
            return pair
        }



            /**
     * Stat multiplier formula for all stats except accuracy and evasion
     *
     * Takes the base fraction 2/2 and adds the absolute value of the given stage to either the
     * numerator or the denominator, depending on whether the value of the stage is negative or
     * positive. E.g. with a given stage og 2 the resulting fraction will be 4/2, and a stage of
     * -3 will give 2/5. Max value of both the numerator and denominator is 6.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param stage The current stat stage of the relevant stat
     * @return An float in the range 0.25f - 4.00f
     * @see calculateMoveDamage
     */
    statMultiplier(stage) {
        console.log("Stat stage: " + stage)

        // return
        if(stage > 0) {
            var temp = stage + 2;
            console.log("-- Stat multiplier: " + temp + "/2")
            return ((2 + stage) / 2)
        }
        else {
            var temp = 2 - stage;
            console.log("-- Stat multiplier: 2/" + temp)
            return (2 / (2 - stage))
        }
    }


        /**
     * Executes all additional move effects for the current move. These come in the form of
     * either applying a specific status effect to the target, or modifying the stat stages
     * of either the user or the target.
     *
     * Effects only trigger if the target is not fainted and it passes a hit roll.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move.
     * @param target Pokemon on receiving end of move.
     * @return A queue of potential message flags created by executed move effects.
     *
     * @see applyStatus
     * @see applyStatStage
     */
         executeMoveEffects(user, target) {
            var messageQueue = [];
            console.log("moveName: " + this.name)
            console.log("moveEffects: " + this.moveEffects)
            console.log("moveEffects length: " + this.moveEffects.length)
            

            // this.moveEffects.forEach(it => {
            //     console.log("it: " + it)
            //     Object.entries(it).forEach(pokemon => {
            //         console.log("attribut i object: " + pokemon)
            //     });
             
            // })

            this.moveEffects.forEach(effect => {
                if(!effect) {
                    console.log("Hopper over denne move: " + this.name + ", sin effekt")
                    return
                    
                }
                console.log("Effect: " + effect)
                console.log("Effect.applychance: " + effect.applyChance)
                // Skips effects targeting fainted pokemon
                if(!target.isFainted()) {
                    // Rolls for whether effect triggers
                    if (this.getRandomArbitrary(0, 100) < effect.applyChance) {
    
                        // Set effect target
                        var effectTarget;
                        if (effect.target == constants.AnimationTarget.FOE){
                            effectTarget = target; 
                            
                        } 
                        else {
                            effectTarget = user;
                        } 
    
                        // Apply status condition
                        if (effect.statusApplied != constants.StatusCondition.NONE) {
                            var response = this.applyStatus(effect, effectTarget)
                            messageQueue.push(response)
                        }
                        
                        // Apply stat stage
                        if (effect.statsModified.length > 0) {
                            var response = this.applyStatStage(effect, effectTarget)
                            response.forEach(message => {
                                messageQueue.push(message)  
                            })
                        }
                    } else console.log("-- Effect missed")
                }
            })
            return messageQueue
        }


        getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }
            /**
     * Applies a status condition to the target pokemon. Fails if target already has a
     * status condition.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param effect The relevant move effect of current move.
     * @param target Pokemon who will receive the effect.
     * @return Returns a message flag based on status applied.
     *
     * @see executeMoveEffects
     */
    applyStatus(effect, target) {
        // TODO: [MEDIUM] Status applied animations, time with application
        var message;

        if(target.applyStatus(effect.statusApplied)) {
            message = target.name

            switch(effect.statusApplied) {
                case constants.StatusCondition.NONE: 
                    message += "nothing happened" 
                    break;
                case constants.StatusCondition.BURN: 
                    message += "was burned"   
                    break;
                
                case constants.StatusCondition.FREEZE: 
                    message += "was frozen solid" 
                    break;
                
                case constants.StatusCondition.PARALYSIS: 
                    message += "was paralyzed"
                    break;

                case constants.StatusCondition.POISON: 
                    message += "was poisoned"
                    break;

                case constants.StatusCondition.BAD_POISON: 
                    message += "was badly poisined"
                    break;

                case constants.StatusCondition.SLEEP: 
                    message += "fell asleep"
                    break;
            }
        }
        else {
            message = "But it failed!"
        }
        return message
    }


       /**
     * Modifies a Pokémon's stat stage values.
     *
     * Applies a positive or negative change to all target's stats listed in the move effect.
     * Maximum modifying value of a single move effect is 3. A pokemon's stat can go no higher
     * than 6 and no lower than -6.
     *
     * @param effect MoveEffect containing list of stats and value to modify by.
     * @param target Pokemon who will receive the effect.
     * @return Queue of MessageFlags, containing textbox messages and animation info.
     *
     * @see executeMoveEffects
     * @see statMultiplier
     */
        applyStatStage(effect, target) {
            var messageQueue = [];
    
            effect.statsModified.forEach (stat => { 
                var message = target.name + "'s " + constants.STAT_TO_STRING[stat];
    
                if(effect.modifiedValue > 0 && target.statStages[stat] == 6)
                    message += "won't go higher!"
                else if(effect.modifiedValue < 0 && target.statStages[stat] == -6)
                    message += "won't go any lower!"
                else {
                    target.modifyStatStage(stat, effect.modifiedValue)
                    switch(effect.modifiedValue) {
                        case 1: 
                            message += "rose!" 
                            break;
                        case 2: 
                            message += "rose sharply!"   
                            break;
                        
                        case 3: 
                            message += "rose drastically!" 
                            break;
                        
                        case -1: 
                            message += "fell!"
                            break;
        
                        case -2: 
                            message += "harshly fell!"
                            break;
        
                        case -3: 
                            message += "severely fell!"
                            break;
                        default:
                            message += "Went to shit!"
                    }
                }
            messageQueue.push(message)
                
            })
            return messageQueue
        }

         /**
     * Calculates the move's base damage.
     *
     * This base damage value is what all other damage multipliers are applied to. Based on move's
     * power and relevant attack and defense stat values.
     *
     * @param user Pokemon using the move
     * @param attack User's relevant attack stat; either Attack or Special Attack
     * @param defense Target's relevant defense stat; either Defense or Special Defense
     * @return Base damage of move
     *
     * @see calculateMoveDamage
     */
    baseDamage(user, attack, defense) {
        var damage = ((((((((2 * user.level) / 5) + 2) * this.power) * attack) / defense) / 50) + 2)
        console.log("Base damage: " + damage)
        return damage
    }

     /**
     * Decides type effectiveness damage multiplier.
     *
     * Returns the product of the move's type effectiveness against both the target's types.
     * Possible type effectiveness values are:
     *
     * Not Very Effective (0.5f)
     *
     * Effective (1.0f)
     *
     * Super Effective (2.0f)
     *
     * Lowest and highest possible values are 0.25f for double NVE and 4.0f for double SE.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param target Pokemon on receiving end of move
     * @return Float in range 0.25 - 4.00
     *
     * @see calculateMoveDamage
     * @see use
     */
      typeEffectivenes(target) {
        var typeMultiplier = constants.TYPE_MAP[this.type].matchup(target.types)
        console.log("Type multiplier: " + typeMultiplier)
        return typeMultiplier
    }

        /**
     * Decides critical hit damage multiplier.
     *
     * Critical hit ratio is a unique property whose effects apply non-linearly. In the current
     * build the highest possible crit ratio is 1.
     *
     * Rolls a random integer 0-23. If this value is lower than the target value associated with
     * the move's crit ratio, move lands a critical hit dealing 1.5 times damage.
     *
     * @return 1.5 on crit, else 1.0
     * @see calculateMoveDamage
     */
         crit() {
            // Crit chance scales non-linearly with move's crit ratio
            // There are other ways to boost crit chance, but we probably won't implement them
            var toCrit;

            switch(this.critRatio) {
                case 0: 
                    toCrit = 1 
                    break;
                case 1: 
                    toCrit = 3
                    break;
                case 2: 
                    toCrit = 12 
                    break;
                case 3: 
                    toCrit = 24
                    break;
                default:
                    toCrit = 1; //should never happen
            }



            var critRoll = this.getRandomArbitrary(0, 24)
            var critMultiplier;

            if(critRoll < toCrit) {
                critMultiplier = 1.5     
            } 
            else {
                critMultiplier = 1.0
            }
    
            console.log("Crit multiplier: " + critMultiplier)
            console.log("-- To crit: " + toCrit)
            console.log("-- Crit roll: " + critRoll)
    
            return critMultiplier
        }

    /**
     * Decides random damage multiplier.
     *
     * @return Float in range 0.85 - 1.00
     * @see calculateMoveDamage
     */
    random() {
        var randomMultiplier = (this.getRandomArbitrary(85, 100) / 100)
        console.log("Random multiplier: " + randomMultiplier)
        return randomMultiplier
    }

       /**
     * Decides STAB damage multiplier.
     *
     * Same Type Attack Bonus. STAB applies if the move is of the same type as one of the user's
     * types, dealing 1.5 times damage.
     *
     * @param user Pokemon using the move
     * @return 1.5 if STAB is active, else 1.0
     *
     * @see calculateMoveDamage
     */
    stabDamage(user) {
        var stab;

        // if(Object.values(user.types).includes(this.type)) {
        if(user.types.includes(this.type)) {
            stab=1.5 
        } 
        else {
            stab=1
        }

        if(stab == 1.5) {
            console.log("STAB active")
        } 
        return stab
    }


           /**
     * Decides type effectiveness damage multiplier.
     *
     * Returns the product of the move's type effectiveness against both the target's types.
     * Possible type effectiveness values are:
     *
     * Not Very Effective (0.5f)
     *
     * Effective (1.0f)
     *
     * Super Effective (2.0f)
     *
     * Lowest and highest possible values are 0.25f for double NVE and 4.0f for double SE.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param target Pokemon on receiving end of move
     * @return Float in range 0.25 - 4.00
     *
     * @see calculateMoveDamage
     * @see use
     */
    typeDmg(target) {
        var typeMultiplier = constants.TYPE_MAP[this.type].matchup(target.types)
        console.log("Type multiplier: " + typeMultiplier)
        return typeMultiplier
    }


        /**
     * Decides burn damage multiplier.
     *
     * Burn halves the damage dealt by the afflicted Pokémon's physical moves.
     *
     * @author Oddbjørn S. Borge-Jensen
     * @param user Pokemon using the move
     * @return 0.5f when burned, else 1.0f
     *
     * @see calculateMoveDamage
     */
    burnDamage(user) {
        var burn; 
        if(user.statusCondition == constants.StatusCondition.BURN && this.isPhysical) {
            burn = 0.5    
        } 
        else {
            burn = 1
        }

        if(burn == 0.5) {
            console.log("Damage halved from burn!")
        }
        return burn
    }

    // *-- GETTER FUNCTIONS --* //
    getName() { return String.prototype.toUpperCase(this.name) }

    
    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }    
}