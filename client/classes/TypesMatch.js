
export class TypeMatchup{

    constructor(immune, weak, strong) {   
        this.immune = immune;
        this.weak = weak;
        this.strong = strong;
    }

    matchup(type) {

        //For hver type i strong
        this.strong.forEach(it => {

            if (type == it) {
                return 2.0;
            }
        
        })

        this.weak.forEach(it => {
     
            if (type == it) {
                return 0.5;
            }
        })
        this.immune.forEach(it => {
           
            if (type == it) {
                return 0.0;
            }
        })

        return 1.0;
    }

    

    // matchupTypes(types): Float {
    //     var multiplier = 1f
    //     types.forEach() {
    //         multiplier *= matchup(it)
    //     }
    //     return multiplier
    // }

}