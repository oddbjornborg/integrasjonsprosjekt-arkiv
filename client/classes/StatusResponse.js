export class StatusResponse{
    constructor(isImmobilized, messageFlag) {
        this.isImmobilized = isImmobilized;
        this.messageFlag = messageFlag;
    }
}
