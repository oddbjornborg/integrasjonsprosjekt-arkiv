import { StatusCondition } from "./constants"
export class MoveEffect {
    constructor(target, applyChance, statusApplied, statsModified, modifiedValue, animFlag) {

        this.target = target;
        this.applyChance = applyChance;
        if(statusApplied) {
            this.statusApplied = statusApplied;
        } else {
            this.statusApplied = StatusCondition.NONE
        }
        this.statsModified = statsModified;
        this.modifiedValue = modifiedValue;
        this.animFlag = animFlag;
    }
}
