//Lit element for register a user
import { LitElement, html, css } from '/client/node_modules/lit-element/lit-element';

/**
 * Her kan en bruker registrer seg.
 */
export class RegisterPage extends LitElement {
    /**
     * web-komponetens css style.
     */
    static styles = css`
        .body {
            display: grid;
            width: 100vw;
            height: 60vh;
            justify-content: center;
            align-items: center;
        }
    `;

    /**
     * html koden til web-komponenten, det visuelle av web-komponenten.
     * @returns html
     */
    render() {
        return html`
        <article class="body">
            <div class="d-flex justify-content-center">
                <form class="form" id="registerUser" style="width: 28rem">
                    <h1>Register User</h1>
                    
                    <div class="row mt-2">
                        <div class="col-12">
                            <label for="username">Username</label>
                            <input type="text" name="username"pattern="[a-z0-9A-Z]{2,63}$" class="form-control" id="username" placeholder="Enter username" required> 
                        </div>
                    </div>
                    
                    <div class="row mt-2">
                        <div class="col">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                        </div>
                        <div class="col">
                            <label for="repetePassword"> </label>
                            <input type="password" name="repeatPassword" class="form-control" id="repetePassword" placeholder="Repeat password">
                        </div>
                    </div>
                
                    <div class="row mt-2">
                        <div class="col-12 d-grid">
                            <button @click="${this.registerUser}" class="btn btn-primary">Register User</button>
                        </div>
                    </div>

                    <div class="row mt-1 ">
                        <a href="/login" class="text-secondary text-center" style="font-size: .8rem;">All ready registerd? login here.</a>
                    </div>
                </form>
            </div>
        </article>
        `;
    }

     /**
     * This function take the formData registerUser and send it backend for creating a new user 
     */
    registerUser(e){
        const newUser = new FormData(e.target.form); //Create a formdata from formen registerUser
        e.preventDefault();
        console.log("event");
        let url = `${window.MyAppGlobals.serverURL}registerUser`;
        fetch(url,{ // fetch til api'et.
            method:'post',
            credentials: "include",
            body:newUser    // sender med form dataen til api'et
        })
        .then(function(response){
            return response.text(); 
        })
        .then(function (text){
            //console.log(text);
            if(text =='ok'){ //If reply message is ok and the user is registered
                console.log("registrert");
                alert("User Registered! You will be redirected to home");
                location.replace("http://localhost:8080/"); //redirect to homepage
            } 
            else if(text=='UsernameExist'){ //If the username exist 
                alert("Username exist");
            }
            else if(text =='UserNameCharNot'){ //if the username caracther is not valid
                alert("Username characther not valid, can only contain 0-9,A-Z,a-z");
            }
            else if(text =='missMatch'){ 
                //alert("Feil i brukernavn eller passord under registrering");
                alert("input value not vallid username, password missmatch ");
            }
            else if(text =='pwToChort'){ //If the password is to short
                alert("Pw to short, must be atleast 8 characther long!");
            }
        })
        .catch(function (error){
            console.log(error);
        })

    }
}
customElements.define('register-page', RegisterPage);