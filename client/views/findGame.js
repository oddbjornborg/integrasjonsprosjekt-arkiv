import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
// import { io } from "../node_modules/socket.io-client";

/**
 *  Web komponentet som lager og finner spill så sender brukeren til spillet.
 */
export class findGame extends LitElement {

    /**
     * Nettsiedens variabler
     * @returns object of variables
     */
    static get properties() {
        return {
            showMenu: { type: Boolean},
            showLobby: { type: Boolean},
            socket: {},
            roomName: { type: String},
        }
    }

    /**
     * klassens konstruktor
     */
    constructor() {
        super();

        this.showLobby = true;
        this.showMenu = true;
        
    }

    /**
     * css style sheet, stylingen til html koden i render().
     */
    static styles = css`
    .body {
        display: grid;
        justify-items: center;
        align-items: center;
        width: 100vw;
        height: 70vh;
    }

    .segment {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        width: 100%;
        height: 200px;
        grid-gap: 10px;
    }

    .img { 
        grid-area: 1 / 1 / 3 / 2;
        display: grid;
        align-items: center;
        justify-items: flex-end;
    }
    .img img {
        height: 200px;
        width: auto;
    }

    .header {
        grid-area: 1 / 2 / 2 / 3;
        border-bottom: solid 3px black;
        display: grid;
        align-items: flex-end;
        padding: 0 10px;
    }
    .header h1 { font-family: 'Press Start 2P', sans-serif; }

    .button { padding: 0 10px; }
    .input-group button { font-family: 'Press Start 2P', sans-serif; }

    input:focus { box-shadow: none !important; border: 1px solid lightgray !important;} 

    .back-btn {
        position: relative;
        bottom: -20px;
        left: -20%;
        width: 120px; 
        height: 50px;
    }

    .msg {
        width: 400px;
        border: 1px solid black;
        background: rgba(255,255,255,.95);
        position: absolute;
        top: 25%;
        left: 50%;
        transform: translateX(-50%);
        z-index: 100;
        display: grid;
        align-items: center;
        justify-content: center;
        border-radius: 10px;
    }

    .msg .btn-close {
        position: absolute;
        top: 10px;
        right: 15px;
    }

    #msg-window { display: none; }
    #msg-loader { display: none; }
    #msg-create { display: none; }
    `;

    /**
     * html koden, nettsidens design.
     * @returns html
     */
    render() {
        return html`
         ${this.showMenu ? html`<link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&family=Roboto+Slab:wght@400&display=swap" rel="stylesheet">
        
        <nav-element></nav-element>
        <article class="body">
            <article class="container mt-5" style="width: 600px;">
                <article class="row">
                    <article class="col-12 p-3">
                        <article class="segment">
                            <div class="img"><img src="images/snorlaxMenu.png" alt="host"></div>
                            <div class="header"><h1 class="display-5">Host</h1></div>
                            <div class="button"><button class="btn btn-outline-dark fs-5" @click=${this.createGameSetup}>Create Game</button></div>
                        </article>
                    </article>
                </article>

                <article class="row">
                    <article class="col-12 p-3">
                        <article class="segment">
                            <div class="img"><img src="images/pikachuMenu.png" alt="host"></div>
                            <div class="header"><h1 class="display-5">Public</h1></div>
                            <div class="button"><button class="btn btn-outline-dark fs-5" role="button" @click=${this.findGame}>Find Game</button></div>
                        </article>
                    </article>
                </article>
                
                <article class="row">
                    <article class="col-12 p-3">
                        <article class="segment">
                            <div class="img"><img src="images/eevee.png" alt="host"></div>
                            <div class="header"><h1 class="display-5">Private</h1></div>
                            <div class="button">
                                <div class="input-group" style="width: 200px">
                                    <input type="text" class="form-control fs-5" placeholder="Game-ID" id="gameId">
                                    <button class="btn btn-outline-dark" @click=${this.joinPrivateGame}> > </button>
                                </div>
                            </div>
                        </article>
                    </article>
                </article>

                <article class="msg" id="msg-window">
                    <div class="text-center mt-5 mb-5" id="msg-loader">
                        <div class="spinner-border text-dark m-2" role="status" style="height: 5rem; width: 5rem;"></div>
                        <p class="h3" id="msg"></p> 
                    </div>

                    <div class="mb-3 mt-4" id="msg-create">
                        <h3 class="text-secondary"> Create Game </h3>
                        <div class="mb-3">
                            <lable class="form-label"> Game Name </lable>
                            <input type="text" class="form-control" id="msg-name">
                        </div>

                        <div class="mb-3">
                            <div>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" role="switch" name="private" id="msg-private">
                                    <label class="form-check-label" for="private">Private</label>
                                </div>
                            </div>
                            <p class="text-muted" style="font-size: 10px; width: 200px"> 
                                All games are public by default, check the switch to make your game private. 
                            </p>
                        </div>
                        
                        <div class="mb-3">
                            <button type="submit" class="btn btn-outline-primary" style="width: 100%" @click=${this.createGame}>Create</button>
                        </div>
                    </div>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click=${this.abort}></button>
                </article>   
            </article>            
        </article>` : html `<game-lobby .hostActivePokemon=${this.myActivePokemon} .playerActivePokemon=${this.opponentActivePokemon} .roomName=${this.roomName} .socket=${this.socket} .showLobby=${this.showLobby}></game-lobby>`}
        `;
    }

/**
 * åpner create game winduet.
 */
createGameSetup() {
    this.shadowRoot.getElementById("msg-window").style.display = "grid";
    this.shadowRoot.getElementById("msg-create").style.display = "inline";
}

/**
 * Sender en fetch til api'et og lager ett spill.
 */
createGame() {
    console.log("-- create game");

    // henter brukerens valgte verdier.
    let gameName = this.shadowRoot.getElementById("msg-name").value;
    let gamePrivate = this.shadowRoot.getElementById("msg-private").checked;
    
    if(gameName == "") gameName = "my-game"     // Setter default lobby navn hvis form er tom

    if(gameName != "") {
        // når burkeren får opprettet en socket tilkoblig.
        this.gameSocket().then(socket => {
            // fetch til api'et
            fetch(`${window.MyAppGlobals.serverURL}createGame`, {
                method: 'POST',
                credentials: 'include',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type':  'application/json'
                },
                // informasjonene som sendes til api'et.
                body: JSON.stringify({
                    'lobby_name': gameName, 
                    'is_private' : gamePrivate, 
                    'host_socket': socket.id 
                })
            }).then(() => { // når brukeren får svar fra api'et
                this.socket = socket;
                this.roomName = "room" + socket.id;
                let parameters = {"room" : this.roomName, "user" : this.socket.id}
                socket.emit("createRoom", (parameters));  // sender en socket melding.
                
                // når denne socket tilkoblingen mottar eventet
                this.socket.on('match-begin', (msg) => {
                    console.log("match-begin battleground")
                    this.showLobby = false;
                });

                this.showMenu = false;

                console.log("Creategame socket: " + socket.id + ", this.socket.id: " + this.socket.id )
            
                        
            }).catch(err => { console.log(err); }); // evt feilmeldinger
        }).catch((err) => { console.log(err); });
    } else { alert("You need to choose a Game Name."); }
}

/**
 * finner et spill og lar brukeren bli med i det spillet.
 */
findGame() {
    console.log("-- find game");
    console.log("-- find game signedCookie" + document.cookie);

    // åpner en "loading screen" så brukeren ser at noe skjer.
    this.shadowRoot.getElementById("msg").innerHTML = "Searching for game.";
    this.shadowRoot.getElementById("msg-window").style.display = "grid";
    this.shadowRoot.getElementById("msg-loader").style.display = "inline";
    
    // sender en fetch til api'et.
    fetch(`${window.MyAppGlobals.serverURL}findGame`, {
        method: 'GET',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        }
    // når man får svar fra api'et
    }).then(response => { return response.json() }).then(data => {
        if (data.msg) {         // if det ikke er noe i playerlobbies
            alert(data.msg);    // alert feilmelidingen
        } else {
            // når bruker har fått tildelt en socket tilkobling
            this.gameSocket().then(socket => {
            let parameters = {"room" : data.socket_id, "user" : data.user}
            this.roomName = data.socket_id;

            // sender et joingame event
            socket.emit("joinGame", (parameters));

            // når eventet intreffer
            socket.once('match-begin', (msg) => {
                console.log("match-begin battleground")
                this.showLobby = false;
            });
           
            this.socket = socket;
            this.showMenu = false;
            });
        }             
    });
}

/**
 * Skal slå opp en spesifik id/kode og om det finnes et ledig spill med id'en så
 * joiner brukeren det spillet.
 */
joinPrivateGame() {
    console.log("-- join private game");

    // åpner samme loaing vindu som i findgame().
    this.shadowRoot.getElementById("msg").innerHTML = "Searching for game.";
    this.shadowRoot.getElementById("msg-window").style.display = "grid";
    this.shadowRoot.getElementById("msg-loader").style.display = "inline";

    // henter spill id/kode
    let gameId = this.shadowRoot.getElementById("gameId").value;
    if (gameId != "") {
        console.log("-- fetching game width id: " + gameId);
        // sender fetch til api'et
        fetch(`${window.MyAppGlobals.serverURL}findGame`, {
            method: 'POST',
            credentials: 'include',
            headers: { 
                'Accept': 'application/json',
                'Content-Type':  'application/json'
            },
            // inneholder spill koden.
            body: JSON.stringify(gameId)
        // per nå er ikke funkjonen helt ferdig og ingenting skjer om brukeren fant et spill.
        }).then(response => { return response.json() }).then(data => {
            console.log(data);
        }).catch(err => { console.log(err) });
    }
    else {
        this.abort();
        alert("No game id");
    }
}

/**
 * lukker evt meldings vinduer.
 */
abort() {
    this.shadowRoot.getElementById("msg-window").style.display = "none";
    this.shadowRoot.getElementById("msg-loader").style.display = "none";
    this.shadowRoot.getElementById("msg-create").style.display = "none";
}

/**
 * Skaper en socket tilkoblig 
 * @returns promise, viktig for å kunne bruke .then() slik at kode ikke blir eksekvert før socket tilkoblingen er etablert.
 */
gameSocket() {
    return new Promise((resolve, reject) => {
        try {
            // sender en forspørsel til api'et ved port 3000.
            const socket = io("http://localhost:3000");
            socket.once('connect', () => {  // når socket koblingen blir skapt.
                // console.log(`-- connected to id: ${socket.id}`);
                window.MyAppGlobals.socket = socket; // oppdaterer global variabel
                resolve(socket); // avslutter promise med socket variablen
            });
            socket.once("connect_error", (err) => { // hvis error
                reject(err.message);
            });
        // hvis error
        } catch (err) {
            reject(err); // aviser promiset med erroren
        }
    });
}

/**
 * Skaper et random tall.
 * @param {int} min - minimum verdi
 * @param {int} max - maximum verdi
 * @returns tall innenfor intervallet [min, max]
 */
randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}   

}
customElements.define('findgame-element', findGame);