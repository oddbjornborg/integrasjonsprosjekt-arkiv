import { LitElement, html, css } from 'lit-element';
import * as Pokemonsystem from '../classes/pokemons'
//lag klasse pokemon
import * as UserClass from '../classes/User.js'
import * as pokemonClass from '../classes/pokemons.js'
import { Move } from '../classes/Move';

    /*****************************************************************
     * Website to display which users are connected to the socket room
     * @author Nicholas Sellevåg
     *****************************************************************/
export class gameLobby extends LitElement {

    /************************************************  
     * Getter for properties in gameLobby litElement
     * @author Nicholas Sellevåg
     ************************************************/
    static get properties() {
        return {
            me: { type: Object},
            opponent: { type: Object},

            myTeam: { type: Array },
            opponentTeam: { type: Array },

            myRatio: { type: Number },
            opponentRatio: { type: Number },
            
            socket: {},

            hostOrNot: { type: Boolean},
            usertest: { type: Boolean},
            hosttest: { type: Boolean},

            roomName: { type: String },
            showGame: { type: Boolean},
            showLobby: { type: Boolean},
            playerId: {},
            opponentId: {},
            opponentActivePokemon: {},
            myActivePokemon: {},

            myself: {},     //@see user class, represents this player as an user
            enemy: {},   //@see user class, represents the oponent as an user
            retrieveHostDone: { type: Boolean },
            retrieveOpponentDone: { type: Boolean }
        }
    }
    
    /****************************************************************
     * When instance of gameLobby is created, define these attributes
     * @author Nicholas Sellevåg
     ****************************************************************/
    constructor() {
        super();
        this.showLobby = false; //Boolean used to indicate if gameLobby html or battleGround html should be rendered
        
        this.opponentActivePokemon; //numberID of opponents active pokemon
        this.myActivePokemon;       //numberID of opponents active pokemon

        this.hostOrNot = false;     //boolean used to indicate which side of gamelobby host and user should be displayed
        // this.usertest = true;
        // this.hosttest = true;
        this.me = {};   //current users User instance @see User.js
        this.opponent = {}; //opponent's User instance @see User.js

        this.myTeam = [];   //listing of current User's team as json defined objects
        this.opponentTeam = []; //listing of current User's team as json defined objects

        this.myRatio = 0;   //win/loss ratio of player
        this.opponentRatio = 0; //win/loss ratio of player
        
        this.retrieveHostDone = false;  //indicate when current player information has been retrieved from database
        this.retrieveOpponentDone = false; //indicate when opponent player information has been retrieved from database
        
    }

    static styles = css`
        * {
        margin: 0;
        padding: 0;
    }

    main {
        background-color: aliceblue;
        display: grid;
        position: absolute;
        width: 100vw;
        height: 70vh;
        place-items: center;
    }
    .container {
        display: grid;
        background-color: white;
        min-height: 100%;
        min-width: 80%;
    }

    .row {
        overflow: hidden;
    }


    body {
        background-color: black;
        display: grid;
        justify-items: center;
        align-items: center;
        width: 100;
        height: 100vh;
    }

    /* kan feste på pokemon for highligting */
    .glowing-border {
        border: 2px solid #dadada;
        border-radius: 7px;
    }
    .glowing-border:focus { 
        outline: none;
        border-color: #9ecaed;
        box-shadow: 0 0 10px #9ecaed;
    }

    #host {
        display: grid;
        vertical-align: baseline;  
    }

    #red {
        background-color: #D74A28;   
    }
    #blue {
        background-color: #28B5D7;   
    }
    
    #background {
        background-image: url("./images/vs.png");
        background-position: center; 
        background-repeat: no-repeat;
        background-size: 90%; 
        
    }

    #dittNavn {
        position: absolute;
        left: 37%;
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        font-size: 40px;
        top: 47%;
    }
    #motstanderNavn {
        position: absolute;
        right: 37%;
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        font-size: 40px;
        top: 47%;
    }
    #knapp {
        margin-bottom: 10%;
    }

    #tekst {
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        font-size: 20px;
    }
    
    `;



    render() {
        return html`
        ${this.showLobby ? html`<article class="container">
                <!-- Stack the columns on mobile by making one full-width and the other half-width -->
            <div class="row">
            ${this.opponentTeam.map((pokemon) => html`
                <div class="col" id="red">
                    <p id="tekst">${pokemon.name}  lv : ${pokemon.level}</p>
                    <img src="images/${pokemon.name}.png" class="img-fluid">
                </div>
                    `)}
            </div>

            <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
            <div class="row">
                
                <div class="col-6 col-md-4 align-items-end" id="host">
                    <div class="row h-50" ><img src="images/charizard.png" class="img-fluid" id="blue"> </div>
                </div>
                
                <div id="background" class="col-6 col-md-4 d-grid align-items-end justify-content-center align-content-end">
                    <button type="button" id="knapp" @click=${this.readyUpp} class="btn-lg btn-outline-primary btn-lg img-fluid" >ready upp</button>
                    <p id="dittNavn">${this.me.name}</p>
                    <p id="motstanderNavn">${this.opponent.name}</p>
                </div>

                   
                  
            
                ${this.opponentTeam[1] ? html`                
                <div class="col-6 col-md-4 align-items-start">
                    <div class="row h-50" id="player"><img src="images/charizard.png" class="img-fluid" id="red"> </div>
                </div> ` : html ``}

            </div>
         
            
            <!-- Columns are always 50% wide, on mobile and desktop -->
            <div class="row">

            ${this.myTeam.map((pokemon) => html`
                <div class="col" id="blue">
                    <p id="tekst">${pokemon.name}  lv : ${pokemon.level}</p>
                    <img src="images/${pokemon.name}.png" class="img-fluid">
                </div>
                    `)}

                    
            </div>
        </article>
        `: html `<battle-element .myActivePokemon=${this.myActivePokemon} .opponentActivePokemon=${this.opponentActivePokemon} .myself=${this.myself} .enemy=${this.enemy} .playerId=${this.playerId} .opponentId=${this.opponentId} .roomName=${this.roomName} .socket=${this.socket} .isHost=${this.hostOrNot} ></battle-element>`};
    `}  //.myTeam=${this.myTeam} .opponentTeam=${this.opponentTeam}


/************************************************************
 * Triggered when both players have clicked on "ready" button
 * @author Nicholas Sellevåg
 ***********************************************************/
readyUpp() {
    //let parameters = {"room" : this.roomName, "user" : this.socketId, "hostOrNot": this.hostOrNot}
    
    this.retrieveMyself()
    this.retrieveOpponent()
}

/**************************************************************************
 * When gameLobby is created, validate if current user is the host og lobby
 * @author Nicholas Sellevåg
 *************************************************************************/
firstUpdated() {    
  
        this.validateHost()
} 

/************************************************  
 * Fetch information about current user from API
 * @author Nicholas Sellevåg
 ************************************************/
retrieveUserInfo() {
    console.log("-- fetching host information.");

    //Retrieve information about user
    let url = `${window.MyAppGlobals.serverURL}user`;
    
    fetch(url, {
        method: 'GET',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        }
    }).then(response => { return response.json() }).then(data => {
        console.log(data);

        //Update user based on data from API
        this.me.name = data.username;
        this.me.wins = data.wins;
        this.me.loss = data.losses;
        this.playerId = data.id;

        if (data.losses == 0) { this.myRatio = data.wins; } 
        else { this.myRatio = (data.wins / data.losses).toFixed(2); }

        //Fill user's pokemon team
        Object.entries(data.team).forEach((pokemon) => {
            this.myTeam.push(pokemon[1]);
        });
    }).catch(err => { console.log(err) });
}

/************************************************  
 * Validate if current user is host from API
 * @author Nicholas Sellevåg
 ************************************************/
validateHost() {
    var socketId = this.socket.id;
    //Fetch for API to valide if current user is the creator of lobby or not 
    console.log("-- validating host, socketId: " + socketId);
    let url = `${window.MyAppGlobals.serverURL}validateHost`;
    fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ 
            'socket':  socketId,
        })
    }).then(response => { return response.json() }).then(data => {
        console.log(data);
        //If current user is the host
        if (data.status == "host") {
            console.log("Jeg er host")
            this.hostOrNot = true;
            //Wait for opponent to join game and ready up
            this.socket.once('match-begin', (msg) => {
                console.log("match-begin battleground")
                //render gameLobby.js html
                this.myActivePokemon = this.pokemonFirst(msg.host_active_pokemon)
                this.opponentActivePokemon = this.pokemonFirst(msg.player_active_pokemon)
                this.showLobby = false;
            });

            this.retrieveUserInfo();
            this.socket.once('player-joined', (data) => {
                // console.log("Player joined event!!!")
                
                this.fetchPlayerInfo();
                this.socket.emit('player-fetch');
            }) 
        } 
        //if current user is not the host
        else if (data.status == "user") {
            console.log("Jeg er user")
            this.hostOrNot == false;
            //Emit that this player is ready to begin the match
            this.socket.once('match-begin', (msg) => {
                console.log("match-begin battleground")
                //render gameLobby.js html
                this.myActivePokemon = this.pokemonFirst(msg.player_active_pokemon)
                this.opponentActivePokemon = this.pokemonFirst(msg.host_active_pokemon)   //ved create game er du alltid host

                this.showLobby = false;
            });
                this.retrieveUserInfo();
                this.fetchHostInfo(); 
        } else {
            // this.hostOrNot == false;
            console.log("validateHost in gameLobby did not work as intended");
            return false;
        }
    }).catch(err => { console.log(err) });
}

/************************************************  
 * Retrieve enum value of which pokemon is active
 * @author Nicholas Sellevåg
 ************************************************/
pokemonFirst(statement) {
    var numb;
    switch(statement) {
        case "pokemon_first":
        numb = 0;
        break;
        
        case "pokemon_second": 
        numb = 1;
        break;
        
        case "pokemon_third": 
        numb = 2;
        break;
        
        case "pokemon_fourth": 
        numb = 3;
        break;
        
        case "pokemon_fifth": 
        numb = 4;
        break;
        
        case "pokemon_sixth": 
        numb = 5;
        break;     
    }
    // console.log("Numb er: " + numb)
    return numb    
}

/************************************************  
 * Fetch information about the joined player
 * @author Nicholas Sellevåg
 ************************************************/
fetchPlayerInfo() {
    var socketId = this.socket.id;
    //Fetch information for player who joined the lobby
    console.log("-- fetching player information, socketId: " + socketId);
    let url = `${window.MyAppGlobals.serverURL}getPlayerInfo`;
    fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ 
            'socket':  socketId,
        })
    }).then(response => { return response.json() }).then(data => {
        console.log(data);
        //Add user information to User from API responce
        this.opponent.name = data.username;
        this.opponent.wins = data.wins;
        this.opponent.loss = data.losses;
        this.opponentId = data.id;
  
        if (data.losses == 0) { this.opponentRatio = data.wins; } 
        else { this.opponentRatio = (data.wins / data.losses).toFixed(2); }
        //Fill User's pokemon team
        Object.entries(data.team).forEach((pokemon) => {
            this.opponentTeam.push(pokemon[1]);
        });
    }).catch(err => { console.log(err) });
}

/************************************************* 
 * Fetch information about the gameLobby's creator
 * @author Nicholas Sellevåg
 ************************************************/
fetchHostInfo() {
    var roomName = this.roomName;
    console.log("-- fetching host information, roomName: " + roomName);
    //Fetch information for player who created the lobby
    let url = `${window.MyAppGlobals.serverURL}getHostInfo`;
    fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ 
            'room':  roomName,
        })
    }).then(response => { return response.json() }).then(data => {
        console.log(data);

        //Add user information to User from API responce
        this.opponent.name = data.username;
        this.opponent.wins = data.wins;
        this.opponent.loss = data.losses;
        this.opponentId = data.id;
     

        if (data.losses == 0) { this.opponentRatio = data.wins; } 
        else { this.opponentRatio = (data.wins / data.losses).toFixed(2); }

        //Fill User's pokemon team
        Object.entries(data.team).forEach((pokemon) => {
            this.opponentTeam.push(pokemon[1]);
        });
    }).catch(err => { console.log(err) });
}



/***************************  
 * Retrieve user data
 * @author Nicholas Sellevåg
 **************************/
retrieveMyself() {
    console.log("RetrievePokemon, user: " + this.playerId)
    var user = this.playerId;
    
    //Fetch user data from API
    let url = `${window.MyAppGlobals.serverURL}retrieveUser`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "user" : user})
    }).then(response => { return response.json()

        }).then(data => {
            //Create new User based on data from API
            this.myself = new UserClass.User(data);
            this.myself.print()
            
        }).then( data => {
            //Retrieve new User's pokemonn team
            this.retrievePokemonHost()
        }).catch(err => { console.log(err) });
    
}

/********************************  
 * Retrieve pokemon team for host
 * @author Nicholas Sellevåg
 *******************************/
retrievePokemonHost() {
    //Fetch pokemons for host
    let url = `${window.MyAppGlobals.serverURL}retrievePokemonTeam`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "team" : this.myself.teamIds})
    }).then(response => { return response.json() }).then(data => {
        data.forEach(pokemon => {
            this.myself.team.push(new pokemonClass.Pokemon(pokemon));
            // for (var key in pokemon) {
                //     if (pokemon.hasOwnProperty(key)) {
                    //         console.log(key + " -> " + pokemon[key]);
                    //     }
                    // }
                });
        }).then(data => {
        var moveIds = [];
            
        //for each pokemon in team
        this.myself.team.forEach(pokemon => {
            //For each move in pokemon
            pokemon.moveIds.forEach(moveId => {
                //If pokemon should have the move
                if(moveIds.includes(moveId)) {
                    // console.log("Har move id: " + moveId + ", fra før") 
                } else {
                    //If pokemon misses the move, add it
                    moveIds.push(moveId)
                }
            });
        })


        
        this.retrieveMovesHost(moveIds);
    })
}

/************************************************* 
 * Retrieve unique values from a Json object
 * @author Nicholas Sellevåg
 ************************************************/
onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

/************************************************* 
 * Retrieve moves to fill into pokemon
 * @author Nicholas Sellevåg
 ************************************************/
retrieveMovesHost(moveIds) {

    //retrieve pokemons movelists
    let url = `${window.MyAppGlobals.serverURL}retrieveMovesWithEffects`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "movelist" : moveIds})
        }).then(response => { return response.json() }).then(data => {

                //for each pokemon
            this.myself.team.forEach(pokemon => {
                //for each pokemons moveId
                pokemon.moveIds.forEach(moveId => {
                    //retrive data to create move from
                    data.forEach(move => {
                        if (moveId == move.id) {
                            // console.log("moveId: " + moveId + ", move.id: " + move.id)
                            // console.log("moveList.length: " + pokemon.moveList.length)

                            var harAddaMove = false;
                            //if the moveList actually contains data
                            if(pokemon.moveList.length > 0 ) {
                                //Go through all moves in moveList
                                pokemon.moveList.forEach(pokemonMove => {
                                    //update name of move in pokemon
                                    if(pokemonMove.name == move.name) {
                                        harAddaMove= true;
                                        //Move has been added
                                    }   
                                })
                                //if move was not priorly declared
                                if (harAddaMove == true) {
                                    // console.log("Pokemon: " + pokemon.name + ", opprettet move: " + move.name)
                                    //add the move's effect
                                    pokemon.moveList[pokemon.moveList.length-1].addMoveEffect(move)     //NB denne funker bare dersom movesa kommer i sortert rekkefølge baser på ID'n, it works :)
                                } else {
                                    //create new move if it is missing in pokemon
                                    pokemon.forceAddMove(new Move(move));
                                }
                            }  
                            else {
                                // console.log("Pokemon: " + pokemon.name + ", opprettet move: " + move.name)
                                //create new move if it is missing in pokemon
                                pokemon.forceAddMove(new Move(move));
                                harAddaMove = true;

                            }
                        } 

                        })
                    });
                });
                // this.myself.team.forEach(pokemon => {
                //     pokemon.moveList.forEach(move => {
                //         console.log("Host Pokemon: " + pokemon.name + ", har move: " + move.name)
                //     })
                // })
    })

    //Jeg er ferdig med mine fetches
    this.retrieveHostDone = true;
    this.imReady()
}





// Retrieve Opponents user, pokemon and moves
retrieveOpponent() {
    console.log("RetrievePokemon, user: " + this.opponentId)
    var user = this.opponentId;
    
    //fetch user data
    let url = `${window.MyAppGlobals.serverURL}retrieveUser`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "user" : user})
    }).then(response => { return response.json()

        }).then(data => {

            //Create new User from fetched data
            this.enemy = new UserClass.User(data);
            this.enemy.print()
            
        }).then( data => {
            //Fill User's pokemon team
            this.retrievePokemonOpponent()
        }).catch(err => { console.log(err) });
    
}

/****************************
 * Retrieve opponents pokemon
 * @author Nicholas Sellevåg
 ****************************/
retrievePokemonOpponent() {
    //Fetch api for pokemons
    let url = `${window.MyAppGlobals.serverURL}retrievePokemonTeam`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "team" : this.enemy.teamIds})
    }).then(response => { return response.json() }).then(data => {
        data.forEach(pokemon => {
            this.enemy.team.push(new pokemonClass.Pokemon(pokemon));
            // for (var key in pokemon) {
                //     if (pokemon.hasOwnProperty(key)) {
                    //         console.log(key + " -> " + pokemon[key]);
                    //     }
                    // }
                });
        }).then(data => {
            //For each pokemon in response, push their moves into the pokemon
        var moveIds = [];
            
        this.enemy.team.forEach(pokemon => {
            pokemon.moveIds.forEach(moveId => {
                if(moveIds.includes(moveId)) {
                    // console.log("Har move id: " + moveId + ", fra før") 
                } else {
                    moveIds.push(moveId)
                }
            });
        })


        
        this.retrieveMovesOpponent(moveIds);
    })
}

/************************************************  
 * Retrieve moves for opponent
 * @author Nicholas Sellevåg
 ************************************************/
retrieveMovesOpponent(moveIds) {
    let url = `${window.MyAppGlobals.serverURL}retrieveMovesWithEffects`;
    fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: { 
            'Accept': 'application/json',
            'Content-Type':  'application/json'
        },
        body: JSON.stringify({ "movelist" : moveIds})
        }).then(response => { return response.json() }).then(data => {

         
                //for hver pokemon denne brukeren har
            this.enemy.team.forEach(pokemon => {
                //for hver move en pokemon har
                pokemon.moveIds.forEach(moveId => {
                    //for hver move hentet
                    data.forEach(move => {

                        if (moveId == move.id) {
                            if(pokemon.moveList.length > 0 ) {

                                let moveAlreadyExists = false
                                pokemon.moveList.forEach(pokemonMove => {
                                    if(pokemonMove.name == move.name) {
                                        moveAlreadyExists= true;
                                    }   
                                })
                                if (moveAlreadyExists == true) {
                                    console.log("Pokemon: " + pokemon.name + ", adda move effecten til: " + move.name)
                                    pokemon.moveList[pokemon.moveList.length-1].addMoveEffect(move)     //NB denne funker bare dersom movesa kommer i sortert rekkefølge baser på ID'n, it works :)
                                } else {
                                    pokemon.forceAddMove(new Move(move));
                                }
                            }  
                            else {
                                pokemon.forceAddMove(new Move(move));
                            }
                        }   
                    })
                });
            });
                    // this.enemy.team.forEach(pokemon => {
                    //     pokemon.moveList.forEach(move => {
                    //         console.log("Enemy Pokemon: " + pokemon.name + ", har move: " + move.name)
                    //     })
                    // })
    })
   
    //Jeg er ferdig med mine fetches
    this.retrieveOpponentDone = true;
    this.imReady()
}

/******************************************************************************
 * When clicking on "ready" button emit this player's ready status
 * When both players are ready the game begine and display battleGround.js html
 * @author Nicholas Sellevåg
 *****************************************************************************/
imReady() {
    if (this.retrieveHostDone && this.retrieveOpponentDone) {
        let parameters = {"isHost": this.hostOrNot}
        this.socket.emit('im-ready', parameters)
    }
}


   
}
customElements.define('game-lobby', gameLobby);