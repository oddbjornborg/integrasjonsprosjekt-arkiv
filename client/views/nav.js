import { LitElement, html } from 'lit-element';

/**
 * lager en navigasjons-bar hvor brukeren enkelt kan navigere til andre web-komponenter
 * uten å gå via meny siden.
 */
export class NavElement extends LitElement {
    /**
     * html koden som utgjør web-komponeneten, alt av css style er basert på bootstrap.
     * @returns html
     */
    render() {
        return html`
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navbar">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                Pokémon
                </a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/play">Play</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/leaderboard">Leaderboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/pokemons">Pokémons</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/userpage">Userpage</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        `;
    }
}
customElements.define('nav-element', NavElement);