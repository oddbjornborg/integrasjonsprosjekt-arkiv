import { LitElement, html, css } from '/client/node_modules/lit-element/lit-element';

/**
 * inn loggings web komponentet, tar imot bruker legitimisering og sender det til databasen.
 */
export class login extends LitElement {
    /**
     * klassens konstruktor
     */
    constructor() {
        super();
    }

    /**
     * css stylen for html koden i render()
     */
    static styles = css`
    .body {
        display: grid;
        width: 100vw;
        height: 60vh;
        justify-content: center;
        align-items: center;
    }
    `;
    
    /**
     * html koden som utgjør web komponentets design
     * @returns html
     */
    render() {
        return html`
        <article class="body">
            <div class="d-flex justify-content-center">
                <form class="form" id="form" enctype="multipart/form-data" action="#" style="width: 28rem">
                    <h1>Login</h1>
                    
                    <div class="row mt-2">
                        <div class="col-12">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control" id="username" placeholder="Enter username" required> 
                        </div>
                    </div>
                    
                    <div class="row mt-2">
                        <div class="col-12">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                        </div>
                    </div>
                
                    <div class="row mt-2">
                        <div class="col-12 d-grid">
                            <button @click="${this.login}" class="btn btn-primary">Log in</button>
                        </div>
                    </div>

                    <div class="row mt-1 ">
                        <a href="/register" class="text-secondary text-center" style="font-size: .8rem;">Not registerd? register here.</a>
                    </div>
                </form>
            </div>
        </article>
        `;
    }

    /**
     * skal sende brukerens oppgitte legitimisering til api'et for sammenlikning.
     * @param e  
     */
    login(e) {
        e.preventDefault()
        let url = `${window.MyAppGlobals.serverURL}login`;
        // skaper formdata ut av form-elemetet e er inni.
        const formData = new FormData(e.target.form);
        
        // konverterer formdata til en psudo json objekt.
        let jsonData = {};
        formData.forEach((value, key) => {
            jsonData[key] = value;
        });

        // sender en fetch til api'et
        fetch(url,{
            method:'post',
            credentials: 'include',
            headers: { 
                'Accept': 'application/json',
                'Content-Type':  'application/json'
            },
            // fetchen sender psudo json objektet
            body: JSON.stringify(jsonData)
        }).then(res => {    // når klienten får svar fra api'et
            if (res.status == 200) {    // hvis alt gikk bra
                setTimeout(() => { window.location.href = "/"; }, 300);
            } else if (res.status == 400) { // hvis brukeren allerde er logget inn
                window.location.href = "/";
            } else {
                res.text().then(err => {    // ved en feil.
                    alert(err);
                    console.log(err);
                });
            }
        });
    }
}
customElements.define('login-page', login);