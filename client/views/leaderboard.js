import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import './nav.js';

/**
 * leaderboard web-komponentet, viser alle brukernes score og rangskjerer dem
 * fra høyest til lavet score.
 */
export class Leaderboard extends LitElement {
    /**
     * klassens variabler
     */
    static get properties() {
        return {
            users: { type: Array },
            i: { type: Number }
        }
    }

    /**
     * klassens konstruktor
     */
    constructor() {
        super();
        this.users = [];
        this.i = 0;
    }

    /**
     * css styleingen til html koden i render().
     */
    static styles = css`
        * {
            margin: 0;
            padding: 0;
        }

        main {
            display: grid;
            position: absolute;
            width: 100vw;
            height: 60vh;
            place-items: center;
        }
    `;

    /**
     * Nettsidens design / html kode.
     * @returns html
     */
    render() {
        return html`
        <nav-element></nav-element>
        <main>
            <article class="container" style="width: 50rem;">
                <h1 class="display-3">Leaderboard</h1>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">#</th>
                            <th scope="col">User</th>
                            <th scope="col" class="text-center">Ratio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- for hver bruker kjøres koden inni .map -->
                        ${this.users.map(user => html`
                            <tr>
                                <th scope="row" class="text-center">${++this.i}</th>
                                <td>${user.username}</td>
                                <td class="text-center">${user.ratio}</td>
                            </tr>
                        `)}
                    </tbody>
                </table>    
            </article>
        </main>
        `;
    }

    /**
     * Oppdaterer users variablen til å inneholde alle brukere.
     */
    firstUpdated() {
        console.log("-- fetching users.");

        let url = `${window.MyAppGlobals.serverURL}retrieveAllUsers`;
        // sender en fetch til api'et.
        fetch(url)
        .then(response => { return response.json() }).then(data => {
            this.users = data;

            // for hver bruker kjøres koden, skaper en score og ungår å dele på 0.
            this.users.map((user) => {
                if (user.losses == 0) { user.ratio = user.wins; } 
                else { user.ratio = (user.wins / user.losses).toFixed(2); }
            });
            
            // sorterer brukerne basert på score.
            this.users.sort((a, b) => {
                return  b.ratio - a.ratio;
            });
            
            console.log(this.users);
        }).catch(err => { console.log(err) });


        // ${this.users.map(user => html`<luser-element .user=${user} .i=${++this.i}></luser-element>`)}
    }
}
customElements.define('leaderboard-element', Leaderboard);