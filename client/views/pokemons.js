import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import './nav.js';

/**
 * Skal vise alle pokemons og la deg bytte ut de pokemonene som er i ditt team.
 * i framtiden kan det være aktuelt å lage en redirect til en create pokemon
 * vindu eller web-komponent.
 */
export class Pokemons extends LitElement {
    /**
     * klassens varibler
     */
    static get properties() {
        return {
            pokemons: { type: Array },
            pokemon: { type: Object },
            team: { type: Array },
            activPokemon: { type: Object },
            i: { type: Number } // det funker med denne var, ikke rør...
        }
    }

    /**
     * klassens konstruktor
     */
    constructor() {
        super();
        this.pokemons = [];
        this.pokemon = {};
        this.team = [];
        this.i = 0;
        this.activPokemon = {"id" : 0, "name" : ""};
    }

    /**
     * css style til render()'s html kode
     */
    static styles = css`
    .body {
        display: grid;
        width: 100vw;
        height: 90vh;
        justify-content: center;
        align-items: center;
    }

    .pokemons {
        display: grid;
        grid-template-columns: repeat(3, 200px);
        grid-template-rows: 200px;
        grid-gap: 5px;
        margin: 5px;
        width: 650px;
        height: 630px;
        border: 1px solid black;
        overflow: auto;
        padding: 10px;
        border-radius: 10px;
    }
    .pokemons .card {
        padding: 2px;
        border: 1px solid black;
        width: 200px;
        height: 200px;
        border-radius: 5px;
        display: grid;
        filter: grayscale(1);
        transition: .3s;
        cursor: pointer;
    }

    .cardImg {
        width: 100%;
        height: 150px;
        object-fit: contain;
    }

    .cardTitle {
        font-size: 20px;
        width: 100%;
        text-align: center;
        position: absolute;
        top: 85%;
    }

    .pokemons .card:hover {
        filter: grayscale(0);
    }

    .info {
        width: 460px;
        height: 630px;
        padding: 5% 10px;
        filter: grayscale(1);
    }

    .info img {
        width: 200px;
        height: auto;
        padding: 10px;
    }

    .silhouette {
        filter: contrast(0) brightness(.2);
    }

    .info h3 {
        font-family: 'Press Start 2P', sans-serif;
        transform: translateY(100px);
    }

    .msg {
        width: 400px;
        border: 1px solid black;
        background: rgba(255,255,255,.95);
        position: absolute;
        top: 25%;
        left: 50%;
        transform: translateX(-50%);
        z-index: 100;
        display: grid;
        align-items: center;
        justify-content: center;
        border-radius: 10px;
    }

    .msg .btn-close {
        position: absolute;
        top: 10px;
        right: 15px;
    }

    .msg .team {
        display: grid;
        width: 300px;
        grid-template-columns: 10% 65% 20%;
        grid-gap: 5px;
        padding: 7px;
        border-radius: 5px;
    }

    .msg .card {
        margin: 2px;
        cursor: pointer;
        transition: .5s;
    }

    .msg .card:hover {
        border-color: gray;
    }

    .team img {
        width: 100%;
        height: auto;
    }

    #msg { display: none; }

    ::-webkit-scrollbar {
        width: 5px;
        transform: translateX(-2px);
    }
    ::-webkit-scrollbar-thumb {
        background: #888;
        border-radius: 3px;
    }
    ::-webkit-scrollbar-thumb:hover {
        background: #555;
    }
    `;

    /**
     * html koden som skaper det visuele i web komponenten.
     * @returns html
     */
    render() {
        return html`
        <!-- linker til alternative fonter -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&family=Roboto+Slab:wght@400&display=swap" rel="stylesheet">

        <!-- henter inn nav-baren, fra nav.js -->
        <nav-element></nav-element>
        <article class="body">
            <article class="container-fluid">
                <div class="row justify-content-center">
                    <article class="col-xl-7 mt-1">
                        <div class="input-group" style="width: 650px">
                            <a href="#" class="btn btn-outline-primary" disabled>Create new</a>
                            <input type="text" label="search" class="form-control" disabled>
                            <span class="input-group-text" disabled> Search </span>
                        </div>

                        <article class="pokemons">
                        <!-- for hver pokemon i variablen pokemons, skriver ut bilder av hver pokemon -->
                        ${this.pokemons.map((pokemon) => html`
                            <div class="card" id="${pokemon.id}" @click=${this.highlightPokemon}>
                                <div style="align-self: center;" id="${pokemon.id}">
                                    <img src="${pokemon.sprite}" alt="${pokemon.name}" class="cardImg" id="${pokemon.id}">
                                    <h1 class="cardTitle" id="${pokemon.id}">${pokemon.name}</h1>
                                </div>
                            </div>
                        `)}
                        </article>
                    </article>

                    <article class="col-lg-5 mt-2">
                        <article class="container info" id="info">
                            <div class="row" style="border-bottom: 2px solid black;">
                                <div class="col-6"><img id="img" src="images/charizard.png" class="silhouette" alt="Silhouette"></div>
                                <div class="col-6"><h3 id="name">UNKNOWN</h3></div>
                            </div>
                            <div class="col-12 table-responsive"  style="transform: translateY(10px);">
                                <table class="table text-center">
                                    <tr>
                                        <td id="typePrim">
                                            Fire
                                        </td>
                                        <td id="typeSec">
                                            Dragon
                                        </td>
                                        <td id="sex">
                                            Sex: male
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-12 table-responsive" style="transform: translateY(-5px);">
                                <table class="table align-middle">
                                <tbody>
                                    <tr>
                                        <td>
                                            HP
                                        </td>
                                        <td>
                                            <div class="progress" style="width: 300px;">
                                                <div id="hp" class="progress-bar bg-warning" style="width: 70%"></div>
                                            </div>
                                        </td>
                                        <td id="hpNr">
                                            85
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Attack
                                        </td>
                                        <td>
                                            <div class="progress" style="width: 300px;">
                                                <div id="attack" class="progress-bar bg-danger" style="width: 90%"></div>
                                            </div>
                                        </td>
                                        <td id="attackNr">
                                            110
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Defence
                                        </td>
                                        <td>
                                            <div class="progress" style="width: 300px;">
                                                <div id="defence" class="progress-bar bg-success" style="width: 50%"></div>
                                            </div>
                                        </td>
                                        <td id="defenceNr">
                                            55
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Speed
                                        </td>
                                        <td>
                                            <div class="progress" style="width: 300px;">
                                                <div id="speed" class="progress-bar bg-secondary" style="width: 40%"></div>
                                            </div>
                                        </td>
                                        <td  id="speedNr">
                                            40
                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>

                            <div class="col-12"><h5>Moves</h5></div>
                            <div class="row">
                                <div class="col-6 d-grid">
                                    <button id="move1" class="btn btn-outline-primary" disabled>Move</button>
                                </div>
                                <div class="col-6 d-grid">
                                    <button id="move2" class="btn btn-outline-primary" disabled>Move</button>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 d-grid">
                                    <button id="move3" class="btn btn-outline-primary" disabled>Move</button>
                                </div>
                                <div class="col-6 d-grid">
                                    <button id="move4" class="btn btn-outline-primary" disabled>Move</button>
                                </div>
                            </div>
                            
                            <hr>
                            
                            <div class="row justify-content-center">
                                <div class="col-9 d-grid">
                                    <button id="btn-addTeam" values="" class="btn btn-outline-primary" @click=${this.openTeam} disabled>Add to team</button>
                                </div>
                            </div>
                        </article>   
                    </article>
                </div>
            </article>
            </article>

            <article class="msg pb-4" id="msg">
                <h3 class="text-secondary mt-2"> Change a Pokémon to <br> ${this.activPokemon.name}</h3>
                <!-- skriver ut alle pokemonene i den inn loggede brukerens team -->
                ${this.team.map(pokemon => html`
                    <div class="card mb-2" style="width: 300px" id=${JSON.stringify(pokemon)} @click=${this.changeTeam}">
                        <div class="card-body team" id=${JSON.stringify(pokemon)}>
                            <article> <img src="images/${pokemon.name}.png" alt="p" class="silhouette" id=${JSON.stringify(pokemon)}> </article>
                            <artilce id=${JSON.stringify(pokemon)}> ${pokemon.name} </artilce>
                            <article id=${JSON.stringify(pokemon)}>  </article>
                        </div>
                    </div>
                `)}

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click=${this.abort}></button>
            </article>
        `;
    }

    /**
     * spør api'et om pokemons og oppdaterer pokemons variablen.
     */
    firstUpdated() {
        console.log("-- fetching pokemons.");

        let url = `${window.MyAppGlobals.serverURL}pokemons`;
        // sender en fetch req til api'et
        fetch(url)
        .then(response => { return response.json() }).then(data => { // ved svar
            data.map((element) => { // for hvert element i data
                let pokemon = {};
                // hvis elementet mangler bilde
                if (element.sprite == "Not yet implemented") {
                    pokemon.sprite = "images/" + element.name + ".png";
                } else { // hvis elementet har bilde
                    pokemon.sprite = element.sprite;
                }
                pokemon.name = element.name;
                pokemon.id = element.id;
                this.pokemons.push(pokemon); // legg til pokemonen i pokemons
            });
            console.log(this.pokemons);
            this.i++; // push "oppdatere" ikke noen variable, oppdaterer derfor i for å få funksjonen til å updatere render()
        }).catch(err => { console.log(err) });
    }

    /**
     * oppdaterer den html koden som viser mer data om en enkelt pokemon.
     * @param e 
     */
    highlightPokemon(e) {
        let elementList = { // liste over alle id'ene som skal oppdateres
            "name" : this.shadowRoot.getElementById("name"),
            "img" : this.shadowRoot.getElementById("img"),
            "typePrim" : this.shadowRoot.getElementById("typePrim"),
            "typeSec" : this.shadowRoot.getElementById("typeSec"),
            "sex" : this.shadowRoot.getElementById("sex"),
            "hp" : this.shadowRoot.getElementById("hp"),
            "hpNr" : this.shadowRoot.getElementById("hpNr"),
            "attack" : this.shadowRoot.getElementById("attack"),
            "attackNr" : this.shadowRoot.getElementById("attackNr"),
            "defence" : this.shadowRoot.getElementById("defence"),
            "defenceNr" : this.shadowRoot.getElementById("defenceNr"),
            "speed" : this.shadowRoot.getElementById("speed"),
            "speedNr" : this.shadowRoot.getElementById("speedNr"),
            "move1" : this.shadowRoot.getElementById("move1"),
            "move2" : this.shadowRoot.getElementById("move2"),
            "move3" : this.shadowRoot.getElementById("move3"),
            "move4" : this.shadowRoot.getElementById("move4"),
            "btn" : this.shadowRoot.getElementById("btn-addTeam")
        };
        elementList.btn.disabled = false;
        elementList.btn.value = e.target.id;

        let url = `${window.MyAppGlobals.serverURL}pokemon`;
        fetch(url, {    // fetch til api'et
            method: 'POST',
            headers: { 
                'Accept': 'application/json',
                'Content-Type':  'application/json'
            },
            // inneholder id'en til den pokemonen vi ønsker mer info om.
            body: JSON.stringify({"id" : e.target.id})
        }).then(response => { return response.json() }).then(data => {
            console.log(data[0]);
            this.shadowRoot.getElementById("info").style.filter = "none";
            this.activPokemon.name = data[0].name;

            elementList.name.innerHTML = data[0].name;
            elementList.typePrim.innerHTML = data[0].type_primary;
            elementList.typeSec.innerHTML = (data[0].type_secondary == "none") ? " " : data[0].type_secondary;
            elementList.sex.innerHTML = data[0].sex;
            elementList.hpNr.innerHTML = data[0].hp;
            elementList.attackNr.innerHTML = data[0].attack;
            elementList.defenceNr.innerHTML = data[0].defense;
            elementList.speedNr.innerHTML = data[0].speed;

            elementList.img.src = "images/" + data[0].name + ".png";

            let bars = {
                "hp" : data[0].hp,
                "attack" : data[0].attack,
                "defence" : data[0].defense,
                "speed" : data[0].speed
            }
            
            // for hvert element i bars objektet, skal oppdatere farge, tall og prosent verdi til progress-barene
            for (const [key, index] of Object.entries(bars)) {
                let progress = 0;
                let color = "";

                if (index >= 130) { progress = 100;} 
                else { progress = (index / 130).toFixed(2) * 100; }

                if (progress > 85) { color = "danger"; }
                else if (progress > 70) { color = "warning"; }
                else if (progress > 40) { color = "success"; }
                else { color = "secondary"; }

                if (elementList[key].classList.contains("bg-danger")) { elementList[key].classList.remove("bg-danger") }
                else if (elementList[key].classList.contains("bg-warning")) { elementList[key].classList.remove("bg-warning") }   
                else if (elementList[key].classList.contains("bg-success")) { elementList[key].classList.remove("bg-success") }
                else { elementList[key].classList.remove("bg-secondary") }
                elementList[key].style.width = progress + "%";
                elementList[key].classList.add("bg-" + color);
            }

            let moves = [
                {"id" : data[0].move_first, "name" : "", "element" : "move1"},
                {"id" : data[0].move_second, "name" : "", "element" : "move2"},
                {"id" : data[0].move_third, "name" : "", "element" : "move3"},
                {"id" : data[0].move_fourth, "name" : "", "element" : "move4"}
            ];

            (async() => {   // henter navnene på movene, så det ikke står tall verdier men selve navnet.
                for await (let move of moves) {
                    this.getMoveInfo(move.id).then((result) => {
                        move.name = result[0].name;
                        elementList[move.element].innerHTML = move.name;
                    });
                }
            })();
        });
    }

    /**
     * Skal returnere et moves navn
     * @param id - id'en til et move.
     * @returns promis -> resolves - move navn.
     */
    getMoveInfo(id) {
        return new Promise((resolve, reject) => { // returnerer promis
            let url = `${window.MyAppGlobals.serverURL}moveLite`;
            fetch(url, {   // gjør en fetch til api'et
                method: 'POST',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type':  'application/json'
                },
                body: JSON.stringify({"id" : id}) // sender med move id'en.
            }).then(response => { return response.json() }).then(data => {
                resolve(data); // resolver promise med resultatet fra api'et
            });
        });
    }

    /**
     * åpner team viduet, viser alle pokemonene som er i ditt team og lar deg bytte
     * en av dem med den pokemonen du har highlightet.
     * @param e - der funksjonen blir startet, inneholder highligheds pokemon id.
     */
    openTeam(e) {
        this.activPokemon.id = e.target.value;

        // forespør api'et, skal få tilbake alle info om innlogget bruker.
        let url = `${window.MyAppGlobals.serverURL}user`;
        fetch(url, {
            method: 'GET',
            credentials: 'include',
            headers: { 
                'Accept': 'application/json',
                'Content-Type':  'application/json'
            }
        }).then(response => { return response.json() }).then(data => {
            this.team = new Array;

            // for hvert element i svaret fra api'ets variabel team.
            Object.entries(data.team).forEach(pokemon => {
                pokemon[1]["slot"] = pokemon[0];
                this.team.push(pokemon[1]); // pusher pokemonen inn i et array
            });
            console.log(this.team);
            this.i++; // for å oppdatere render() siden .push ikke oppdaterer en variable.
        });
        this.shadowRoot.getElementById("msg").style.display = "grid";
    }

    /**
     * bytter ut en pokemon i ditt team med den highlightede pokemonen.
     * @param e - fra der funksjonen blir kjørt.
     */
    changeTeam(e) {
        let currentPokemon = e.target.id;
        currentPokemon = JSON.parse(currentPokemon);
        let newPokemon = this.activPokemon;

        // vertifiserer om du ønsker å bytte ut pokemonen.
        if (confirm(`Are you sure you want to change ${currentPokemon.name} to ${newPokemon.name}`)) {
            let url = `${window.MyAppGlobals.serverURL}updateTeam`;
            fetch(url, { // fetch den oppdaterte infoen til api'et
                method: 'POST',
                credentials: 'include',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type':  'application/json'
                },
                // sender med hvilket team slot som skal få hvilken nye pokemon
                body: JSON.stringify({"slot" : currentPokemon.slot, "new": newPokemon.id})
            }).then(response => { return response.json() }).then(data => {
                console.log(data);
                this.abort(); // lukker vinduet.
            });
        } else {
            console.log("ABORT!!!!");
        }
    }

    /**
     * lukker team vinduet.
     */
    abort() {
        this.shadowRoot.getElementById("msg").style.display = "none";
    }
}
customElements.define('pokemons-element', Pokemons);