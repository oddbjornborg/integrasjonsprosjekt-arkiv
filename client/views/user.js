import { LitElement, html, css } from '../node_modules/lit-element/lit-element';

/**
 * web-komponentet skal vise all info om brukeren.
 * Et forbedrings potenisale er å lage funksjoner til noen av knappene på komponentet.
 */
export class User extends LitElement {
    /**
     * klassens variabler
     */
    static get properties() {
        return {
            user: { type: Object},
            team: { type: Array },
            ratio: { type: Number }
        }
    }

    /**
     * klassens konstruktor
     */
    constructor() {
        super();
        this.user = {};
        this.team = [];
        this.ratio = 0;
    }

    /**
     * css koden som gir styling til html koden i reader()
     */
    static styles = css`
    .body {
        display: grid;
        justify-items: center;
        align-items: center;
        width: 100vw;
        height: 90vh;
    }

    .grid {
        display: grid;
        grid-template-columns: 30% 70%;
        grid-template-rows: 30% 70%;
        grid-gap: 5px;
        width: 800px;
        height: 70vh;
    }

    .userName {
        font-family: 'Press Start 2P', sans-serif;
        transform: translate(-2vw, 14vh);
    }

    .pokemon {
        display: grid;
        grid-template-columns: 58% 30% 10%;
        grid-gap: 5px;
        padding: 7px;
    }
    `;

    /**
     * html koden som skaper det visuele til nettsiden.
     * @returns html
     */
    render() {
        return html`
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&family=Roboto+Slab:wght@400&display=swap" rel="stylesheet">
           
        <nav-element></nav-element>
        <article class="body">
            <article class="grid">
                <article>
                    <img src="images/#" alt="user">
                </article>
                <article>
                    <h1 class="userName">${this.user.name}</h1>
                </article>
                <article style="border-right: 2px solid grey">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <td>Wins : </td>
                                <td>${this.user.wins}</td>
                            </tr>
                            <tr>
                                <td>Loss : </td>
                                <td>${this.user.loss}</td>
                            </tr>
                            <tr>
                                <td>Win/loss : </td>
                                <td>${this.ratio}</td>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <a class="btn btn-outline">Change Password</button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a class="btn btn-outline">Delete self</a>
                                </td>
                            </tr>    
                        </tbody>
                    </table>
                </article>
                <article>
                    <h5 class="text-muted">Your team</h5>
                    <!-- for hvert element i team, viser hvilke pokemon som er i ditt team -->
                    ${this.team.map((pokemon) => html`
                        <div class="card mb-1">
                            <div class="card-body pokemon">
                                <article>
                                    ${pokemon.name}
                                </article>
                                <article>
                                    ${pokemon.type_primary}${pokemon.type_secondary == "none" ? "" : ", " + pokemon.type_secondary}
                                </article>
                                <article>
                                    lv : ${pokemon.level}
                                </article>
                            </div>
                        </div>
                    `)}
                </article>
            </article>
        </article>
        `;
    }

    /**
     * oppdaterer informasjonen om brukeren.
     */
    firstUpdated() {
        console.log("-- fetching user information.");

        let url = `${window.MyAppGlobals.serverURL}user`;
        // fetch til api'et, henter info om brukeren
        fetch(url, {
            method: 'GET',
            credentials: 'include', // innholder cookien du får ved innlogging
            headers: { 
                'Accept': 'application/json',
                'Content-Type':  'application/json'
            }
        }).then(response => { return response.json() }).then(data => {
            console.log(data);

            // oppdaterer variabler
            this.user.name = data.username;
            this.user.wins = data.wins;
            this.user.loss = data.losses;

            // gir brukeren en score.
            if (data.losses == 0) { this.ratio = data.wins; } 
            else { this.ratio = (data.wins / data.losses).toFixed(2); }

            // hvor hvert element i api-resultatets team variable
            Object.entries(data.team).forEach((pokemon) => {
                this.team.push(pokemon[1]); // pusher hver pokemon inn i en egen variable.
            });
        }).catch(err => { console.log(err) });
    }  
}
customElements.define('user-element', User);