import { LitElement, html, css } from '../node_modules/lit-element/lit-element';

/**
 * meny siden for tjenesten, fra her skal man kunne bli sendt videre til alle sider 
 */
export class menu extends LitElement {
    /**
     * sidens css style
     */
    static styles = css`
        * {
            margin: 0;
            padding: 0;
        }

        main {
            display: grid;
            position: absolute;
            width: 100vw;
            height: 100vh;
            text-align: center;
        }

        h1 {
            font-size: 8rem;
            font-family: 'Roboto Slab', serif;
            margin: 40px;
        }

        li {
            font-family: 'Press Start 2P', sans-serif;
            font-size: 2rem;
            margin: 27px 0;
            list-style: none;
        }

        li a {
            text-decoration: none;
            color: gray;
            transition: .3s;
        }

        li a span {
            color: rgba(128, 128, 128, 0);
            transition: .3s;
        }

        li a:hover, li a:hover span {
            color:black;
        }
    `;

    /**
     * html koden som utgjør nettsiden.
     * @returns html
     */
    render() {
        return html`
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&family=Roboto+Slab:wght@400&display=swap" rel="stylesheet">
        
        <main>
            <article style="place-self: center;">
                <h1>Pokémon</h1>
                <ul>
                    <li><a href="/play">
                        <span>> </span> Play <span> <</span>
                    </a></li>
                    <li><a href="/leaderboard">
                        <span>> </span> Leaderboard <span> <</span>
                    </a></li>
                    <li><a href="/pokemons">
                        <span>> </span> Pokémons <span> <</span>
                    </a></li>
                    <li><a href="/userpage">
                        <span>> </span> Userpage <span> <</span>
                    </a></li>
                </ul>
            </article>
            <article></article>
            <article></article>
        </main>
        `;
    }
}
customElements.define('menu-element', menu);