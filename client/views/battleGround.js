import { LitElement, html, css } from 'lit-element';
import * as pokemonClass from '../classes/pokemons.js'
import * as UserClass from '../classes/User.js'

/**
 * Website for playing the game
 * 
 * Creates visual elements based on user and opponents teams.
 * Players are able to make a move which sends request to the backend API, 
 * the turn is not registered before both players have made their move.
 * After a turn has finished, updates visual elements attributes and data.
 */
export class battleGround extends LitElement {

    /**
    * Variablene til klassen
    */
    static get properties() {
        return {
          
            socket: {}, //refference to the socket connection established in parent lit-elemnts
            roomName: { type: String},  //room231331...
            playerId: { type: Number }, //1..9999
            opponentId: { type: Number }, //1..9999
            
            // myTeam: { type: Array },    //array of ints, int represent id of pokemon
            // opponentTeam: { type: Array }, //array of ints, int represent id of pokemon
            isHost: { type: Boolean },   //true if this player created the game, false else
            myself: {},     //@see user class, represents this player as an user
            enemy: {},   //@see user class, represents the oponent as an user
            opponentActivePokemon: {},  //Array of all pokemons in opponent's team
            myActivePokemon: {},        //Array of all pokemons in user's team
            messageQueue: [],           //For future devlopment, used to store all messages generated while playing the game. For eksample, "Pokemon has made move X"
            i: { type: Number},         //Important integer for use to force html refreshes. For example we update value of i which makes html render again
        }
    }
    
    /**
     * Define values to be defined by parent
     * In practice all values needs to be sent from parent to be able to play the game
     * @see gameLobby
     */
    constructor() {
        super();
        this.i = 0;
        this.myself;     //@see user class, represents this player as an user
        this.enemy;   //@see user class, represents the oponent as an user
        this.opponentActivePokemon;
        this.myActivePokemon;
        this.messageQueue = [];
        // this.myTeam = [];
        // this.opponentTeam = [];
        this.playerId;
        this.opponentId;
    }
    
    /**
     * stylingen of html code to be rendered inn render()
     */
    static styles = css`
        /* Placement of main, center top */
        .body {
            display: grid;
            justify-items: center;
            width: 100vw;
        }

        /* Grid layout and general articles placement */
        main {
            display: grid;
            width: 900px;
            height: 600px;
            grid-template-columns: repeat(6, 150px);
            grid-template-rows: repeat(6, 150px);
        }
        .enemyPokemon { grid-area: 1 / 4 / 3 / 7; }
        .enemyHp { grid-area: 1 / 1 / 3 / 4;}
        .yourPokemon { grid-area: 3 / 1 / 5 / 4; transform: scaleX(-1); }
        .yourHp { grid-area: 3 / 4 / 5 / 7; }
        .yourMoves { grid-area: 5 / 1 / 7 / 7; }
        
        /* Pokemon img placement */
        .enemyPokemon, .yourPokemon {
            display: grid;
            align-items: flex-end;
            justify-items: center;
        }
        .ground {
            position: absolute;
            height: 400px;
            width: 400px;
            background-color: rgb(156, 209, 121);
            border-radius: 50%;
            transform: rotateX(70deg) translateY(540px);
            z-index: 0;
        }
        .shadow {
            height: 200px;
            width: 200px;
            background-color: rgba(0,0,0,.5);
            z-index: 1;
            transform: rotateX(70deg) translateY(250px);
        }

        /* Pokemon hp bar placement */
        .enemyHp, .yourHp {
            display: grid;
            place-items: center;
        }
        .yourHp { transform: translate(-70px, 70px); }
        .enemyHp { transform: translate(70px, 40px); }

        /* General rules */
        .enemyPokemon img, .yourPokemon img {
            height: 200px;
            width: auto;
            margin: 0 50px;
            z-index: 3;
            padding: 0 0 10px 0;
        }

        .yourMoves {
            background-color: white;
            z-index: 2;
        }

        .switch {
            display: grid; 
            place-items: center;
        }
        .switch img {
            height: 20px;
            margin: 0;
            padding: 0;
        }

        .msgBattle {
            width: 400px;
            border: 1px solid black;
            background: rgba(255,255,255,.95);
            position: absolute;
            top: 25%;
            left: 50%;
            transform: translateX(-50%);
            z-index: 100;
            display: grid;
            align-items: center;
            justify-content: center;
            border-radius: 10px;
        }
    
        .msgBattle .btn-close {
            position: absolute;
            top: 10px;
            right: 15px;
        }
    
        .msgBattle .team {
            display: grid;
            width: 300px;
            grid-template-columns: 10% 65% 20%;
            grid-gap: 5px;
            padding: 7px;
            border-radius: 5px;
        }
    
        .msgBattle .card {
            margin: 2px;
            cursor: pointer;
            transition: .5s;
        }
    
        .msgBattle .card:hover {
            border-color: gray;
        }
    
        .team img {
            width: 100%;
            height: auto;
        }
    
        #msgBattle { display: none; }

        /* visualisering av status effekter, css animajoner */
        .poison { animation: poisoned 2s infinite linear; }
        .bad_poison { animation: poisoned 2s infinite linear; }
        @keyframes poisoned {
            0%      { filter: none; }
            1%      { filter: contrast(.4) sepia(100%) hue-rotate(210deg) saturate(2000%) brightness(100%); }
            30%     { filter: contrast(.4) sepia(100%) hue-rotate(210deg) saturate(2000%) brightness(100%); }
            31%     { filter: none; }
            100%    { filter: none; }
        }

        .para { animation: paralize 2s infinite linear; }
        @keyframes paralize {
            0%      { filter: none; }
            1%      { filter: contrast(.4) sepia(100%) hue-rotate(20deg) saturate(2000%) brightness(90%); }
            30%     { filter: contrast(.4) sepia(100%) hue-rotate(20deg) saturate(2000%) brightness(90%); }
            31%     { filter: none; }
            100%    { filter: none; }
        }

        .burn { animation: burned 2s infinite linear; }
        @keyframes burned {
            0%      { filter: none; }
            1%      { filter: contrast(.4) sepia(100%) hue-rotate(-10deg) saturate(2000%) brightness(90%); }
            30%     { filter: contrast(.4) sepia(100%) hue-rotate(-10deg) saturate(2000%) brightness(90%); }
            31%     { filter: none; }
            100%    { filter: none; }
        }

        .freeze { animation: frozen 2s infinite linear; }
        @keyframes frozen {
            0%      { filter: none; }
            1%      { filter: contrast(.4) sepia(100%) hue-rotate(175deg) saturate(2000%) brightness(80%); }
            30%     { filter: contrast(.4) sepia(100%) hue-rotate(175deg) saturate(2000%) brightness(80%); }
            31%     { filter: none; }
            100%    { filter: none; }
        }

        .sleep { animation: doze 2s infinite linear; }
        @keyframes doze {
            0%      { filter: none; }
            1%      { filter: contrast(.2) grayscale(.2) brightness(180%); }
            30%     { filter: contrast(.2) grayscale(.2) brightness(180%); }
            31%     { filter: none; }
            100%    { filter: none; }
        }
    `;
    
    /**
     * Html to be displayed which is dynamically defined with javascript.
     * @returns html
     */
    render() {
        return html`
        <article class="body">
        <div>Put informasjon til lobby her</div>
        <main>
            <article class="enemyPokemon">
                <img id="enemyPokemon" src="../images/${this.enemy.team[this.opponentActivePokemon].name}.png">
                <div class="ground shadow"></div>
                <div class="ground"></div>
            </article>
    
            <article class="enemyHp">
                <article class="container" style="width: 300px">
                    <div class="row">
                        <h3 class="col-7">${this.enemy.team[this.opponentActivePokemon].name}</h3>
                        
                        <p class="col-5" style="transform: translateY(10px)">
                            <strong>HP:</strong> ${this.enemy.team[this.opponentActivePokemon].hpCurrent} / ${this.enemy.team[this.opponentActivePokemon].hpMax}
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-success" style="width: 100%;" role="progressbar" id="enemyHpBar"></div>
                            </div>
                        </div>
                    </div>
                </article>
            </article>
    
            <article class="yourPokemon">
                <img id="yourPokemon" src="../images/${this.myself.team[this.myActivePokemon].name}.png">
                <div class="ground shadow"></div>
                <div class="ground"></div>
            </article>
    
            <article class="yourHp">
                <article class="container" style="width: 300px">
                    <div class="row">
                        <h3 class="col-7">${this.myself.team[this.myActivePokemon].name}</h3>
                        <p class="col-5" style="transform: translateY(10px)" id="yourHpStat">
                            <strong>HP:</strong> ${this.myself.team[this.myActivePokemon].hpCurrent} / ${this.myself.team[this.myActivePokemon].hpMax}
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-success" style="width: 100%" role="progressbar" id="yourHpBar"></div>
                            </div>
                        </div>
                    </div>
                </article>
            </article>
    
            <article class="yourMoves container">
                <hr>
                <div class="row">
                    <div class="col-6 d-grid" style="place-items: center;" id="mq"></div>
                    <div class="col-6 container">
                        <div class="row">
                            <!-- oppdaterer move knappene til å inneholde riktig -->
                            ${this.myself.team[this.myActivePokemon].moveList.map((index, key) => html`
                                ${console.log("fra moveliste - index: " + index + ", key: " + key)}
                                <div class="col-5 d-grid mb-2" style="transform:translateX(20px)">
                                    <button value="${this.i++}" type="button" @click=${this.chooseAction} class="btn btn-outline-primary">${index.name}</button>
                                </div>

                                ${key == 1 ? html`
                                    <div class="col-2 d-grid mb-2" data-bs-toggle="tooltip" data-bs-placement="left" title="Change Pokémon">
                                        <button class="btn btn-outline-success switch" @click=${this.openTeam} role="button">
                                            <img src="images/switch.png">
                                        </button>
                                    </div>
                                `: ""}
                                
                                ${key == 3 ? html`
                                    <div class="col-2 d-grid mb-2" data-bs-toggle="tooltip" data-bs-placement="left" title="Give up">
                                        <a class="btn btn-outline-danger" href="/" role="button" style="width: 47px">X</a>
                                    </div>
                                `: ""}
                            `)}
                        </div>
                    </div>
                </div>
            </article>
    
        </main>

            <article class="msgBattle pb-4" id="msgBattle">
                <h3 class="text-secondary mt-2"> Change Pokémon from <br> ${this.myself.team[this.myActivePokemon].name} </h3>
                <!-- lister ut alle pokemonene, i en meldings boks -->
                ${this.myself.team.map(pokemon => html`
                    <div class="card mb-2" style="width: 300px" id=${pokemon.id} @click=${this.changePokemon}>
                        <div class="card-body team" id=${pokemon.id}>
                            <article> <img src=${"images/" + pokemon.name + ".png"} alt="p" class="silhouette" id=${pokemon.id}> </article>
                            <article id=${pokemon.id}> ${pokemon.name} </article>
                            <article id=${pokemon.id}>  </article>
                        </div>
                    </div>
                `)}

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click=${this.closeTeam}></button>
            </article>
        </article>
    `}

    /*************************************************************************************
     * Retrieves which move was selected by user and updates database with according move. 
     * Emits which player action was made.
     * @author Nicholas Sellevåg
     ************************************************************************************/
    chooseAction(e) {
        //  console.log("User " + this.playerId + ", isHost: " + this.isHost + ", has chosen action " + e.target.value);


        let parameters = {"user" : this.playerId, "action" : e.target.value, "isHost" : this.isHost}
        this.socket.emit("choose action", parameters);
        
        this.socket.once('player-turn', (parameters) => {
            // console.log("Player_Action: " + parameters.Player_Action + ", Host_action: " + parameters.Host_Action)
            this.executeActions(parameters.Host_Action, parameters.Player_Action)
        })
    }

    /*************************************************************************************
     * Called when both players have made their move and comes with data from database 
     * Each turn is ended when actions are performed, followed up by another turn.
     * Effectively defines the game loop 
     * @author Nicholas Sellevåg
     ************************************************************************************/
    executeActions(hostAction, playerAction) {
        //Who goes first

        //player goes first
        var imFirst = this.myself.team[this.myActivePokemon].isFasterThan(this.enemy.team[this.opponentActivePokemon])    //Finn ut hvem som skal gå først

        if (imFirst) {
            if (this.isHost) {
                this.myTurn(hostAction)
                this.opponentTurn(playerAction)
                this.updateHtml()
            } else {
                this.myTurn(playerAction)
                this.opponentTurn(hostAction)
                this.updateHtml()
            }
            
        }
        //Opponent goes first 
        else {
            if(this.isHost) {
                this.opponentTurn(playerAction)
                this.myTurn(hostAction)
                this.updateHtml()
            } else {
                this.opponentTurn(hostAction)
                this.myTurn(playerAction)
                this.updateHtml()
            }
        }
    }

    /*************************************************************************************
     * Called whenever users attributes needs to be changed in the html displayed to users
     * @author Nicholas Sellevåg
     ************************************************************************************/
    updateHtml() {
        console.log("Messageque: " + this.messageQueue)

        
        var yourHp = ((this.myself.team[this.myActivePokemon].hpCurrent / this.myself.team[this.myActivePokemon].hpMax) * 100).toFixed(2)
        var enemyHp = ((this.enemy.team[this.opponentActivePokemon].hpCurrent / this.enemy.team[this.opponentActivePokemon].hpMax) * 100).toFixed(2)
        this.shadowRoot.getElementById('yourHpBar').style.width = yourHp + "%";
        this.shadowRoot.getElementById('enemyHpBar').style.width = enemyHp + "%";

        // // sets a status effect for your pokemon
        // if (this.myself.team[this.myActivePokemon].statusCondition == "none") {
        //     this.shadowRoot.getElementById('yourPokemon').classList = [];
        // } else {
        //     this.shadowRoot.getElementById('yourPokemon').classList = [this.myself.team[this.myActivePokemon].statusCondition];
        // }
        
        // // sets a status effect for opponents pokemon
        // if (this.enemy.team[this.opponentActivePokemon].statusCondition == "none") {
        //     this.shadowRoot.getElementById('enemyPokemon').classList = [];
        // } else {
        //     this.shadowRoot.getElementById('enemyPokemon').classList = [this.enemy.team[this.opponentActivePokemon].statusCondition];
        // }
        
    }

    /*************************************************************************************
     * Perform users action before opponent
     * @author Nicholas Sellevåg
     ************************************************************************************/
    myTurn(action) {
        console.log("My turn")
        var rolls=0;
        // this.myself.team.forEach(pokemon => {
        //     Object.entries(pokemon).forEach(value => {
        //         console.log("Myself pokemon: " + value)
        //     });
        //     pokemon.moveList.forEach(move => {
        //         Object.entries(move).forEach(value => {
        //             console.log("Myself move: " + value)
        //         });
        //         move.moveEffects.forEach(effect => {
        //             // Object.entries(effect).forEach(pokemon => {
        //             //     console.log("mySelf attribut i object effect: " + pokemon)
        //             // });
        //             // console.log("mySelf Pokemon: " + pokemon.name + ", move.name: " + move.name + ", effect.target: " + effect.target)
        //         })
        //         // console.log("opponent Pokemon: " + pokemon.name + ", har move: " + move.name)
        //         // move.use(this.enemy, this.myself, rolls);
        //     })
        // })
        switch(action) {
            /*
            * Each move in switch corresponds to the pokemon's first, second... move
            */
            case "move1":
            console.log("myTurn: move1")
            this.messageQueue.push(this.myself.team[this.myActivePokemon].useMove(0, this.enemy.team[this.opponentActivePokemon], rolls)) 
            break;
            case "move2":
            console.log("myTurn: move2")
            this.messageQueue.push(this.myself.team[this.myActivePokemon].useMove(1, this.enemy.team[this.opponentActivePokemon], rolls))
            break;
            case "move3":
            console.log("myTurn: move3")
            this.messageQueue.push(this.myself.team[this.myActivePokemon].useMove(2, this.enemy.team[this.opponentActivePokemon], rolls))  
            break;
            case "move4":
            console.log("myTurn: move4")
            this.messageQueue.push(this.myself.team[this.myActivePokemon].useMove(3, this.enemy.team[this.opponentActivePokemon], rolls))  
            break;

            /*
            * Each swap in switch corresponds to the changing current pokemon into the first, second... pokemon in player's team
            */
            case "pokemon_swap1":
            console.log("myTurn: pokemon_swap1")
            this.myActivePokemon = 0;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
            case "pokemon_swap2":
            console.log("myTurn: pokemon_swap2")
            this.myActivePokemon = 1;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
            case "pokemon_swap3":
            console.log("myTurn: pokemon_swap3")
            this.myActivePokemon = 2;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
            case "pokemon_swap4":
            console.log("myTurn: pokemon_swap4")
            this.myActivePokemon = 3;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
            case "pokemon_swap5":
            console.log("myTurn: pokemon_swap5")
            this.myActivePokemon = 4;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
            case "pokemon_swap6":
            console.log("myTurn: pokemon_swap6")
            this.myActivePokemon = 5;
            this.messageQueue.push(this.myself.name + " changed pokemon to " + this.myself.team[this.myActivePokemon].name)
            break;
        }
        // this.requestUpdate();
        this.i = 0;
    }

    /*************************************************************************************
     * Perform opponents move before users
     * @author Nicholas Sellevåg
     ************************************************************************************/
    opponentTurn(action) {
        console.log("Opponent turn")
        var rolls=0;
        // // this.enemy.team.forEach(pokemon => {
        // //     pokemon.moveList.forEach(move => {
        // //         Object.entries(move).forEach(pokemon => {
        // //             console.log("attribut i object move: " + pokemon)
        // //         });
        // //         move.moveEffects.forEach(effect => {
        // //             Object.entries(effect).forEach(pokemon => {
        // //                 console.log("attribut i object effect: " + pokemon)
        // //             });
        // //             console.log("Pokemon: " + pokemon.name + ", move.name: " + move.name + ", effect.target: " + effect.target)
        // //         })
        // //         // console.log("opponent Pokemon: " + pokemon.name + ", har move: " + move.name)
        // //         // move.use(this.enemy, this.myself, rolls);
        // //     })
        // // })
        switch(action) {
            /*
            * Each move in switch corresponds to the pokemon's first, second... move
            */
            case "move1":
            console.log("myTurn: move1")
            this.messageQueue.push(this.enemy.team[this.opponentActivePokemon].useMove(0, this.myself.team[this.myActivePokemon], rolls))
            break;
            case "move2":
            console.log("myTurn: move2")
            this.messageQueue.push(this.enemy.team[this.opponentActivePokemon].useMove(1, this.myself.team[this.myActivePokemon], rolls))
            break;
            case "move3":
            console.log("myTurn: move3")
            this.messageQueue.push(this.enemy.team[this.opponentActivePokemon].useMove(2, this.myself.team[this.myActivePokemon], rolls))
            break;
            case "move4":
            console.log("myTurn: move4")
            this.messageQueue.push(this.enemy.team[this.opponentActivePokemon].useMove(3, this.myself.team[this.myActivePokemon], rolls))  
            break;

            /*
            * Each swap in switch corresponds to the changing current pokemon into the first, second... pokemon in player's team
            */
            case "pokemon_swap1":
            console.log("myTurn: pokemon_swap1")
            this.opponentActivePokemon = 0;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
            case "pokemon_swap2":
            console.log("myTurn: pokemon_swap2")
            this.opponentActivePokemon = 1;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
            case "pokemon_swap3":
            console.log("myTurn: pokemon_swap3")
            this.opponentActivePokemon = 2;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
            case "pokemon_swap4":
            console.log("myTurn: pokemon_swap4")
            this.opponentActivePokemon = 3;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
            case "pokemon_swap5":
            console.log("myTurn: pokemon_swap5")
            this.opponentActivePokemon = 4;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
            case "pokemon_swap6":
            console.log("myTurn: pokemon_swap6")
            this.opponentActivePokemon = 5;
            this.messageQueue.push(this.enemy.name + " changed pokemon to " + this.enemy.team[this.opponentActivePokemon].name)
            break;
        }
        // this.requestUpdate();
        this.i = 0;

    }

    /*************************************************************************************
     * Open menu for changing active pokemon
     * @author Nicholas Sellevåg
     ************************************************************************************/
    openTeam() { 
        this.shadowRoot.getElementById("msgBattle").style.display = "grid"; 
    }

    /*************************************************************************************
     * Close menu for changing active pokemon
     * @author Nicholas Sellevåg
     ************************************************************************************/
    closeTeam() { 
        this.shadowRoot.getElementById("msgBattle").style.display = "none"; 
    }

    /*************************************************************************************
     * Change current pokemon into the select pokemon
     * @author Nicholas Sellevåg
     ************************************************************************************/
    changePokemon(e) {
        let id = e.target.id;
        console.log("switch to Pokemon: " + id);

        // switch pokemon, to be implemented
    }

}
customElements.define('battle-element', battleGround);
