import { Router } from '@vaadin/router';
import './views/menu.js';
import './views/battleGround.js';
import './views/findGame.js';
import './views/leaderboard.js';
import './views/pokemons.js';
import './views/user.js';
import './views/login.js';
import './views/userRegister';
import './views/gameLobby';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

// konfigurasjon av ruteren.
router.setRoutes([{ 
    path: '/', animate: true, children: [
            { path: '/', component: 'menu-element' },
            { path: '/login', component: 'login-page' },
            { path: '/register', component: 'register-page' },
            { path: '/play', component: 'findgame-element' },
            { path: '/battle', component: 'battle-element' },
            { path: '/leaderboard', component: 'leaderboard-element' },
            { path: '/pokemons', component: 'pokemons-element' },
            { path: '/userpage', component: 'user-element' },
            { path: '(.*)', redirect: '/' }
    ]}   
]);

// listens to changes in location
window.addEventListener('vaadin-router-location-changed', (event) => {
    let location = event.detail.location.pathname;
    // console.log("-- location: " + location);

    isAuth().then(res => { 
        // console.log(res);
        if (!res && !(location == '/register' || location == '/login')) {
            window.location.href = '/register'; // sends user to login-page if not authenticated
        }
    });
    
});

// checks if user is authenticated
function isAuth() {
    return new Promise((resolve, reject) => {
        let url = `${window.MyAppGlobals.serverURL}isAuth`;
        fetch(url, {
            method:'get',
            credentials: 'include',
        })
        .then(response => { return response.json() }).then(data => {
            resolve(data);
        }).catch(err => { console.log(err) });
    });
}