-- Database for integrasjons prosjekt

CREATE Database `integrasjons_prosjekt` DEFAULT CHARACTER SET utf8;
USE `integrasjons_prosjekt`;

-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `pwd` VARCHAR(200) NOT NULL,
    `win` INT NOT NULL,
    `loss` INT NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `mp games`
-- -----------------------------------------------------
CREATE TABLE `mp_games` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `room` VARCHAR(20) NOT NULL,
    `host` INT NOT NULL,
    PRIMARY KEY (`id`)
);
ALTER TABLE `mp_games`
    ADD CONSTRAINT `fk_user_mp`
        FOREIGN KEY (`host`) REFERENCES `user` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Table `effect`
-- -----------------------------------------------------
CREATE TABLE `effect` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `status` VARCHAR(200) NOT NULL,
    `element` VARCHAR(200) NULL,
    `statsType` VARCHAR(200) NOT NULL,
    `modifiedValue` VARCHAR(200) NOT NULL,
    `targetSelf` BOOLEAN NOT NULL,
    `animation` varchar(16) DEFAULT 'NONE',
    `animation_target` varchar(16) DEFAULT 'FOE',
    `description` VARCHAR(2000) NOT NULL DEFAULT 'No description',
    `chance` INT NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `move`
-- -----------------------------------------------------
CREATE TABLE `move` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `element` VARCHAR(200) NOT NULL,
    `damage` VARCHAR(200) NOT NULL,
    `description` VARCHAR(2000) NOT NULL DEFAULT 'No description',
    `accuracy` VARCHAR(200) NOT NULL,
    `priority` BOOLEAN NOT NULL,
    `isPhysical` BOOLEAN NOT NULL,
    `pp` int(11) NOT NULL,          -- hvor mange ganger man kan bruke et move
    `crit_ratio` int(11) NOT NULL,
    `animation` varchar(16) DEFAULT 'NONE',
    `animation_target` varchar(16) DEFAULT 'FOE',
    PRIMARY KEY (`id`)
);
ALTER TABLE `move`
    ADD CONSTRAINT `fk_effect_move`
        FOREIGN KEY (`effect`) REFERENCES `effect` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Table `move/effect`
-- -----------------------------------------------------
CREATE TABLE `moveEffect` (
    `move` INT NOT NULL,
    `effect` INT NOT NULL,
    PRIMARY KEY (`move`, `effect`)
)
ALTER TABLE `moveEffect`
    ADD CONSTRAINT `fk_moveEffect_move` 
        FOREIGN KEY (`move`) REFERENCES `move` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_moveEffect_effect`
        FOREIGN KEY (`effect`) REFERENCES `effect` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE;


-- -----------------------------------------------------
-- Table `pokemon`
-- -----------------------------------------------------
CREATE TABLE `pokemon` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `level` int(11) DEFAULT 50,
    -- `description` VARCHAR(2000) NOT NULL, ? om trengs er den her.
    `img` VARCHAR(200) NOT NULL DEFAULT 'Not yet implemented',
    `element1` VARCHAR(200) NOT NULL,
    `element2` VARCHAR(200) NULL,
    `hp` INT NOT NULL,
    `atc` INT NOT NULL,
    `def` INT NOT NULL,
    `special_attack` int(11) NOT NULL,
    `special_defense` int(11) NOT NULL,
    `speed` INT NOT NULL,
    `sex` varchar(6) NOT NULL,
    `move1` INT NOT NULL,
    `move2` INT NOT NULL,
    `move3` INT NOT NULL,
    `move4` INT NOT NULL,
    PRIMARY KEY (`id`)
);
ALTER TABLE `pokemon`
    ADD CONSTRAINT `fk_move1_pokemon`
        FOREIGN KEY (`move1`) REFERENCES `move` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_move2_pokemon`
        FOREIGN KEY (`move2`) REFERENCES `move` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_move3_pokemon`
        FOREIGN KEY (`move3`) REFERENCES `move` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_move4_pokemon`
        FOREIGN KEY (`move4`) REFERENCES `move` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE;