-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2021 at 05:47 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `integrasjon_pokemon`
--

CREATE DATABASE `integrasjon_pokemon` DEFAULT CHARACTER SET utf8;
USE `integrasjon_pokemon`;

-- --------------------------------------------------------

--
-- Table structure for table `effects`
--

CREATE TABLE `effects` (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `target` enum('self','foe') NOT NULL,
  `apply_chance` tinyint(3) UNSIGNED NOT NULL,
  `status_applied` enum('none','burn','freeze','paralysis','poison','bad_poison','sleep') DEFAULT 'none',
  `stats_modified` char(7) DEFAULT '0000000',
  `modified_value` tinyint(4) DEFAULT 0,
  `animation` enum('none','attack','buff','debuff') DEFAULT 'none',
  `animation_target` enum('self','foe') DEFAULT 'foe'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `moves`
--

CREATE TABLE `moves` (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `type` enum('normal','fighting','flying','poison','ground','rock','bug','ghost','steel','fire','water','grass','electric','psychic','ice','dragon','dark') NOT NULL,
  `power` tinyint(3) UNSIGNED NOT NULL,
  `target` enum('self','foe') NOT NULL,
  `is_physical` tinyint(1) NOT NULL,
  `power_points` tinyint(4) NOT NULL,
  `accuracy` tinyint(3) UNSIGNED NOT NULL,
  `priority` tinyint(3) UNSIGNED DEFAULT 0,
  `crit_ratio` tinyint(3) UNSIGNED DEFAULT 0,
  `description` varchar(255) DEFAULT 'No description',
  `animation` enum('none','attack','buff','debuff') DEFAULT 'none',
  `animation_target` enum('self','foe') DEFAULT 'foe'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `move_has_effects`
--

CREATE TABLE `move_has_effects` (
  `move_id` smallint(5) UNSIGNED NOT NULL,
  `effect_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `player_lobbies`
--


CREATE TABLE `player_lobbies` (
  `socket_id` varchar(30) NOT NULL,
  `name` varchar(16) NOT NULL,
  `host_id` int(10) UNSIGNED NOT NULL,
  `host_socket` varchar(20) NOT NULL,
  `player_id` int(10) UNSIGNED DEFAULT NULL,
  `player_socket` varchar(20) DEFAULT NULL,
  `is_private` tinyint(1) UNSIGNED DEFAULT 0,
  `host_active_pokemon` enum('pokemon_first','pokemon_second','pokemon_third','pokemon_fourth','pokemon_fifth','pokemon_sixth') DEFAULT 'pokemon_first',
  `player_active_pokemon` enum('pokemon_first','pokemon_second','pokemon_third','pokemon_fourth','pokemon_fifth','pokemon_sixth') DEFAULT 'pokemon_first',
  `host_action` enum('move1','move2','move3','move4','pokemon_swap1','pokemon_swap2','pokemon_swap3','pokemon_swap4','pokemon_swap5','pokemon_swap6', 'waiting', 'ready') DEFAULT 'waiting',
  `player_action` enum('move1','move2','move3','move4','pokemon_swap1','pokemon_swap2','pokemon_swap3','pokemon_swap4','pokemon_swap5','pokemon_swap6', 'waiting', 'ready') DEFAULT 'waiting'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `level` tinyint(3) UNSIGNED DEFAULT 50,
  `hp` smallint(5) UNSIGNED NOT NULL,
  `attack` smallint(5) UNSIGNED NOT NULL,
  `defense` smallint(5) UNSIGNED NOT NULL,
  `special_attack` smallint(5) UNSIGNED NOT NULL,
  `special_defense` smallint(5) UNSIGNED NOT NULL,
  `speed` smallint(5) UNSIGNED NOT NULL,
  `type_primary` enum('normal','fighting','flying','poison','ground','rock','bug','ghost','steel','fire','water','grass','electric','psychic','ice','dragon','dark') NOT NULL,
  `type_secondary` enum('normal','fighting','flying','poison','ground','rock','bug','ghost','steel','fire','water','grass','electric','psychic','ice','dragon','dark','none') DEFAULT 'none',
  `sprite` varchar(255) DEFAULT 'Not yet implemented',
  `sex` enum('male','female','genderless') NOT NULL,
  `move_first` smallint(5) UNSIGNED NOT NULL,
  `move_second` smallint(5) UNSIGNED NOT NULL,
  `move_third` smallint(5) UNSIGNED NOT NULL,
  `move_fourth` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `wins` smallint(5) UNSIGNED DEFAULT 0,
  `losses` smallint(5) UNSIGNED DEFAULT 0,
  `pokemon_first` int(10) UNSIGNED DEFAULT NULL,
  `pokemon_second` int(10) UNSIGNED DEFAULT NULL,
  `pokemon_third` int(10) UNSIGNED DEFAULT NULL,
  `pokemon_fourth` int(10) UNSIGNED DEFAULT NULL,
  `pokemon_fifth` int(10) UNSIGNED DEFAULT NULL,
  `pokemon_sixth` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `effects`
--
ALTER TABLE `effects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_condition_index` (`status_applied`,`apply_chance`,`target`),
  ADD KEY `stat_modifier_index` (`stats_modified`,`modified_value`,`apply_chance`,`target`);

--
-- Indexes for table `moves`
--
ALTER TABLE `moves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `move_has_effects`
--
ALTER TABLE `move_has_effects`
  ADD PRIMARY KEY (`move_id`,`effect_id`),
  ADD KEY `effect_id` (`effect_id`);

--
-- Indexes for table `player_lobbies`
--
ALTER TABLE `player_lobbies`
  ADD PRIMARY KEY (`socket_id`),
  ADD KEY `player_id` (`player_id`),
  ADD KEY `lobby_host_index` (`host_id`),
  ADD KEY `lobby_name_index` (`name`);

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `move_first` (`move_first`),
  ADD KEY `move_second` (`move_second`),
  ADD KEY `move_third` (`move_third`),
  ADD KEY `move_fourth` (`move_fourth`),
  ADD KEY `pokemon_name_index` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `pokemon_first` (`pokemon_first`),
  ADD KEY `pokemon_second` (`pokemon_second`),
  ADD KEY `pokemon_third` (`pokemon_third`),
  ADD KEY `pokemon_fourth` (`pokemon_fourth`),
  ADD KEY `pokemon_fifth` (`pokemon_fifth`),
  ADD KEY `pokemon_sixth` (`pokemon_sixth`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `move_has_effects`
--
ALTER TABLE `move_has_effects`
  ADD CONSTRAINT `move_has_effects_ibfk_1` FOREIGN KEY (`move_id`) REFERENCES `moves` (`id`),
  ADD CONSTRAINT `move_has_effects_ibfk_2` FOREIGN KEY (`effect_id`) REFERENCES `effects` (`id`);

--
-- Constraints for table `player_lobbies`
--
ALTER TABLE `player_lobbies`
  ADD CONSTRAINT `player_lobbies_ibfk_1` FOREIGN KEY (`host_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `player_lobbies_ibfk_2` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD CONSTRAINT `pokemon_ibfk_1` FOREIGN KEY (`move_first`) REFERENCES `moves` (`id`),
  ADD CONSTRAINT `pokemon_ibfk_2` FOREIGN KEY (`move_second`) REFERENCES `moves` (`id`),
  ADD CONSTRAINT `pokemon_ibfk_3` FOREIGN KEY (`move_third`) REFERENCES `moves` (`id`),
  ADD CONSTRAINT `pokemon_ibfk_4` FOREIGN KEY (`move_fourth`) REFERENCES `moves` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`pokemon_first`) REFERENCES `pokemon` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`pokemon_second`) REFERENCES `pokemon` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`pokemon_third`) REFERENCES `pokemon` (`id`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`pokemon_fourth`) REFERENCES `pokemon` (`id`),
  ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`pokemon_fifth`) REFERENCES `pokemon` (`id`),
  ADD CONSTRAINT `users_ibfk_6` FOREIGN KEY (`pokemon_sixth`) REFERENCES `pokemon` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
